//
//  PersonModel.m
//  BeautyAddressBook
//
//  Created by 余华俊 on 15/10/22.
//  Copyright © 2015年 hackxhj. All rights reserved.
//

#import "PersonModel.h"

@implementation PersonModel

/*@property (nonatomic,copy) NSString *firstName;
 @property (nonatomic,copy) NSString *lastName;
 @property (nonatomic,copy) NSString *name1;
 @property (nonatomic,copy) NSString *phoneNumber;
 @property(nonatomic,copy)  NSString *phonename;
 @property(nonatomic,copy)  NSString *friendId;
 @property NSInteger sectionNumber;
 @property NSInteger recordID;
 @property BOOL rowSelected;
 @property (nonatomic, retain) NSString *email;
 @property (nonatomic, retain) NSString *tel;
 @property(nonatomic, strong) NSData *icon;//图片*/

- (void)encodeWithCoder:(NSCoder *)aCoder{
    
    [aCoder encodeObject:self.firstName forKey:@"person_firstName"];
    [aCoder encodeObject:self.lastName forKey:@"person_lastName"];
    [aCoder encodeObject:self.name1 forKey:@"person_name1"];
    [aCoder encodeObject:self.phoneNumber forKey:@"person_phoneNumber"];
    [aCoder encodeObject:self.phonename forKey:@"person_phonename"];
    [aCoder encodeObject:self.friendId forKey:@"person_friendId"];
    [aCoder encodeObject:self.email forKey:@"person_email"];
    [aCoder encodeObject:self.tel forKey:@"person_tel"];
    [aCoder encodeObject:self.icon forKey:@"person_icon"];
    [aCoder encodeInteger:self.recordID forKey:@"person_recordID"];
    [aCoder encodeInteger:self.sectionNumber forKey:@"person_sectionNumber"];
    [aCoder encodeBool:self.rowSelected forKey:@"person_rowSelected"];

    
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    
    
    if (self = [super init]){
        
        _firstName=[aDecoder decodeObjectForKey:@"person_firstName"];
        _lastName=[aDecoder decodeObjectForKey:@"person_lastName"];
        _name1=[aDecoder decodeObjectForKey:@"person_name1"];
        _phoneNumber=[aDecoder decodeObjectForKey:@"person_phoneNumber"];
        _phonename=[aDecoder decodeObjectForKey:@"person_phonename"];
        _friendId=[aDecoder decodeObjectForKey:@"person_friendId"];
        _email=[aDecoder decodeObjectForKey:@"person_email"];
        _tel=[aDecoder decodeObjectForKey:@"person_tel"];
        _icon=[aDecoder decodeObjectForKey:@"person_icon"];
        _recordID = [aDecoder decodeIntegerForKey:@"person_recordID"];
        _sectionNumber = [aDecoder decodeIntegerForKey:@"person_sectionNumber"];
        _rowSelected = [aDecoder decodeBoolForKey:@"person_rowSelected"];
    }
    
    return self;
    
}

@end
