//
//  YDSearchModel.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/4/19.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "YDSearchModel.h"

@implementation YDSearchModel


- (void)encodeWithCoder:(NSCoder *)aCoder{
    
    [aCoder encodeObject:self.YDNo forKey:@"SearchHistory_YDNo"];
    [aCoder encodeObject:self.searchTime forKey:@"SearchHistory_searchTime"];

    
}

- (id)initWithCoder:(NSCoder *)aDecoder{

    if (self = [super init]){
        
        _YDNo=[aDecoder decodeObjectForKey:@"SearchHistory_YDNo"];
        _searchTime=[aDecoder decodeObjectForKey:@"SearchHistory_searchTime"];
    }
    
    return self;
}

@end
