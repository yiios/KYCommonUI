//
//  YDSearchModel.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/4/19.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YDSearchModel : NSObject
@property (nonatomic, copy) NSString *YDNo;
@property (nonatomic, copy) NSString *searchTime; //寄件时间（原来是查询时间）
@end
