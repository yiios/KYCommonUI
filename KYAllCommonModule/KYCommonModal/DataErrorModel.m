//
//  DataErrorModel.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/8/20.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "DataErrorModel.h"
#import "MyUUIDManager.h"
#import "Tools.h"

@interface DataErrorModel()

//@property (nonatomic,copy) NSString *macAddress;
//@property (nonatomic,copy) NSString *userID; //登入账号
@end

@implementation DataErrorModel

- (instancetype)init
{
    self = [super init];
    NSLog(@"MyUUIDManager getUUID = %@",[MyUUIDManager getUUID]);
    
    self.deviceType = @"iOS";
    
    if ([MyUUIDManager getUUID] != NULL) {
        self.deviceCode = [MyUUIDManager getUUID];
    }else{
        self.deviceCode = @"";
    }
    
    //部分用户没有userID,所以使用电话号码
    self.loginAccount = [[UserInfoManage sharedInstance]  getTelePhone];
    
    return self;
}

- (instancetype)initNoticeDepartmentErrorWithResponse:(NSDictionary *)response;
{
    self = [self init];
    
    NSLog(@"loginAccount : %@",self.loginAccount);
    
    NSMutableDictionary *inputDict = [NSMutableDictionary dictionary];
    [inputDict setObject:kUUIDStr forKey:@"uuid"];
    
    self.input =  [self dictionaryToJson:inputDict];
    self.output = [self dictionaryToJson:response];
    self.interfaceName = @"UserGroup_V2";
    return self;
}



//登录错误日志

- (instancetype)initLoginErrorWithInputDic:(NSDictionary *)inputDic outDic:(NSDictionary *)outDic InterfaceName:(NSString*)InterfaceName
{
    self = [self init];
    self.input =  [self dictionaryToJson:inputDic];
    self.output = [self dictionaryToJson:outDic];
    self.interfaceName = InterfaceName;
    return self;
}

- (instancetype)initLoginErrorWithInputDic:(NSDictionary *)inputDic errorStr:(NSString *)errorStr InterfaceName:(NSString*)InterfaceName{
    
    self = [self init];
    self.input =  [self dictionaryToJson:inputDic];
    self.output = errorStr;
    self.interfaceName = InterfaceName;
    return self;
    
}


/*!
 
 * @brief 把格式化的JSON格式的字符串转换成字典
 
 * @param jsonString JSON格式的字符串
 
 * @return 返回字典
 
 */

- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    
    if (jsonString == nil) {
        return nil;
    }
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    
    if(err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    
    return dic;
}

//词典转换为字符串

- (NSString*)dictionaryToJson:(NSDictionary *)dic
{
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&parseError];
    
    NSString *jsonStr0 = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSMutableString *jsonStr = [Tools deleteSpecialJsonStrWithStr:jsonStr0];
    jsonStr = [NSMutableString stringWithString:[Tools filterSpaceSring:jsonStr]];
    if ([Tools isBlankString:jsonStr]) {
        jsonStr = [NSMutableString stringWithFormat:@""];
    }
    return jsonStr;
}

@end
