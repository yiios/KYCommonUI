//
//  ActivityModel.m
//  kyExpress
//
//  Created by zhigang on 15/12/23.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import "ActivityModel.h"
#import "DatabaseManager.h"

@implementation ActivityModel

+ (NSDictionary *)replacedKeyFromPropertyName{
    return @{@"ID":@"id"};
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeInteger:self.ID forKey:@"ID"];
    [aCoder encodeObject:self.classid forKey:@"classid"];
    [aCoder encodeObject:self.classParentId forKey:@"classParentId"];
    [aCoder encodeObject:self.startDate forKey:@"startDate"];
    [aCoder encodeObject:self.endDate forKey:@"endDate"];
    [aCoder encodeObject:self.titleImg forKey:@"titleImg"];
    [aCoder encodeObject:self.smallImg forKey:@"smallImg"];
    [aCoder encodeObject:self.largeImg forKey:@"largeImg"];
    [aCoder encodeObject:self.url forKey:@"url"];
    [aCoder encodeObject:self.accessUrl forKey:@"accessUrl"];
    [aCoder encodeObject:self.adName forKey:@"adName"];
    [aCoder encodeObject:self.className forKey:@"className"];
    [aCoder encodeObject:self.module forKey:@"module"];
    [aCoder encodeObject:self.isScreenshot forKey:@"isScreenshot"];
    [aCoder encodeObject:self.videoUrl forKey:@"videoUrl"];
    [aCoder encodeDouble:self.videoSecond forKey:@"videoSecond"];
    [aCoder encodeObject:self.lastModifyDate forKey:@"lastModifyDate"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init] ;
    if(self)
    {
        self.ID = [aDecoder decodeIntegerForKey:@"ID"] ;
        self.classid = [aDecoder decodeObjectForKey:@"classid"] ;
        self.classParentId = [aDecoder decodeObjectForKey:@"classParentId"] ;
        self.startDate = [aDecoder decodeObjectForKey:@"startDate"] ;
        self.endDate = [aDecoder decodeObjectForKey:@"endDate"] ;
        self.titleImg = [aDecoder decodeObjectForKey:@"titleImg"] ;
        self.smallImg = [aDecoder decodeObjectForKey:@"smallImg"] ;
        self.largeImg = [aDecoder decodeObjectForKey:@"largeImg"] ;
        self.url = [aDecoder decodeObjectForKey:@"url"] ;
        self.accessUrl = [aDecoder decodeObjectForKey:@"accessUrl"] ;
        self.adName = [aDecoder decodeObjectForKey:@"adName"] ;
        self.className = [aDecoder decodeObjectForKey:@"className"] ;
        self.module = [aDecoder decodeObjectForKey:@"module"] ;
        self.isScreenshot = [aDecoder decodeObjectForKey:@"isScreenshot"] ;
        self.videoUrl = [aDecoder decodeObjectForKey:@"videoUrl"];
        self.videoSecond = [aDecoder decodeDoubleForKey:@"videoSecond"];
        self.lastModifyDate = [aDecoder decodeObjectForKey:@"lastModifyDate"];
    }
    return self ;
}

-(BOOL)saveToDatabaseWithId:(NSString *)mainId h5Str:(NSString *)h5Str{
    
    //NSString *replaceSQL = @"INSERT INTO queryTakeTurnsBannerModel_T (ID, jsonData) VALUES (?, ?);",mainId, @(age)";
    
    __block BOOL ret = NO;
    //WS(weakSelf);
    [[DatabaseManager shareManager].databaseQueue inDatabase:^(FMDatabase *db) {
     ret = [db executeUpdate:@"INSERT INTO queryTakeTurnsBannerModel_T (ID, h5Content) VALUES (?, ?);", mainId, h5Str];
        
    }];
    return ret;

}

-(NSString *)getH5DataWithId:(NSString *)mainId{
    
    __block NSString * h5Str = @"";
    NSString * selectedSQL = [NSString stringWithFormat:@"SELECT * FROM queryTakeTurnsBannerModel_T WHERE ID = %@ order by createDate desc",mainId];//desc从大到小   asc从小到大
    [[DatabaseManager shareManager].databaseQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:selectedSQL];
        
        while ([rs next]) {
            h5Str = [rs stringForColumn:@"h5Content"];
            
        }
    }];
    return h5Str;
}

@end
