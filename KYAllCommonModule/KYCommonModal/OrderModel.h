//
//  OrderModel.h
//  kyExpress
//
//  Created by 陈志刚 on 15/12/11.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderModel : NSObject

/**
 *  订单号
 */
@property (nonatomic, copy) NSString *orderNum;

/**
 *  寄件时间
 */
@property (nonatomic, copy) NSString *sendTime;

/**
 *  收件人
 */
@property (nonatomic, copy) NSString *receiveUser;

/**
 *  目的地
 */
@property (nonatomic, copy) NSString *destinationLocation;


@property (nonatomic, copy) NSString *companyNo;

@property (nonatomic, copy) NSString *custAddr;

@property (nonatomic, copy) NSString *custName;   //客户名称



@end
