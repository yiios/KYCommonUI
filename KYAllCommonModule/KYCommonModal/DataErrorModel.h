//
//  DataErrorModel.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/8/20.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataErrorModel : NSObject

@property (nonatomic,copy) NSString *deviceType;
@property (nonatomic,copy) NSString *deviceCode;
@property (nonatomic,copy) NSString *loginAccount;
@property (nonatomic,copy) NSString *input;
@property (nonatomic,copy) NSString *output;
@property (nonatomic,copy) NSString *interfaceName;


- (instancetype)initNoticeDepartmentErrorWithResponse:(NSDictionary *)response;


- (instancetype)initLoginErrorWithInputDic:(NSDictionary *)inputDic outDic:(NSDictionary *)outDic InterfaceName:(NSString*)InterfaceName;

//登录错误日志


- (instancetype)initLoginErrorWithInputDic:(NSDictionary *)inputDic errorStr:(NSString *)errorStr InterfaceName:(NSString*)InterfaceName;



@end
