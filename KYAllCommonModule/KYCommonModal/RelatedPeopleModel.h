//
//  RelatedPeopleModel.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/8/25.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface RelatedPeopleModel : NSObject
@property (nonatomic,copy) NSString *PeopleName;    //string  员工姓名
@property (nonatomic,copy) NSString *Workshop;    //  string  所属部门
@property (nonatomic,copy) NSString *CZDepart;    //  string  所属点部
@property (nonatomic,copy) NSString *ZhiChun;    //  string  工作职务
@property (nonatomic,copy) NSString *UserType;    // int  查询类型 1 按操作部点部+姓名 2 按经理
@end
