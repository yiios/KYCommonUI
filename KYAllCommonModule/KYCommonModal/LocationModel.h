//
//  LocationModel.h
//  kyExpress
//
//  Created by 魏信洋 on 15/12/31.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
@interface LocationModel : NSObject

@property (nonatomic,assign) CLLocationDegrees latitude;
@property (nonatomic,assign) CLLocationDegrees longitude;

@property (nonatomic,copy) NSString *companyName;
@property (nonatomic,copy) NSString *companyPlace;

@property (nonatomic,copy) NSString *wholeAddress;  //地址全称
@property (nonatomic,copy) NSString *shortAddress;  //地址简称-第 5 级
@property (nonatomic,copy) NSString *shortAddress2;  //地址简称-第 3+4 级

@property (nonatomic,copy) NSString *code;  //区号（如：0755）
@property (nonatomic,copy) NSString *guid;  //编码

@property (nonatomic,strong) NSArray *addressArray;  //五级地址

@end
