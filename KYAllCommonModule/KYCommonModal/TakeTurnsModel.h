//
//  TakeTurnsModel.h
//  kyExpress_Internal
//
//  Created by ZHENGCHENG JIE on 2017/4/19.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TakeTurnsModel : NSObject

@property (nonatomic,copy) NSString *ID;     /**< Id */
@property (nonatomic,copy) NSString *h5Content;     /**< 内容 */


/**
 **
 *  存入数据
 */
-(BOOL)saveContentToDatabase;

/**
 *  查询所有数据
 *
 *  @return 返回数组
 */
+(NSArray *)getAllH5ContentInDatabaseWith:(NSString *)idStr;

@end
