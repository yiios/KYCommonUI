//
//  RouteModel.m
//  kyExpress
//
//  Created by 李兰芳 on 15/12/19.
//  Copyright (c) 2015年 kyExpress. All rights reserved.
//

#import "RouteModel.h"


@implementation RouteModel



//+(NSArray *)LuYousRouteArray:(NSArray *)LuYouArray
//{
//    NSMutableArray *array = [NSMutableArray array];
//    
//    int num = (int)LuYouArray.count;
//    NSLog(@"路由信息条数num = %d",num);
//    
//    for (int i = num - 1; i > 0; i--) {
//       
//        NSDictionary * dic = LuYouArray[i];
//        
//        RouteModel *model = [[RouteModel alloc]init];
//        model.recode = dic[@"Col_002"];
//        model.time = dic[@"LastTime"];
//        
//        if (i == num - 1) {
//            model.isFirst = YES;
//        }else{
//            model.isFirst = NO;
//        }
//        if (i == 1) {
//            model.isLast = YES;
//        }else{
//            model.isLast = NO;
//        }
//        
//        [array addObject:model];
//
//    }
//    
//    return array;
//
//}

+(NSMutableArray *)PartnerLuYousRouteArray:(NSArray *)LuYouArray //伙伴路由
{
    NSMutableArray *array = [NSMutableArray array];
    
    int num = (int)LuYouArray.count;
    NSLog(@"伙伴路由信息条数num = %d",num);
    
    for (int i = num - 1; i > -1; i--) {
        
        NSDictionary * dic = LuYouArray[i];
        
        RouteModel *model = [[RouteModel alloc]init];
        model.recode = dic[@"RouteInfo"];
        model.time = dic[@"HandoVerTime"];
        
        if (i == num - 1) {
            model.isFirst = YES;
        }else{
            model.isFirst = NO;
        }
        if (i == 1) {
            model.isLast = YES;
        }else{
            model.isLast = NO;
        }
        
        [array addObject:model];
        
    }
    
    return array;
    
}

+(NSMutableArray *)LuYousRouteArray:(NSArray *)LuYouArray //内部路由
{
    NSMutableArray *array = [NSMutableArray array];
    
    int num = (int)LuYouArray.count;
    NSLog(@"内部路由信息条数num = %d",num);
    
    for (int i = num - 1; i > 0; i--) {
        
        NSDictionary * dic = LuYouArray[i];
        
        RouteModel *model = [[RouteModel alloc]init];
        model.recode = dic[@"Col_003"];
        model.time = dic[@"Col_002"];
        model.Col_001 = dic[@"Col_001"];
        model.UniqId = dic[@"UniqId"];


        if (i == num - 1) {
            model.isFirst = YES;
        }else{
            model.isFirst = NO;
        }
        if (i == 1) {
            model.isLast = YES;
        }else{
            model.isLast = NO;
        }
        
        [array addObject:model];
        
    }
    
    return array;
    
}


+(NSMutableArray *)outLuYousRouteArray:(NSArray *)LuYouArray //外部路由
{
    NSMutableArray *array = [NSMutableArray array];
    
    int num = (int)LuYouArray.count;
    NSLog(@"外部路由信息条数num = %d",num);
    
    for (int i = num - 1; i > -1; i--) {
        
        NSDictionary * dic = LuYouArray[i];
        
        RouteModel *model = [[RouteModel alloc]init];
        model.recode = dic[@"Col_002"];
        model.time = dic[@"Col_008"];
        model.Col_001 = dic[@"Col_001"];
        
        
        if (i == num - 1) {
            model.isFirst = YES;
        }else{
            model.isFirst = NO;
        }
        if (i == 0) {
            model.isLast = YES;
        }else{
            model.isLast = NO;
        }
        
        [array addObject:model];
        
    }
    
    return array;
    
}





+(NSArray *)routeModelArray:(NSDictionary *)sender
{
    
    
//    Col_076：寄件时间，string   Col_106：跟踪记录1，string
//    Col_112：到达公司时间，string   Col_107：跟踪记录2，string
//    Col_113：提货时间，string   Col_108：跟踪记录3，string
//    Col_281：到达点部时间，    Col_282：跟踪记录4，string
//    Col_328：装车派送时间，    Col_329：跟踪记录5，string
//    Col_379：派签到，string     Col_380：跟踪记录6, string
//    Col_062：签收时间，string   Col_109：跟踪记录7，string
    
    NSMutableArray *array = [NSMutableArray array];
    
    if (![sender[@"Col_109"] isEqualToString:@""])
    {
        RouteModel *model = [[RouteModel alloc]init];
        model.recode = sender[@"Col_109"];
        model.time = sender[@"Col_062"];
        [array addObject:model];
        //return array ;
    }
    if (![sender[@"Col_380"] isEqualToString:@""])
    {
        
        RouteModel *model = [[RouteModel alloc]init];
        model.recode = sender[@"Col_380"];
        model.time = sender[@"Col_379"];
        [array addObject:model];
       // return array ;

    }
    if (![sender[@"Col_329"] isEqualToString:@""])
    {
        
        RouteModel *model = [[RouteModel alloc]init];
        model.recode = sender[@"Col_329"];
        model.time = sender[@"Col_328"];
        [array addObject:model];
       // return array ;

    }
    if (![sender[@"Col_282"] isEqualToString:@""])
    {
        
        RouteModel *model = [[RouteModel alloc]init];
        model.recode = sender[@"Col_282"];
        model.time = sender[@"Col_281"];
        [array addObject:model];
       // return array ;

    }
    if (![sender[@"Col_108"] isEqualToString:@""])
    {
        
        RouteModel *model = [[RouteModel alloc]init];
        model.recode = sender[@"Col_108"];
        model.time = sender[@"Col_113"];
        [array addObject:model];
       // return array ;

    }
    if (![sender[@"Col_107"] isEqualToString:@""])
    {
        
        RouteModel *model = [[RouteModel alloc]init];
        model.recode = sender[@"Col_107"];
        model.time = sender[@"Col_112"];
        [array addObject:model];
       // return array ;

    }
    if (![sender[@"Col_106"] isEqualToString:@""])
    {
        
        RouteModel *model = [[RouteModel alloc]init];
        model.recode = sender[@"Col_106"];
        model.time = sender[@"Col_076"];
        [array addObject:model];
       // return array ;

    }
    
    return array;
}

+(NSArray *)routeDetailModelArray:(NSDictionary *)sender
{
    
    
    //    Col_076：寄件时间，string   Col_106：跟踪记录1，string
    //    Col_112：到达公司时间，string   Col_107：跟踪记录2，string
    //    Col_113：提货时间，string   Col_108：跟踪记录3，string
    //    Col_281：到达点部时间，    Col_282：跟踪记录4，string
    //    Col_328：装车派送时间，    Col_329：跟踪记录5，string
    //    Col_379：派签到，string     Col_380：跟踪记录6, string
    //    Col_062：签收时间，string   Col_109：跟踪记录7，string
    
    NSMutableArray *array = [NSMutableArray array];
    
    if (![sender[@"Col_109"] isEqualToString:@""])
    {
        RouteModel *model = [[RouteModel alloc]init];
        model.recode = sender[@"Col_109"];
        model.time = [self subTimeString:sender[@"Col_062"]];
        [array addObject:model];
    }
    if (![sender[@"Col_380"] isEqualToString:@""])
    {
        
        RouteModel *model = [[RouteModel alloc]init];
        model.recode = sender[@"Col_380"];
        model.time = [self subTimeString:sender[@"Col_379"]];
        [array addObject:model];
    }
    if (![sender[@"Col_329"] isEqualToString:@""])
    {

        RouteModel *model = [[RouteModel alloc]init];
        model.recode = sender[@"Col_329"];
        model.time = [self subTimeString:sender[@"Col_328"]];
        [array addObject:model];
    }
    if (![sender[@"Col_282"] isEqualToString:@""])
    {

        RouteModel *model = [[RouteModel alloc]init];
        model.recode = sender[@"Col_282"];
        model.time = [self subTimeString:sender[@"Col_281"]];
        [array addObject:model];
    }
    if (![sender[@"Col_108"] isEqualToString:@""])
    {

        RouteModel *model = [[RouteModel alloc]init];
        model.recode = sender[@"Col_108"];
        model.time = [self subTimeString:sender[@"Col_113"]];
        [array addObject:model];
    }
    if (![sender[@"Col_107"] isEqualToString:@""])
    {

        RouteModel *model = [[RouteModel alloc]init];
        model.recode = sender[@"Col_107"];
        model.time = [self subTimeString:sender[@"Col_112"]];
        [array addObject:model];
    }
    if (![sender[@"Col_106"] isEqualToString:@""])
    {

        RouteModel *model = [[RouteModel alloc]init];
        model.recode = sender[@"Col_106"];
//            model.time = [sender[@"Col_076"]substringToIndex:16];
        model.time = [self subTimeString:sender[@"Col_076"]];
        [array addObject:model];
    }
    
    return array;
}

+(NSString *)subTimeString:(NSString *)time{
    return [time substringToIndex:16];
}

-(void)setRecode:(NSString *)recode
{
    _recode = recode;
    //屏幕比例转换
    CGFloat padding =50*([UIScreen mainScreen].bounds.size.width/320.0);

    CGFloat recordW = [UIScreen mainScreen].bounds.size.width - padding - 120 - 10;
    
    _contentH =[self caculateText:_recode fontSize:12 maxSize:CGSizeMake(recordW, CGFLOAT_MAX)].height;
  //  NSLog(@"屏幕比例转换 recode = %@ padding = %f  recordW = %f _contentH = %f",recode,padding,recordW,_contentH);

    
}

-(CGSize)caculateText:(NSString *)str fontSize:(CGFloat)size maxSize:(CGSize)maxSize
{
    UIFont * font = [UIFont systemFontOfSize:size];
    return [str boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;
    
}

@end
