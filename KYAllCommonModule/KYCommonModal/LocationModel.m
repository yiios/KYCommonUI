//
//  LocationModel.m
//  kyExpress
//
//  Created by 魏信洋 on 15/12/31.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import "LocationModel.h"

@implementation LocationModel

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.wholeAddress forKey:@"wholeAddress"];
    [aCoder encodeObject:self.shortAddress forKey:@"shortAddress"];
    [aCoder encodeObject:self.shortAddress2 forKey:@"shortAddress2"];
    
    [aCoder encodeObject:self.code forKey:@"code"];
    [aCoder encodeObject:self.guid forKey:@"guid"];
    
    [aCoder encodeDouble:self.latitude forKey:@"latitude"];
    [aCoder encodeDouble:self.longitude forKey:@"longitude"];
    
    [aCoder encodeObject:self.companyName forKey:@"companyName"];
    [aCoder encodeObject:self.companyPlace forKey:@"companyPlace"];
    [aCoder encodeObject:self.addressArray forKey:@"addressArray"];

}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        
        self.wholeAddress=[aDecoder decodeObjectForKey:@"wholeAddress"];
        self.shortAddress=[aDecoder decodeObjectForKey:@"shortAddress"];
        self.shortAddress2=[aDecoder decodeObjectForKey:@"shortAddress2"];
        
        self.code=[aDecoder decodeObjectForKey:@"code"];
        self.guid=[aDecoder decodeObjectForKey:@"guid"];
        
        self.companyName=[aDecoder decodeObjectForKey:@"companyName"];
        self.companyPlace=[aDecoder decodeObjectForKey:@"companyPlace"];
     
        self.latitude = [aDecoder decodeDoubleForKey:@"latitude"];
        self.longitude = [aDecoder decodeDoubleForKey:@"longitude"];
        self.addressArray = [aDecoder decodeObjectForKey:@"addressArray"];
        
    }
    return self;
}

@end
