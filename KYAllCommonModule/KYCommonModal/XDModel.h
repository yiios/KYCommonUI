//
//  XDModel.h
//  kyExpress
//
//  Created by 陈志刚 on 15/12/15.
//  Copyright © 2015年 kyExpress. All rights reserved.
//  下单请求接口使用的模型


/**
 *  下单模型
 */
#import <Foundation/Foundation.h>

@interface XDModel : NSObject<NSCopying>

/**
 *  联系人
 */
//@property (nonatomic, copy) NSString *cD_108;
@property (nonatomic, copy) NSString *dD_108;
/**
 *  手机号
 */
//@property (nonatomic, copy) NSString *cD_105;
@property (nonatomic, copy) NSString *dD_105;
/**
 *  件数
 */
//@property (nonatomic, copy) NSString *cD_106;
@property (nonatomic, copy) NSString *dD_106;

/**
 *  重量
 */
//@property (nonatomic, copy) NSString *cD_107;
@property (nonatomic, copy) NSString *dD_107;

/**
 *  详细地址
 */
//@property (nonatomic, copy) NSString *cD_110;
@property (nonatomic, copy) NSString *dD_110;


/**
 *  货好时间
 */
//@property (nonatomic, copy) NSString *cD_133;
@property (nonatomic, copy) NSString *dD_133;

/**
 *  特殊备注
 */
//@property (nonatomic, copy) NSString *cD_111;
@property (nonatomic, copy) NSString *dD_111;



/**
 *  寄托物
 */
//@property (nonatomic, copy) NSString *cD_168;
@property (nonatomic, copy) NSString *dD_168;

/**
 *  客户名称
 */
//@property (nonatomic, copy) NSString *cD_104;
@property (nonatomic, copy) NSString *dD_104;

/**
 *  尺寸
 */
@property (nonatomic, copy) NSString *col_033;

// 新尺寸

@property (nonatomic, copy) NSString *dD_200;


/**
 *  固定电话
 */
//@property (nonatomic, copy) NSString *cD_191;
@property (nonatomic, copy) NSString *dD_191;


/**
 *  退货公司
 */
@property (nonatomic, copy) NSString *dD_212;
@property (nonatomic, copy) NSString *dD_217;       //付款公司、
@property (nonatomic, copy) NSString *dD_213;       //收件人、
@property (nonatomic, copy) NSString *dD_214;       //收件电话、
@property (nonatomic, copy) NSString *dD_223;       //退货地址


//新增字段

@property (nonatomic, copy) NSString *dD_134; // 临时要求配载
@property (nonatomic, copy) NSString *dD_153; // 目的地1
@property (nonatomic, copy) NSString *dD_156; // 目的地2
@property (nonatomic, copy) NSString *dD_159; // 目的地3

@property (nonatomic, copy) NSString *dD_154; // 服务方式1
@property (nonatomic, copy) NSString *dD_157; // 服务方式2
@property (nonatomic, copy) NSString *dD_160; // 服务方式3

//
@property (nonatomic, copy) NSString *dD_171; // 区号1
@property (nonatomic, copy) NSString *dD_172; // 区号2
@property (nonatomic, copy) NSString *dD_173; // 区号3

@property (nonatomic, copy) NSString *dD_166; //  新客户

// 增加入参
@property (nonatomic, copy) NSString *dD_155; // 重量1
@property (nonatomic, copy) NSString *dD_158; // 重量2
@property (nonatomic, copy) NSString *dD_161; // 重量3

@property (nonatomic, copy) NSString *dD_197;  // 总票数

@property (nonatomic, copy) NSString *dD_208;  //(临时取派点部)可空;
@property (nonatomic, copy) NSString *dD_203;  //临时取货地址
@property (nonatomic, copy) NSString *dD_207;  //管理编码
@property (nonatomic, copy) NSString *addrQH;  //区号
/**
 *  回公司过磅
 */
@property (nonatomic, copy) NSString *backComtoWeigh;

@property (nonatomic, copy) NSString *SCyuan;    //市场员

@property (nonatomic, copy)NSString * dD_184;   //到唯品会否

@property (nonatomic, copy)NSString * dD_222;   //取货联系人电话

@end
