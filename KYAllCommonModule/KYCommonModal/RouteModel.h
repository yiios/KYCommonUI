//
//  RouteModel.h
//  kyExpress
//
//  Created by 李兰芳 on 15/12/19.
//  Copyright (c) 2015年 kyExpress. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RouteModel : NSObject
/** 记录 **/
@property (strong, nonatomic) NSString *recode; //对应返回的Col_003字段
/** 记录时间 **/
@property (strong, nonatomic) NSString *time; //对应返回的Col_002 字段
/** 记录文字高度 **/
@property (assign, nonatomic) CGFloat  contentH;
@property (strong, nonatomic) NSString *UniqId;  //路由对应的ID
@property (strong, nonatomic) NSString *Col_001; //对应返回的Col_001 字段


@property (nonatomic,assign) BOOL  isFirst;  //是第一条记录

@property (nonatomic,assign) BOOL  isLast;  //是最后一条记录


+(NSArray *)routeModelArray:(NSDictionary *)sender;

+(NSMutableArray *)outLuYousRouteArray:(NSArray *)LuYouArray; //外部路由

+(NSMutableArray *)LuYousRouteArray:(NSArray *)LuYouArray;//内部路由

+(NSMutableArray *)PartnerLuYousRouteArray:(NSArray *)LuYouArray; //伙伴路由
+(NSArray *)routeDetailModelArray:(NSDictionary *)sender;
@end
