//
//  ActivityModel.h
//  kyExpress
//
//  Created by zhigang on 15/12/23.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ActivityModel : NSObject<NSCoding>


@property (nonatomic, assign) int ID;  //广告ID
@property (nonatomic, copy) NSString *classid;

@property (nonatomic, copy) NSString *classParentId;
//@property (nonatomic, copy) NSString *classRecomdFlag;

//@property (nonatomic, copy) NSString *content;

@property (nonatomic, copy) NSString *startDate;
@property (nonatomic, copy) NSString *endDate;


//@property (nonatomic, copy) NSString *imgPaths;
@property (nonatomic, copy) NSString *titleImg; //
@property (nonatomic, copy) NSString *smallImg;
@property (nonatomic, copy) NSString *largeImg;

@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *accessUrl;
@property (nonatomic, copy) NSString *adName; ////标题
@property (nonatomic, copy) NSString *className; //类型		如：幻灯片
/** 启动视频相关 */
/** 启动视频urlString */
@property (nonatomic, copy) NSString *videoUrl;
/** 启动视频时长（秒） */
@property(nonatomic, assign) NSTimeInterval videoSecond;


@property (nonatomic, assign) int sort;

@property (nonatomic, copy) NSString *module;
@property (nonatomic, copy) NSString *lastModifyDate;
@property (nonatomic, copy) NSString *moduleID;
@property (nonatomic, copy) NSString *isScreenshot;   //是否允许查看Web图片,1：是  0：否
@end
