//
//  XDModel.m
//  kyExpress
//
//  Created by 陈志刚 on 15/12/15.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import "XDModel.h"

@implementation XDModel
- (instancetype)init
{
    if (self = [super init]) {
        self.dD_108 = @"";
        self.dD_105 = @"";
        self.dD_106 = @"";
        self.dD_107 = @"";
        
        self.dD_110 = @"";
        self.dD_133 = @"";
        self.dD_111 = @"";
        
        self.dD_168 = @"";
        self.dD_104 = @"";
        
        self.col_033 = @"";
        self.dD_191 = @"";
        
        self.dD_134 = @"";
        
        
        self.dD_153 = @"";
        self.dD_156 = @"";
        self.dD_159 = @"";
        
        self.dD_154 = @"";
        self.dD_157 = @"";
        self.dD_160 = @"";
        
        
        self.dD_171 = @"";
        self.dD_172 = @"";
        self.dD_173 = @"";
        
        self.dD_166 = @"";
        
        self.dD_155 = @"";
        self.dD_158 = @"";
        self.dD_161 = @"";
        self.dD_197 = @"";
        
        self.dD_212 = @"";
        //回公司入磅
        self.backComtoWeigh = @"";
        self.dD_200 = @"";
        
        self.dD_208 = @"";
        self.dD_203 = @"";  //临时取货地址
        self.dD_207 = @"";  //管理编码
        self.addrQH = @"";  //区号
        self.dD_184 = @"";  //是否唯品会
        
        self.dD_217 = @"";       //付款公司、
        self.dD_213 = @"";       //收件人、
        self.dD_214 = @"";       //收件电话、
        self.dD_223 = @"";
    }
    return self;
}

- (instancetype)copyWithZone:(nullable NSZone *)zone{

    XDModel * tmp = [[XDModel alloc]init];
    tmp.dD_108 = self.dD_108;
    tmp.dD_105 = self.dD_105;
    tmp.dD_106 = self.dD_106;
    tmp.dD_107 = self.dD_107;
   
    tmp.dD_110 = self.dD_110;
    tmp.dD_133 = self.dD_133;
    tmp.dD_111 = self.dD_111;
    
    tmp.dD_168 = self.dD_168;
    tmp.dD_104 = self.dD_104;
    
    tmp.col_033 = self.col_033;
    tmp.dD_191 = self.dD_191;
    
    tmp.dD_134 = self.dD_134;
    

    tmp.dD_153 = self.dD_153;
    tmp.dD_156 = self.dD_156;
    tmp.dD_159 = self.dD_159;
    
    tmp.dD_154 = self.dD_154;
    tmp.dD_157 = self.dD_157;
    tmp.dD_160 = self.dD_160;
    

    tmp.dD_171 = self.dD_171;
    tmp.dD_172 = self.dD_172;
    tmp.dD_173 = self.dD_173;
    
    tmp.dD_166 = self.dD_166;
    
    tmp.dD_155 = self.dD_155;
    tmp.dD_158 = self.dD_158;
    tmp.dD_161 = self.dD_161;
    
    tmp.dD_197 = self.dD_197;
    
    tmp.dD_212 = self.dD_212;

    //回公司入磅
    tmp.backComtoWeigh = self.backComtoWeigh;
    tmp.dD_200 = self.dD_200;
    
    tmp.dD_208 = self.dD_208;
    tmp.dD_203 = self.dD_203;  //临时取货地址
    tmp.dD_207 = self.dD_207;  //管理编码
    tmp.addrQH = self.addrQH;  //区号

    tmp.SCyuan = self.SCyuan;
    
    tmp.dD_184 = self.dD_184;
    
    tmp.dD_222 = self.dD_222;
    
    tmp.dD_217 = self.dD_217;       //付款公司、
    tmp.dD_213 = self.dD_213;       //收件人、
    tmp.dD_214 = self.dD_214;       //收件电话、
    tmp.dD_223 = self.dD_223;
    
    return  tmp;
}
- (NSString *)description
{
    return [NSString stringWithFormat:@"联系人：%@\n手机号:%@\n件数:%@\n重量:%@\n票数:%@\n详细地址:%@\n货好时间:%@\n特殊备注:%@\n寄托物:%@\n客户名称:%@\n尺寸:%@\n固定电话:%@\n目的地1:%@\n服务方式1:%@\n重量1:%@\n区号1:%@\n目的地2:%@\n服务方式2:%@\n重量2:%@\n区号2:%@\n目的地3:%@\n服务方式3:%@\n重量3:%@\n区号3:%@\n新客户:%@\n临时要求配载:%@\n退货公司:%@\n回公司过磅:%@\n临时取货地址:%@\n管理编码:%@\n区号:%@\n市场员:%@\n到唯品会否:%@\n取货电话:%@\n付款公司:%@\n收件人:%@\n收件电话:%@\n退货地址:%@",self.dD_108,self.dD_105,self.dD_106,self.dD_107,self.dD_197,self.dD_110,self.dD_133,self.dD_111,self.dD_168,self.dD_104,self.dD_200,self.dD_191,self.dD_153,self.dD_154,self.dD_155,self.dD_171,self.dD_156,self.dD_157,self.dD_158,self.dD_172,self.dD_159,self.dD_160,self.dD_161,self.dD_173,self.dD_166,self.dD_134,self.dD_212,self.backComtoWeigh,self.dD_203,self.dD_207,self.addrQH,self.SCyuan,self.dD_184,self.dD_222,self.dD_217,self.dD_213,self.dD_214,self.dD_223];
}
@end
