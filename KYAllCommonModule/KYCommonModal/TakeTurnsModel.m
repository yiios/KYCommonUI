//
//  TakeTurnsModel.m
//  kyExpress_Internal
//
//  Created by ZHENGCHENG JIE on 2017/4/19.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "TakeTurnsModel.h"
#import "DatabaseManager.h"

@implementation TakeTurnsModel

+ (NSDictionary *)replacedKeyFromPropertyName{
    return @{@"ID":@"id"};
    
}


-(BOOL)saveContentToDatabase{
    
    NSString *replaceSQL = @"REPLACE INTO queryTakeTurnsBannerModel_T(ID,h5Content) VALUES(?,?)";
    
    __block BOOL ret = NO;
    WS(weakSelf);
    [[DatabaseManager shareManager].databaseQueue inDatabase:^(FMDatabase *db) {
        ret = [db executeUpdate:replaceSQL,
               weakSelf.ID,
               weakSelf.h5Content
               ];
    }];
    return ret;
}


+(NSArray *)getAllH5ContentInDatabaseWith:(NSString *)idStr{
    
    NSString *selectedSQL = [NSString stringWithFormat:@"SELECT * FROM queryTakeTurnsBannerModel_T WHERE ID = %@",idStr];
    
    NSMutableArray *arrM = [NSMutableArray array];
    
    NSLog(@"selectedSQL = %@",selectedSQL);
    
    [[DatabaseManager shareManager].databaseQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:selectedSQL];
        
        while ([rs next]) {
            TakeTurnsModel *model = [[TakeTurnsModel alloc]init];
            model.ID = [rs stringForColumn:@"ID"];
            model.h5Content = [rs stringForColumn:@"h5Content"];
            
            [arrM addObject:model];
        }
    }];
    return arrM;
}
@end
