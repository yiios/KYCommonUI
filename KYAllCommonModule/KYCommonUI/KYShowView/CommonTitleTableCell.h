//
//  CommonTitleTableCell.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/10/10.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonTitleTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label0;

+ (UINib *)nib;
@end
