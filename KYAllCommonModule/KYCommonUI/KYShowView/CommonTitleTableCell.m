//
//  CommonTitleTableCell.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/10/10.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "CommonTitleTableCell.h"

@implementation CommonTitleTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

+ (UINib *)nib
{
    return [UINib nibWithNibName:@"CommonTitleTableCell" bundle:nil];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
