//
//  ShowPhotoCell.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/8/5.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BlockDidTouchImg)(void);

@interface ShowPhotoCell : UITableViewCell
+ (UINib *)nib;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (nonatomic,copy) BlockDidTouchImg blockDidTouchImg;

-(void)setBlockDidTouchImg:(BlockDidTouchImg)blockDidTouchImg;

@end
