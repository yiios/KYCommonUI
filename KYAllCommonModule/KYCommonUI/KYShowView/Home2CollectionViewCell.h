//
//  Home2CollectionViewCell.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/4/15.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AYBubbleView.h"

typedef void(^RemoveCornerMarkBlock)();


@interface Home2CollectionViewCell : UICollectionViewCell

@property (nonatomic,assign) BOOL isUploading;

@property (nonatomic, strong)  UIImageView *imageView;
@property (nonatomic, strong)  UIImageView *uploadImgView;
@property (nonatomic, strong)  UILabel *label;
//@property (nonatomic, strong)  UILabel *badgeLabel;
@property (nonatomic, strong) AYBubbleView *cellBubbleView;  //
//@property (strong, nonatomic, readonly) UIStepper *mySteper;

@property (nonatomic, copy) RemoveCornerMarkBlock removeCornerMarkBlock;


- (void)refreshUIWithImageName:(NSString *)imageName title:(NSString *)title badgeLabelStr:(NSString *)badgeLabelStr;

-(void)setRemoveCornerMarkBlock:(RemoveCornerMarkBlock)removeCornerMarkBlock;

@end
