//
//  TextViewTableViewCell.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/11/15.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextViewTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextView *txView;

+ (UINib *)nib;

@end
