//
//  ShowPhotoCell.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/8/5.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "ShowPhotoCell.h"

@implementation ShowPhotoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.imgView.userInteractionEnabled = YES;
    [self.imgView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imgTouchAction)]];
    
    [self setSeparatorInset:UIEdgeInsetsMake(0, kSCREEN_WIDTH, 0, 0)];  //上左下右
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)imgTouchAction{

    if (_blockDidTouchImg) {
        _blockDidTouchImg();
    }
}



+ (UINib *)nib{
    return [UINib nibWithNibName:@"ShowPhotoCell" bundle:nil];
}

-(void)setBlockDidTouchImg:(BlockDidTouchImg)blockDidTouchImg
{
    _blockDidTouchImg = blockDidTouchImg;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
