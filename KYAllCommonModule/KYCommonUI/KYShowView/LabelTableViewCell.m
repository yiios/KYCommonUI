//
//  LabelTableViewCell.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/9/3.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "LabelTableViewCell.h"
#import "Tools.h"

@implementation LabelTableViewCell


+ (UINib *)nib{
    return [UINib nibWithNibName:@"LabelTableViewCell" bundle:nil];
}

//运费时效-历史选项（地址）
- (void)refreshUIForFTLocationHistory:(NSString *)address{
    self.label.textAlignment = NSTextAlignmentLeft;
    self.label.text = address;
    self.label.textColor = kColorTextBlack;
    self.label.numberOfLines = 0;
    
    self.label.font = kFont16;
}

//通知详情-查阅对象
- (void)refreshUIForNoticeRecieveDepartWithStr:(NSString *)recieveDepart{
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.label.textAlignment = NSTextAlignmentLeft;
    self.label.text = [NSString stringWithFormat:@"查阅对象：%@",recieveDepart];;
    self.label.textColor = KTextGray;
    self.label.numberOfLines = 0;
    
    self.label.font = kFont14;
}

//通知详情-对象人数
- (void)refreshUIForNoticeRecieveCountWithStr:(NSString *)recieveCount{
    self.label.textAlignment = NSTextAlignmentLeft;
    
    if ([Tools isBlankString:recieveCount]) {
        self.label.text = @"对象人数";
    }else{
        self.label.text = [NSString stringWithFormat:@"对象人数：%@",recieveCount];
    }
    
    self.label.textColor = KTextGray;
    self.label.numberOfLines = 0;
    
    self.label.font = kFont14;
}

//通知详情-对象职位
- (void)refreshUIForNoticeJobsWithStr:(NSString *)jobs{
    self.label.textAlignment = NSTextAlignmentLeft;
    
    if ([Tools isBlankString:jobs]) {
        
        self.label.text = @"";
        self.label.hidden = YES;
        
    }else{
        
        NSString *str = [StringTools getStr1:jobs];
        self.label.hidden = NO;
        self.label.text = [NSString stringWithFormat:@"对象职位：%@",str];
        
    }
    
    self.label.textColor = KTextGray;
    self.label.numberOfLines = 0;
    
    self.label.font = kFont14;
}




- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
