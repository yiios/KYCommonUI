//
//  LineTableViewCell.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/6/30.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LineTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *lineView;

+ (UINib *)nib;
@end
