//
//  ShowResultTableViewCell.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/3/12.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "ShowResultTableViewCell.h"
#import "UILabel+Copyable.h"

@implementation ShowResultTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.titleLabel.font = KTextFont;
    self.resultLabel.font = KTextFont;
    
    self.titleLabel.textColor = kColorTextBlack;
    self.resultLabel.textColor = kColorTextBlack;
    
    self.resultLabel.numberOfLines = 0;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.resultLabel.copyingEnabled = YES;
}

- (void)refreshUIWithTitle:(NSString *)titleStr result:(NSString *)resultStr{
    
    [self.resultLabel setText:resultStr];
    [self.titleLabel setText:titleStr];
    
    NSLog(@"resultStr : %@",resultStr);
    
    
}
+ (UINib *)nib{
    return [UINib nibWithNibName:@"ShowResultTableViewCell" bundle:nil];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
