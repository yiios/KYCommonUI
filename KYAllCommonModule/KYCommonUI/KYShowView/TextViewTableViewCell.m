//
//  TextViewTableViewCell.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/11/15.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "TextViewTableViewCell.h"

@implementation TextViewTableViewCell


+ (UINib *)nib{
    return [UINib nibWithNibName:@"TextViewTableViewCell" bundle:nil];
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
