//
//  LabelTableViewCell.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/9/3.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LabelTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label;
+ (UINib *)nib;

//通知详情-接收部门
- (void)refreshUIForNoticeRecieveDepartWithStr:(NSString *)recieveDepart;
//通知详情-对象人数
- (void)refreshUIForNoticeRecieveCountWithStr:(NSString *)recieveCount;

//通知详情-对象职位
- (void)refreshUIForNoticeJobsWithStr:(NSString *)jobs;

//运费时效-历史选项（地址）
- (void)refreshUIForFTLocationHistory:(NSString *)address;
@end
