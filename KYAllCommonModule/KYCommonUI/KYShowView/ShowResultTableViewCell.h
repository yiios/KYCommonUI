//
//  ShowResultTableViewCell.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/3/12.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CopyLabel;

@interface ShowResultTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;

+ (UINib *)nib;
- (void)refreshUIWithTitle:(NSString *)titleStr result:(NSString *)resultStr;
@end
