//
//  ZRLoadingHUD.m
//  CoreAnimationDemo
//
//  Created by GKY on 2017/9/11.
//  Copyright © 2017年 Run. All rights reserved.
//

#import "ZRLoadingHUD.h"

@interface ZRLoadingHUD ()

@property (nonatomic,strong) UIColor *smallRoundedfillColor;
@property (nonatomic,strong) UIColor *minRoundedfileColor;
@end

@implementation ZRLoadingHUD
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.currentProgress = 0;
        self.isPlay = YES;
        [self setBackgroundColor:[UIColor clearColor]];
        //        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(playOrSuspendTapeGestureAction)];
        //        [self addGestureRecognizer:tapGesture];
        
    }
    return self;
}

//- (void)willMoveToSuperview:(UIView *)newSuperview{
//    [super willMoveToSuperview:newSuperview];
//    if (!newSuperview) {
//        //销毁
//        [_displayLink invalidate];
//    }
//}

- (void)drawRect:(CGRect)rect {
    CGFloat width = rect.size.width;
    CGFloat height = rect.size.height;
    //    CGFloat bigRadius = width * 0.9 / 2;
    CGFloat smallRadius = width * 1.f / 2;
    
//    [[UIColor colorWithWhite:0 alpha:0] setFill];
//    [[UIColor colorWithWhite:0 alpha:0] setStroke];
    //    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(width / 2 - bigRadius, height / 2 - bigRadius, bigRadius * 2, bigRadius * 2) cornerRadius:bigRadius];
    //    [path fill];
    
    [_smallRoundedfillColor setFill];
    UIBezierPath *path1 = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(width / 2 - smallRadius, height / 2 - smallRadius, smallRadius * 2, smallRadius * 2) cornerRadius:smallRadius];
//    [path1 stroke];
    [path1 fill];
    
    
    [_minRoundedfileColor setFill];
//    [_minRoundedfileColor setStroke];
    CGPoint center = CGPointMake(width / 2, height / 2);
    UIBezierPath *path2 = [UIBezierPath bezierPath];
    [path2 moveToPoint:center];
    [path2 addLineToPoint:center];
    [path2 addArcWithCenter:center radius:smallRadius startAngle:self.endAngle endAngle:-M_PI_2 clockwise:YES];
    [path2 addLineToPoint:CGPointMake(width / 2, height / 2 - smallRadius)];
//    [path2 stroke];
    [path2 fill];
}


//- (void)playOrSuspendTapeGestureAction{
//    isPlay = !isPlay;
//    _displayLink.paused = !isPlay;
//    if (_playOrSuspendHandler) {
//        _playOrSuspendHandler(isPlay);
//    }
//    [self setNeedsDisplay];
//}

- (CGFloat)endAngle{
    //值为0~1之间，类似下载的处理
    //- M_PI_2 + M_PI * 2 * _currentProgress;
    //对于值可以无穷大，无限旋转,类似网络加载的处理
    //double modf (double, double*); 将参数的整数部分通过指针回传, 返回小数部分
    double integer;
    CGFloat endAngle = - M_PI_2 + M_PI * 2 * modf(_currentProgress, &integer);//(_currentProgress - (NSInteger)(_currentProgress));
    return endAngle;
}

- (void)setCurrentProgress:(CGFloat)currentProgress{
    _currentProgress = currentProgress;
    if ((NSInteger)currentProgress % 2 == 0) {
        // 真实的颜色
        self.smallRoundedfillColor = [UIColor clearColor];
        self.minRoundedfileColor = self.strokeColor;
    }else{
        self.minRoundedfileColor = self.strokeColor;
        self.smallRoundedfillColor = [UIColor whiteColor];
    }
    [self setNeedsDisplay];
}

//- (void)setProgress:(CGFloat)progress{
//    if (_progress != progress) {
//        //控制进度在0~1之间
//        //progress = MIN(MAX(progress, 0), 1);
//        _progress = progress;
//    }
//}

@end
