//
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/4/15.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "Home2CollectionViewCell.h"
#import "UIView+MJExtension.h"
#import "Tools.h"


@interface Home2CollectionViewCell ()



@end

@implementation Home2CollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setUpView];
    }
    return self;
}


- (void)setUpView{
    
    CGFloat imgX = 58; //图标左间距
    CGFloat imgY = 30;  //图标Y值
    
   // CGFloat badgeW = 20;//badge宽度
   // CGFloat badgeR = 20;//badge距离右侧
    //CGFloat badgeY = 20;//badge Y
    
    
    if (IS_IPHONE_5) {
       // badgeW = 16;
        //badgeY = 10;
        //badgeR = 13;
        
        imgX = 32* 3/4;
        imgY = 13;
        
        //self.badgeLabel.font = kFont10;
        
    }else if (IS_IPHONE_6){
       // badgeW = 20;
        //badgeY = 8;
        //badgeR = 20;
        
        imgX = 40* 3/4;
        imgY = 15;
        
        //self.badgeLabel.font = kFont12;
        
    }else if (IS_IPHONE_6Plus){
        //badgeW = 22;
        //badgeY = 10;
        //badgeR = 20;
        
        imgX = 45 * 3/4;
        imgY = 20;
    
        //self.badgeLabel.font = kFont12;
    }else if(IS_IPHONE_X) {
        
       // badgeW = 22;
        //badgeY = 15;
        //badgeR = 20;
        
        imgX = 45* 3/4;
        imgY = 20;
    }
    
//    NSLog(@"cellW : %f",self.mj_w);
    CGFloat imgW = self.mj_w - imgX * 2;
    
    self.imageView.frame = CGRectMake(imgX, imgY, imgW, imgW);
    
   // self.uploadImgView.frame = CGRectMake(CGRectGetMaxX(self.imageView.frame) , imgY - 8, 16, 16);
    
//    self.badgeLabel.frame = CGRectMake(self.mj_w - badgeR - badgeW, badgeY, badgeW, badgeW);
//    self.badgeLabel.layer.cornerRadius = badgeW * 0.5;
//    self.badgeLabel.layer.masksToBounds = YES;
    
   // self.cellBubbleView.frame = CGRectMake(self.mj_w - badgeR - badgeW, badgeY, badgeW, badgeW);
    
    CGFloat titleH = self.mj_h - CGRectGetMaxY(self.imageView.frame) - 5;
    self.label.frame = CGRectMake(0, CGRectGetMaxY(self.imageView.frame), self.mj_w, titleH);
    [self addSubview:self.imageView];
    
   // [self addSubview:self.uploadImgView];
   // [self addSubview:self.badgeLabel];
    //[self addSubview:self.cellBubbleView];

    [self addSubview:self.label];
    //self.imageView.backgroundColor = [UIColor greenColor];
    //self.label.backgroundColor = [UIColor redColor];
    
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (void)refreshUIWithImageName:(NSString *)imageName title:(NSString *)title{
    
    if ([Tools isBlankString:imageName]) {
        self.imageView.hidden = YES;
    }else{
        self.imageView.hidden = NO;
        [self.imageView setImage:IMAGE(imageName)];
    }
    
    [self.label setText:title];
    self.label.textColor = kColorTextBlack;
    self.label.font = [UIFont systemFontOfSize:14];
    if (IS_IPHONE_5) {
        self.label.font = [UIFont systemFontOfSize:13];
    }
}

- (void)refreshUIWithImageName:(NSString *)imageName title:(NSString *)title badgeLabelStr:(NSString *)badgeLabelStr{
    [self  refreshUIWithImageName:imageName title:title];
    
    if ([Tools isBlankString:badgeLabelStr]) {
       
        [_cellBubbleView hidenBubbleView];  // 隐藏红点标注
        
    }else{
     
        [_cellBubbleView showBubbleView];  //  显示红点标注
        if (badgeLabelStr.integerValue > 99) {
            //badgeLabelStr = @"99";
        }
        _cellBubbleView.unReadLabel.text = badgeLabelStr;
       
    }
    
    self.uploadImgView.hidden = YES;

    
}

-(UIImageView *)imageView
{
    if (!_imageView) {
        _imageView = [[UIImageView alloc]init];
    }
    return _imageView;
}


- (AYBubbleView *)cellBubbleView{
    
    
    if (!_cellBubbleView) {
        
       // CGFloat imgX = 58; //图标左间距
        //CGFloat imgY = 30;  //图标Y值
        
        CGFloat badgeW = 20;//badge宽度
        CGFloat badgeR = 20;//badge距离右侧
        CGFloat badgeY = 20;//badge Y
        
        
        if (IS_IPHONE_5) {
            badgeW = 15;
            badgeY = 10;
            badgeR = 13;
            
           // imgX = 32;
            //imgY = 13;
            
            //self.badgeLabel.font = kFont10;
            
        }else if (IS_IPHONE_6){
            badgeW = 15;
            badgeY = 8;
            badgeR = 20;
            
           // imgX = 40;
           // imgY = 15;
            
            //self.badgeLabel.font = kFont12;
            
        }else if (IS_IPHONE_6Plus){
            badgeW = 22;
            badgeY = 10;
            badgeR = 20;
            
            //imgX = 45;
           // imgY = 20;
            
            
            //self.badgeLabel.font = kFont12;
        }else if(IS_IPHONE_X) {
            
            badgeW = 22;
            badgeY = 15;
            badgeR = 20;
            
           // imgX = 45;
            //imgY = 20;
        }

        
        _cellBubbleView = [[AYBubbleView alloc] initWithCenterPoint:CGPointMake(self.mj_w - badgeR - badgeW + 10, badgeY + 5) bubleRadius:10 addToSuperView:self];
        
        //CGRectMake(self.mj_w - badgeR - badgeW, badgeY, badgeW, badgeW);
        
        _cellBubbleView.decayCoefficent = .2;  //  拉伸系数
        _cellBubbleView.unReadLabel.font = [UIFont systemFontOfSize:12];
        _cellBubbleView.bubbleColor = [UIColor redColor];
        [_cellBubbleView hidenBubbleView];  // 隐藏红点标注
        
        __weak typeof(self) weakSelf = self;
        _cellBubbleView.cleanMessageBlock = ^(BOOL isClean) {
            
            if (isClean) {
                // 让未读数消失
               // weakSelf.messageSteper.value = 0;
                if (weakSelf.removeCornerMarkBlock) {
                    weakSelf.removeCornerMarkBlock();
                }
                
                
            } else {
                
            }
        };


    }

    return _cellBubbleView;
    
    
}

-(void)setRemoveCornerMarkBlock:(RemoveCornerMarkBlock)removeCornerMarkBlock
{
    _removeCornerMarkBlock = removeCornerMarkBlock;
}

-(UIImageView *)uploadImgView
{
    if (!_uploadImgView) {
        _uploadImgView = [[UIImageView alloc]init];
        _uploadImgView.hidden = YES;
    }
    return _uploadImgView;
}

-(UILabel *)label{
    if (!_label) {
        _label = [[UILabel alloc]init];
        _label.textColor = kColorTextBlack;
        _label.textAlignment = NSTextAlignmentCenter;
    }
    return _label;
}

@end
