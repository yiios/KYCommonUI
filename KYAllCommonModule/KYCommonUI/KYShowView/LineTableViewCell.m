//
//  LineTableViewCell.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/6/30.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "LineTableViewCell.h"

@implementation LineTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setSeparatorInset:UIEdgeInsetsMake(0, kSCREEN_WIDTH, 0, 0)];  //上左下右
    
}

+ (UINib *)nib{
    return [UINib nibWithNibName:@"LineTableViewCell" bundle:nil];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
