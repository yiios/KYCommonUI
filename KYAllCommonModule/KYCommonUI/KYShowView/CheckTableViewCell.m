//
//  CheckTableViewCell.m
//  kyExpress
//
//  Created by 陈志刚 on 15/12/10.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import "CheckTableViewCell.h"

@interface CheckTableViewCell ()




@end

@implementation CheckTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
//    self.titleLabel.textColor = kColorTextBlack;
    self.titleLabel.font = [UIFont systemFontOfSize:14];
    
    if (IS_IPHONE_6Plus ) {
        self.contentLabel.font = [UIFont systemFontOfSize:14];

    }else if(IS_IPHONE_6){
        self.contentLabel.font = [UIFont systemFontOfSize:13];
        
    }else{
        self.contentLabel.font = [UIFont systemFontOfSize:12];

    }
    
   // self.contentLabel.font = [UIFont systemFontOfSize:12];

    self.backgroundColor = KMainBackColor;
    self.contentLabel.numberOfLines = 0;
    
//    self.contentLabel.text = @"1.当天达、次日达、隔日达、同城件、早班件、中班件、晚班件 抛重计费方式为: 长度(cm) x 宽度(cm) x 高度(cm) ÷ 6000 \n2.陆运件 抛重计费方式: 长度(cm) x 宽度(cm) x 高度(cm) ÷ 5000";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (UINib *)nib{
    return [UINib nibWithNibName:@"CheckTableViewCell" bundle:nil];
}

@end
