//
//  CheckTableViewCell.h
//  kyExpress
//
//  Created by 陈志刚 on 15/12/10.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIView *bgView;

+ (UINib *)nib;

@end
