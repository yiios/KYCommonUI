//
//  SelectDateTimeView.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/8/31.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "SelectDateTimeView.h"

@interface SelectDateTimeView()

@property (nonatomic, strong) NSDateFormatter *formatter;

@end

@implementation SelectDateTimeView

-(instancetype)init
{
    self = [super init];
    return self;
}




- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
//    [self setPicker];
    
}

- (void)showPicker{
  
    
    if (self.isSetPickerStyleDate) {
        [KIInstructTool shareInstructTool].pickType=PGDatePickerModeDate;
        [KIInstructTool shareInstructTool].maximumDate = [NSDate date];
        [KIInstructTool shareInstructTool].minimumDate=nil;
    }else{
        [KIInstructTool shareInstructTool].pickType=PGDatePickerModeDateHourMinute;
        if (!_noForce) {
            [KIInstructTool shareInstructTool].minimumDate = [NSDate date];
            [KIInstructTool shareInstructTool].maximumDate=nil;
        }
        
        if (self.isSetPickerstyleFlightDate == YES) {
            [KIInstructTool shareInstructTool].maximumDate = [NSDate date];
            [KIInstructTool shareInstructTool].minimumDate = nil;
        }
    }
    
    WS(weakSelf);
    [[KIInstructTool shareInstructTool] AlertNStimePickType:PGDatePickerModeDateHourMinute DefaultData:nil TimePickerClick:^(NSString *str, NSDate *date) {
        
        NSDate *date2 = [NSDate dateWithTimeInterval:60*60*8 sinceDate:date];//前一天

        _formatter.dateFormat =  @"yyyy-MM-dd HH:mm";
        NSString *dateStr = [NSString stringWithFormat:@"%@",date2];
        
        NSString *string0 = [dateStr substringWithRange:NSMakeRange (0, 16)];
        if (weakSelf.blockDidSelectDateTime) {
            weakSelf.blockDidSelectDateTime(string0);
        }
        
    }];
    
   
}

-(void)setBlockDidSelectDateTime:(BlockDidSelectDateTime)blockDidSelectDateTime
{
    _blockDidSelectDateTime = blockDidSelectDateTime;
}

- (NSDateFormatter *)formatter{
    if (!_formatter) {
        _formatter= [[NSDateFormatter alloc] init];
        [_formatter setDateStyle:NSDateFormatterMediumStyle];
        [_formatter setTimeStyle:NSDateFormatterShortStyle];
        
    }
    return _formatter;
}



@end
