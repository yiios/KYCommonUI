//
//  KYEButton.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/7/7.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BtnDidClickBlock)(void);

@interface KYEButton : UIButton
@property (nonatomic, copy)BtnDidClickBlock btnDidClickBlock;
- (instancetype)initWithTitle:(NSString *)title frame:(CGRect)frame;
-(void)setBtnDidClickBlock:(BtnDidClickBlock)btnDidClickBlock;

@end
