//
//  KYESegmentedControl.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/7/7.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "KYESegmentedControl.h"

#define segmentBackWidth  200
#define segmentBackHeight 40

@interface KYESegmentedControl()
@property (nonatomic,strong) NSArray *selectArray;

@end

@implementation KYESegmentedControl

- (instancetype)initWithSelectArray:(NSArray *)sArray
{
    self = [super initWithItems:sArray];
    self.bounds = CGRectMake(0, 0, segmentBackWidth, 28);
    
    self.layer.cornerRadius = 10;
    self.layer.borderWidth = 1;
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.clipsToBounds = YES;
    self.tintColor = [UIColor whiteColor];
    self.backgroundColor = KMain_Color;
    self.selectedSegmentIndex = 0;
    return self;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
}




@end
