//
//  SelectTimeTableViewCell.h
//  kyExpress
//
//  Created by 陈志刚 on 15/12/10.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface SelectTimeTableAndButtonViewCell : UITableViewCell


typedef void(^BlockToSelectTime)(NSString *string,NSDate *date);
typedef void(^BlockToDismiss)(void);

typedef void(^BlockBeginSelect)(void);
typedef void(^BlockDidSelect)(NSString *string);

//typedef void(^BlockToDateString)(NSString *string);

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *selectLabel;
@property (weak, nonatomic) IBOutlet UIImageView *selectImaeView;
@property (weak, nonatomic) IBOutlet UIButton *editButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageRightConstraint;
@property (assign, nonatomic) BOOL noForce;//允许选当前时间之前的时间



@property (nonatomic,assign) BOOL isSetPickerStyleDate;
@property (nonatomic,assign) BOOL isSetPickerstyleFlightDate;
@property (nonatomic,assign) BOOL isBaGun;

@property (nonatomic, copy)BlockToSelectTime blockToSelect;
@property (nonatomic, copy)BlockToDismiss blockToDismiss;

@property (nonatomic, copy)BlockDidSelect blockDidSelect;
@property (nonatomic, copy)BlockBeginSelect blockBeginSelect;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleRightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLeftConstraint;

//@property (nonatomic, copy)BlockToDateString blockToDateString;

+ (UINib *)nib;

- (void)isShowLocalTime:(BOOL)flag;

- (void)setBlockToSelect:(BlockToSelectTime)blockToSelect;
- (void)setBlockToDismiss:(BlockToDismiss)blockToDismiss;

- (void)setCellWithTitle:(NSString *)title placeHolder:(NSString *)placeholder
                  detail:(NSString *)detail
        blockBeginSelect:(BlockBeginSelect)blockBeginSelect
          blockDidSelect:(BlockDidSelect)blockDidSelect;

//- (void)setBlockToDateString:(BlockToDateString)blockToDateString;

//- (void)setPickerStyleDate;


@end
