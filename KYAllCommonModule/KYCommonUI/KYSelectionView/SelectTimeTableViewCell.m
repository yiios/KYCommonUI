//
//  SelectTimeTableViewCell.m
//  kyExpress
//
//  Created by 陈志刚 on 15/12/10.
//  Copyright © 2015年 kyExpress. All rights reserved.
//  AABBCC

#import "SelectTimeTableViewCell.h"
#import "Tools.h"


@interface SelectTimeTableViewCell ()


@property (nonatomic, strong) NSDateFormatter *formatter;
@property (weak, nonatomic) IBOutlet UIImageView *xingImage;
@property (unsafe_unretained, nonatomic) IBOutlet NSLayoutConstraint *xingImageLeftLayout;
@end

@implementation SelectTimeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self setXingImageHide:YES];
//    self.backgroundColor = KBack_Color;
    self.titleLabel.font = KTextFont;
    self.selectLabel.font = KTextFont;
    
//    self.titleLabel.textColor = kColorTextBlack;
    self.selectLabel.textColor = KTextGray;
    
    self.selectLabel.backgroundColor = KWhite_Color;
    self.titleLabel.backgroundColor  = KWhite_Color;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.selectImaeView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSelect)]];
    
    [self.selectLabel   addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSelect)]];
    
//    self.backgroundColor = KBack_Color;
    
}



- (void)setCellWithTitle:(NSString *)title placeHolder:(NSString *)placeholder
                  detail:(NSString *)detail
        blockBeginSelect:(BlockBeginSelect)blockBeginSelect
          blockDidSelect:(BlockDidSelect)blockDidSelect{
    
    [self.titleLabel setText:title];
    
//    IQActionSheetPickerStyleDateTimePicker;
    
    if ([Tools isBlankString:detail]) {
        
        if (![Tools isBlankString:placeholder]) {
            [self.selectLabel setText:placeholder];
            self.selectLabel.textColor = kColorPlaceHolder;
        }else{
            [self.selectLabel setText:@"请选择"];
            self.selectLabel.textColor = kColorPlaceHolder;
        }
    }else{
        [self.selectLabel setText:detail];
        self.selectLabel.textColor = kColorTextBlack;
    }
    
    self.blockDidSelect = blockDidSelect;
    self.blockBeginSelect = blockBeginSelect;
    
}

+ (UINib *)nib{
    return [UINib nibWithNibName:@"SelectTimeTableViewCell" bundle:nil];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setBlockToSelect:(BlockToSelectTime)blockToSelect{
    _blockToSelect = blockToSelect;
}
//- (void)setBlockToDateString:(BlockToDateString)blockToDateString{
//    _blockToDateString = blockToDateString;
//}


- (void)setBlockToDismiss:(BlockToDismiss)blockToDismiss{
    _blockToDismiss = [blockToDismiss copy];
}

- (void)onSelect{
    //键盘消失
    if (_blockToDismiss) {
        _blockToDismiss();
    }
    
    if (self.blockBeginSelect) {
        self.blockBeginSelect();
    }
    
    if (self.blockBeginSelect) {
        self.blockBeginSelect();
    }
    
    if (self.isSetPickerStyleDate) {
        [KIInstructTool shareInstructTool].pickType=PGDatePickerModeDate;

        [KIInstructTool shareInstructTool].maximumDate = [NSDate date];;

    }else{

        [KIInstructTool shareInstructTool].pickType=PGDatePickerModeDateHourMinute;
        if (!_noForce) {

            [KIInstructTool shareInstructTool].minimumDate = [NSDate date];
        }
        if(self.isBaGun){
            [KIInstructTool shareInstructTool].maximumDate=nil;
            [KIInstructTool shareInstructTool].minimumDate = nil;
        }
        if (self.isSetPickerstyleFlightDate == YES) {

            [KIInstructTool shareInstructTool].maximumDate=[NSDate date];
            [KIInstructTool shareInstructTool].minimumDate = nil;
        }
        if (self.isAddadvertisement == YES) {
            [KIInstructTool shareInstructTool].maximumDate=[self getCurrentDate];
             [KIInstructTool shareInstructTool].minimumDate = nil;

        }
        
    }
    
//    [picker show];
    WS(weakSelf);
    [[KIInstructTool shareInstructTool]AlertNStimePickType:[KIInstructTool shareInstructTool].pickType DefaultData:nil TimePickerClick:^(NSString *str, NSDate *date) {
        
        weakSelf.selectLabel.textColor = [UIColor blackColor];
//        [weakSelf.formatter setDateStyle:NSDateFormatterMediumStyle];
//        [weakSelf.formatter setTimeStyle:NSDateFormatterShortStyle];
        
        weakSelf.selectLabel.text = str;//[weakSelf.formatter stringFromDate:date];
        if (!weakSelf.isSetPickerStyleDate) {
            weakSelf.formatter.dateFormat = @"yyyy-MM-dd";
            NSString *string1 = [weakSelf.formatter stringFromDate:date];
            
            weakSelf.formatter.dateFormat = @"HH:mm:ss";
            if (weakSelf.isSetPickerstyleFlightDate == YES) {
                weakSelf.formatter.dateFormat = @"HH:mm:ss";
            }
            NSString *string2 = [_formatter stringFromDate:date];
            
            NSString *str =[NSString stringWithFormat:@"%@T%@",string1,string2];
            if (weakSelf.blockToSelect) {
                weakSelf.blockToSelect(str,date);
            }
        }else{
            weakSelf.formatter.dateFormat = @"yyyy-MM-dd";
            NSString *string1 = [weakSelf.formatter stringFromDate:date];
            weakSelf.selectLabel.text = string1;
            if (weakSelf.blockToSelect) {
                weakSelf.blockToSelect(string1,date);
            }
        }
        
        if (weakSelf.blockDidSelect) {
        
            
            weakSelf.formatter.dateFormat =  @"yyyy-MM-dd HH:mm";
            NSString *string0 = [weakSelf.formatter stringFromDate:date];
            
            weakSelf.blockDidSelect(string0);
        }
        
        
    }];
    
}


// 设置星号是否隐藏
- (void)setXingImageHide:(BOOL)isHide {
    self.xingImage.hidden = isHide;
    
    if (isHide == YES) {
        self.xingImageLeftLayout.constant = 2;
    } else {
        self.xingImageLeftLayout.constant = 15;
    }
}

#pragma  mark －获取日期
- (NSDate *)getCurrentDate{
    NSDate * senddate= [NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"yyyy-MM-dd"]; //HH:mm
    NSString *string = [NSString stringWithFormat:@"%@ 23:59",[dateformatter stringFromDate:senddate]];
    
    NSDateFormatter  *dateformatter2=[[NSDateFormatter alloc] init];
    [dateformatter2 setDateFormat:@"yyyy-MM-dd HH:mm"];
    return [dateformatter2 dateFromString:string];
}



- (void)isShowLocalTime:(BOOL)flag{
    
    if (flag) {
        [_formatter setDateStyle:NSDateFormatterMediumStyle];
        [_formatter setTimeStyle:NSDateFormatterShortStyle];
        self.selectLabel.text = [self.formatter stringFromDate:[NSDate date]];
    }else{
        self.selectLabel.text = @"";
    }
    
}

- (NSDateFormatter *)formatter{
    if (!_formatter) {
        _formatter= [[NSDateFormatter alloc] init];
        [_formatter setDateStyle:NSDateFormatterMediumStyle];
        [_formatter setTimeStyle:NSDateFormatterShortStyle];
    }
    return _formatter;
}



@end
