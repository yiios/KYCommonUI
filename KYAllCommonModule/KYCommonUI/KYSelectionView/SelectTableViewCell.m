//
//  SelectTableViewCell.m
//  kyExpress
//
//  Created by 陈志刚 on 15/12/9.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import "SelectTableViewCell.h"

#import "Tools.h"

@interface SelectTableViewCell ()



@property (weak, nonatomic) IBOutlet UIImageView *xingImage;
@property (unsafe_unretained, nonatomic) IBOutlet NSLayoutConstraint *xingImageLeftLayout;

@property (nonatomic, copy) NSArray *titleArray;
@property (nonatomic, strong)NSArray * titleArr;

@end


@implementation SelectTableViewCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    //    self.backgroundColor = KBack_Color;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.selectLabel.textColor = KTextGray;
    self.titleLabel.textColor  = kColorLabel;
    
    self.selectLabel.backgroundColor = KWhite_Color;
    self.titleLabel.backgroundColor  = KWhite_Color;
    
    self.titleLabel.font = KTextFont;
    self.selectLabel.font = KTextFont;
    
    
        [self.selectImaeView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSelect)]];
        [self.selectLabel    addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSelect)]];
    
    [self setXingImageHide:YES];
    self.partingLine.hidden = YES;
    
}

// 设置星号是否隐藏
- (void)setXingImageHide:(BOOL)isHide {
    self.xingImage.hidden = isHide;
    
    if (isHide == YES) {
        self.xingImageLeftLayout.constant = 2;
    } else {
        self.xingImageLeftLayout.constant = 15;
    }
}


- (void)refreshUIWithTitle:(NSString *)string {
    [self.titleLabel setText:string];
}

- (void)setCellWithTitle:(NSString *)title placeHolder:(NSString *)placeholder detail:(NSString *)detail selectArray:(NSArray *)selectArray
  shouldBeginSelectBlock:(ShouldBeginSelectBlock)shouldBeginSelectBlock
         didDismissBlock:(DidDismissBlock)didDismissBlock
          didSelectBlock:(DidSelectBlock)didSelectBlock{
    
    [self.titleLabel setText:title];
    
    if ([Tools isBlankString:detail]) {
        
        if (![Tools isBlankString:placeholder]) {
            [self.selectLabel setText:placeholder];
            self.selectLabel.textColor = kColorPlaceHolder;
        }else{
            [self.selectLabel setText:@"请选择"];
            self.selectLabel.textColor = kColorPlaceHolder;
        }
    }else{
        [self.selectLabel setText:detail];
        self.selectLabel.textColor = kColorTextBlack;
    }
    
    self.titleArr = selectArray;
    
    _shouldBeginSelectBlock = shouldBeginSelectBlock;
    _didDismissBlock = didDismissBlock;
    _didSelectBlock = didSelectBlock;
    
}


- (void)setPickerToTitles:(NSArray *)titleArray{
    _titleArr = titleArray;
//    [self.picker setTitlesForComponents:[NSArray arrayWithObjects:titleArray, nil]];
}



//增加弹出框灵敏度
- (IBAction)beginSelect:(id)sender {
    if (_shouldBeginSelectBlock) {
        _shouldBeginSelectBlock();
    }
    
    if (_shouldBeginClick) {
        _shouldBeginClick();
    }else{
        [self onSelect];
    }
}

- (void)onSelect{
    
    if (_blockClick) {
        _blockClick();
    }
    
   
    if (_titleArr.count != 0) {
        WS(weakSelf);
        [[KIInstructTool shareInstructTool] AlertPickerArray:_titleArr TimePickerClick:^(NSString *str, NSDate *date) {
            if (weakSelf.didSelectBlock){
                weakSelf.didSelectBlock(str);
            }
            
            if (weakSelf.blockToSelect) {
                weakSelf.blockToSelect(str);
            }
            weakSelf.selectLabel.text = str;
            weakSelf.selectLabel.textColor = kColorTextBlack;
        }];
    }
    
    if (_didDismissBlock) {
        _didDismissBlock();
    }
    
    if (_blockToDismiss) {
        _blockToDismiss();
    }
}

+ (UINib *)nib{
    return [UINib nibWithNibName:@"SelectTableViewCell" bundle:nil];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setBlockToSelect:(BlockToSelect)blockToSelect{
    _blockToSelect = [blockToSelect copy];
}

- (void)setBlockToDismiss:(BlockToDismiss)blockToDismiss{
    _blockToDismiss = [blockToDismiss copy];
    
}

- (void)setBlockClick:(BlockClick)blockClick{
    _blockClick = blockClick;
}

@end
