//
//  KYEButton.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/7/7.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "KYEButton.h"


@implementation KYEButton

- (instancetype)initWithTitle:(NSString *)title frame:(CGRect)frame{
    self = [super initWithFrame:frame];
    self.backgroundColor = KMain_Color;
    [self setTitle:title forState:UIControlStateNormal];
    [self setTitleColor:KWhite_Color forState:UIControlStateNormal];
    [self setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    self.layer.cornerRadius = 5;
    self.clipsToBounds = YES;
    [self addTarget:self action:@selector(btnOnClick) forControlEvents:UIControlEventTouchUpInside];
    return self;
}

- (void)btnOnClick{
    
    if (self.btnDidClickBlock) {
        self.btnDidClickBlock();
    }
}

-(void)setBtnDidClickBlock:(BtnDidClickBlock)btnDidClickBlock
{
    _btnDidClickBlock = btnDidClickBlock;
}

- (void)drawRect:(CGRect)rect {
    // Drawing code
}


@end
