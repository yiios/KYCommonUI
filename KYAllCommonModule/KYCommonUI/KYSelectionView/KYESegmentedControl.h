//
//  KYESegmentedControl.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/7/7.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface KYESegmentedControl : UISegmentedControl

- (instancetype)initWithSelectArray:(NSArray *)sArray;

@end
