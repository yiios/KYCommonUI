//
//  SelectTableViewCell.h
//  kyExpress
//
//  Created by 陈志刚 on 15/12/9.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^BlockToSelect)(NSString *string);
typedef void(^BlockToDismiss)(void);
typedef void(^BlockClick)(void);


typedef void(^ShouldBeginSelectBlock)(void);
typedef void(^DidSelectBlock)(NSString *string);
typedef void(^DidDismissBlock)(void);
typedef void(^ShouldBeginClick)(void);

@interface SelectTableViewCell : UITableViewCell

@property (nonatomic, copy) BlockToSelect blockToSelect;
@property (nonatomic, copy) BlockToDismiss blockToDismiss;
@property (nonatomic, copy) BlockClick blockClick;

@property (nonatomic, copy) ShouldBeginSelectBlock shouldBeginSelectBlock;
@property (nonatomic, copy) DidSelectBlock didSelectBlock;
@property (nonatomic, copy) DidDismissBlock didDismissBlock;
@property (nonatomic, copy) ShouldBeginClick shouldBeginClick;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *selectLabel;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;
@property (weak, nonatomic) IBOutlet UIImageView *selectImaeView;
@property (weak, nonatomic) IBOutlet UIView *partingLine;

- (void)setCellWithTitle:(NSString *)title placeHolder:(NSString *)placeholder detail:(NSString *)detail selectArray:(NSArray *)selectArray
  shouldBeginSelectBlock:(ShouldBeginSelectBlock)shouldBeginSelectBlock
         didDismissBlock:(DidDismissBlock)didDismissBlock
          didSelectBlock:(DidSelectBlock)didSelectBlock;

- (void)refreshUIWithTitle:(NSString *)string;
- (void)setPickerToTitles:(NSArray *)titleArray;

// 设置星号是否隐藏
- (void)setXingImageHide:(BOOL)isHide;

+ (UINib *)nib;
- (void)setBlockToSelect:(BlockToSelect)blockToSelect;
- (void)setBlockToDismiss:(BlockToDismiss)blockToDismiss;

@end

