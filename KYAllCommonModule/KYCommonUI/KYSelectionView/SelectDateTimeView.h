//
//  SelectDateTimeView.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/8/31.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BlockDidSelectDateTime)(NSString *string);
@interface SelectDateTimeView : UIView


@property (nonatomic,assign) BOOL noForce;//允许选当前时间之前的时间
@property (nonatomic,assign) BOOL isSetPickerStyleDate;
@property (nonatomic,assign) BOOL isSetPickerstyleFlightDate;

@property (nonatomic, copy)BlockDidSelectDateTime blockDidSelectDateTime;

- (void)showPicker;
-(void)setBlockDidSelectDateTime:(BlockDidSelectDateTime)blockDidSelectDateTime;
@end
