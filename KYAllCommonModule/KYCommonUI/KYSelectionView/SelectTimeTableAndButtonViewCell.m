//
//  SelectTimeTableViewCell.m
//  kyExpress
//
//  Created by 陈志刚 on 15/12/10.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import "SelectTimeTableAndButtonViewCell.h"
#import "Tools.h"

@interface SelectTimeTableAndButtonViewCell ()


@property (nonatomic, strong) NSDateFormatter *formatter;

@end

@implementation SelectTimeTableAndButtonViewCell

- (void)awakeFromNib {
    // Initialization code
    
    [super awakeFromNib];
//    self.backgroundColor = KBack_Color;
    self.titleLabel.font = KTextFont;
    self.selectLabel.font = KTextFont;
    
//    self.titleLabel.textColor = kColorTextBlack;
    self.selectLabel.textColor = KTextGray;
    
    self.selectLabel.backgroundColor = KWhite_Color;
    self.titleLabel.backgroundColor  = KWhite_Color;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self.selectImaeView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSelect)]];
    
    [self.selectLabel   addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSelect)]];
    
    
    self.editButton.layer.masksToBounds = YES;
    self.editButton.layer.cornerRadius = 5;
    self.editButton.titleLabel.font = STextFont;
    [self.editButton setBackgroundColor:KMain_Color];
    [self.editButton setTitle:@"编辑" forState:UIControlStateNormal];
    

    
//    self.backgroundColor = KBack_Color;
    
}

- (void)setCellWithTitle:(NSString *)title placeHolder:(NSString *)placeholder
                  detail:(NSString *)detail
        blockBeginSelect:(BlockBeginSelect)blockBeginSelect
          blockDidSelect:(BlockDidSelect)blockDidSelect{
    
    [self.titleLabel setText:title];
    
//    IQActionSheetPickerStyleDateTimePicker;
    
    if ([Tools isBlankString:detail]) {
        
        if (![Tools isBlankString:placeholder]) {
            [self.selectLabel setText:placeholder];
            self.selectLabel.textColor = kColorPlaceHolder;
        }else{
            [self.selectLabel setText:@"请选择"];
            self.selectLabel.textColor = kColorPlaceHolder;
        }
    }else{
        [self.selectLabel setText:detail];
        self.selectLabel.textColor = kColorTextBlack;
    }
    
    self.blockDidSelect = blockDidSelect;
    self.blockBeginSelect = blockBeginSelect;
    
}

+ (UINib *)nib{
    return [UINib nibWithNibName:@"SelectTimeTableAndButtonViewCell" bundle:nil];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setBlockToSelect:(BlockToSelectTime)blockToSelect{
    _blockToSelect = blockToSelect;
}
//- (void)setBlockToDateString:(BlockToDateString)blockToDateString{
//    _blockToDateString = blockToDateString;
//}


- (void)setBlockToDismiss:(BlockToDismiss)blockToDismiss{
    _blockToDismiss = [blockToDismiss copy];
}
- (void)onSelect{
    //键盘消失
    if (_blockToDismiss) {
        _blockToDismiss();
    }
    
    if (self.blockBeginSelect) {
        self.blockBeginSelect();
    }
    
    if (self.blockBeginSelect) {
        self.blockBeginSelect();
    }
    
    
    [KIInstructTool shareInstructTool].pickType=PGDatePickerModeDateHourMinute;
    if (self.isSetPickerStyleDate) {
       [KIInstructTool shareInstructTool].pickType=PGDatePickerModeDate;
        //picker.maximumDate = [NSDate date];
    }else{
        [KIInstructTool shareInstructTool].pickType=PGDatePickerModeDateHourMinute;
        if (!_noForce) {
            [KIInstructTool shareInstructTool].minimumDate = [NSDate date];
             [KIInstructTool shareInstructTool].maximumDate = nil;
        }
        if(self.isBaGun){
            [KIInstructTool shareInstructTool].maximumDate = nil;
            [KIInstructTool shareInstructTool].minimumDate = nil;
        }
        if (self.isSetPickerstyleFlightDate == YES) {
            [KIInstructTool shareInstructTool].maximumDate = [NSDate date];
            [KIInstructTool shareInstructTool].minimumDate = nil;
        }
    }
    
    WS(weakSelf);
    [[KIInstructTool shareInstructTool] AlertNStimePickType:[KIInstructTool shareInstructTool].pickType DefaultData:nil TimePickerClick:^(NSString *str, NSDate *date) {
        [_formatter setDateStyle:NSDateFormatterMediumStyle];
        [_formatter setTimeStyle:NSDateFormatterShortStyle];
        weakSelf.selectLabel.text = [weakSelf.formatter stringFromDate:date];
        weakSelf.selectLabel.textColor = [UIColor blackColor];
        
        if (!weakSelf.isSetPickerStyleDate) {
            weakSelf.formatter.dateFormat = @"yyyy-MM-dd";
            NSString *string1 = [_formatter stringFromDate:date];
            
            weakSelf.formatter.dateFormat = @"HH:mm:ss";
            if (weakSelf.isSetPickerstyleFlightDate == YES) {
                weakSelf.formatter.dateFormat = @"HH:mm:ss";
            }
            NSString *string2 = [_formatter stringFromDate:date];
            
            NSString *str =[NSString stringWithFormat:@"%@T%@",string1,string2];
            if (weakSelf.blockToSelect) {
                
                weakSelf.blockToSelect(str,date);
            }
        }else{
            weakSelf.formatter.dateFormat = @"yyyy-MM-dd";
            NSString *string1 = [weakSelf.formatter stringFromDate:date];
            weakSelf.selectLabel.text = string1;
            if (weakSelf.blockToSelect) {
                weakSelf.blockToSelect(string1,date);
            }
        }
        
        if (weakSelf.blockDidSelect) {
            
            NSLog(@"date : %@",date);
            
            weakSelf.formatter.dateFormat =  @"yyyy-MM-dd HH:mm";
            NSString *string0 = [weakSelf.formatter stringFromDate:date];
            
            NSLog(@"string0 : %@ ",string0);
            
            weakSelf.blockDidSelect(string0);
        }
    }];
    
}




- (void)isShowLocalTime:(BOOL)flag{
    
    if (flag) {
        [_formatter setDateStyle:NSDateFormatterMediumStyle];
        [_formatter setTimeStyle:NSDateFormatterShortStyle];
        self.selectLabel.text = [self.formatter stringFromDate:[NSDate date]];
    }else{
        self.selectLabel.text = @"";
    }
    
}

- (NSDateFormatter *)formatter{
    if (!_formatter) {
        _formatter= [[NSDateFormatter alloc] init];
        [_formatter setDateStyle:NSDateFormatterMediumStyle];
        [_formatter setTimeStyle:NSDateFormatterShortStyle];
    }
    return _formatter;
}



@end
