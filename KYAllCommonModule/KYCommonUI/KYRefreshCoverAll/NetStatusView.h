//
//  NetStatusView.h
//  CustomView
//
//  Created by iOS_Chris on 17/3/23.
//  Copyright © 2017年 iOS_Chris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NetStatusView : UIView


//网络请求失败
- (void)setNetErrorStyle;
//服务器无响应
- (void)setNoResponseStyle;


@end
