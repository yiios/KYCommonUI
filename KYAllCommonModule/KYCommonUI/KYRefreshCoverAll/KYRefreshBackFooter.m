//
//  KYRefreshBackFooter.m
//  kyExpress_Internal
//
//  Created by MelissaShu on 17/4/21.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "KYRefreshBackFooter.h"
#import "KYRefreshComponent.h"
#import "Tools.h"

@interface KYRefreshBackFooter()
@property (assign, nonatomic) NSInteger lastRefreshCount;
@property (assign, nonatomic) CGFloat lastBottomDelta;

@property (assign, nonatomic) NSInteger screenNum;  //一屏显示的数量
@property (assign, nonatomic) NSInteger pageNum; //一页请求的数据个数

@end

@implementation KYRefreshBackFooter


#pragma mark - 初始化
- (void)willMoveToSuperview:(UIView *)newSuperview
{
    [super willMoveToSuperview:newSuperview];
    
    [self scrollViewContentSizeDidChange:nil];
}

- (void)setFooterScreenNum5:(NSInteger)screenNum5  screenNum6:(NSInteger)screenNum6  screenNum6p:(NSInteger)screenNum6p{
    
    
    if (IS_IPHONE_5) {
        self.screenNum = screenNum5;
    }else if(IS_IPHONE_6){
        self.screenNum = screenNum6;
    }else if(IS_IPHONE_6Plus){
        self.screenNum = screenNum6p;
    }
    
}


#pragma mark - 实现父类的方法
- (void)scrollViewContentOffsetDidChange:(NSDictionary *)change
{
    [super scrollViewContentOffsetDidChange:change];
    
    WS(weakSelf);
    // 如果正在刷新，直接返回
    if (self.state == MJRefreshStateRefreshing) return;
    
    // 拦截上拉刷新，如果内容不足一屏，就不能上拉刷新
    if ([self heightForContentBreakView] < 0) {
        self.state = MJRefreshStateIdle;
        return;
    }
    // 如果外界禁止了上拉刷新，就返回
    if (self.isProhibitFooter)                  return;
    
    /******************* 自动执行回调******************/
    [self autoExecBlockWithChange:change];
    /******************* 自动执行回调******************/
    
    _scrollViewOriginalInset = self.scrollView.contentInset;
    
    // 当前的contentOffset
    CGFloat currentOffsetY = self.scrollView.mj_offsetY;
    // 尾部控件刚好出现的offsetY
    CGFloat happenOffsetY = [self happenOffsetY];
    // 如果是向下滚动到看不见尾部控件，直接返回
    if (currentOffsetY <= happenOffsetY) return;
    
    CGFloat pullingPercent = (currentOffsetY - happenOffsetY) / self.mj_h;
    
    //    // 如果已全部加载，仅设置pullingPercent，然后返回
    //    if (self.state == MJRefreshStateNoMoreData) {
    //        self.pullingPercent = pullingPercent;
    //        return;
    //    }
    
    // 普通 和 即将刷新 的临界点
    CGFloat normal2pullingOffsetY = happenOffsetY + self.mj_h;
    
    if (self.scrollView.isDragging) {
        self.pullingPercent = pullingPercent;
        
        if (self.state == MJRefreshStateIdle) {
            self.state = MJRefreshStateDraging;
        }
        
        if (self.state == MJRefreshStateDraging && currentOffsetY > normal2pullingOffsetY) {
            // 转为即将刷新状态
            self.state = MJRefreshStatePulling;
            // 在这里算成了88
            
        } else if (self.state == MJRefreshStatePulling && currentOffsetY <= normal2pullingOffsetY) {
            // 转为普通状态
            self.state = MJRefreshStateIdle;
            
        }
    } else if (self.state == MJRefreshStatePulling) {// 即将刷新 && 手松开
        // 开始刷新
        [self beginRefreshing];
        
        // 无更多数据
    } else if (self.state == MJRefreshStateNoMoreData &&
               currentOffsetY > normal2pullingOffsetY) {
        // 无更多数据的停留
        self.state = KYRefreshStateStop;
        
    } else if (pullingPercent < 1) {
        
        self.pullingPercent = pullingPercent;
    }
}

- (void)scrollViewContentSizeDidChange:(NSDictionary *)change
{
    [super scrollViewContentSizeDidChange:change];
    
    // 内容的高度
    CGFloat contentHeight = self.scrollView.mj_contentH + self.ignoredScrollViewContentInsetBottom;
    // 表格的高度
    CGFloat scrollHeight = self.scrollView.mj_h - self.scrollViewOriginalInset.top - self.scrollViewOriginalInset.bottom + self.ignoredScrollViewContentInsetBottom;
    // 设置位置和尺寸
    self.mj_y = MAX(contentHeight, scrollHeight);
    
}

- (void)setState:(MJRefreshState)state
{
    MJRefreshCheckState
    
    //    logSize(self.scrollView)
    //    logState(@"footerNow", self.state)
    //    logState(@"foorerOld", oldState)
    
    // 无更多数据状态->无更多数据状态下的停留和合拢
    if (self.state == KYRefreshStateStop &&
        oldState == MJRefreshStateNoMoreData) {
        [self stopWithState:KYRefreshStateStop];       // 停留
        return;
    }
    // 正在刷新->无更多数据状态下的合拢
    if (state == MJRefreshStateNoMoreData &&
        oldState == MJRefreshStateRefreshing) {
        [self stopWithState:MJRefreshStateNoMoreData]; // 停留
        return;
    }
    
    // 根据状态来设置属性
    if (state == MJRefreshStateIdle) {
        
        if (oldState == MJRefreshStateRefreshing ||
            oldState == KYRefreshStateSuccess    ||
            oldState == KYRefreshStateFail       ||
            oldState == KYRefreshStateNoResponse ||
            oldState == KYRefreshStateNoNetwork ||
            oldState == MJRefreshStateNoMoreData ) {
            
            //解决合拢,合拢时间为1.0s
            [UIView animateWithDuration:KYRefreshSlowAnimationDuration animations:^{
                //                self.scrollView.mj_insetB -= self.lastBottomDelta;
                self.scrollView.mj_insetB = 0;
                // 自动调整透明度
                if (self.isAutomaticallyChangeAlpha) self.alpha = 0.0;
            } completion:^(BOOL finished) {
                self.pullingPercent = 0.0;
            }];
        }
        
        CGFloat deltaH = [self heightForContentBreakView];
        // 刚刷新完毕
        if (MJRefreshStateRefreshing == oldState && deltaH > 0 && self.scrollView.mj_totalDataCount != self.lastRefreshCount) {
            self.scrollView.mj_offsetY = self.scrollView.mj_offsetY;
        }
        // 正在刷新
    } else if (state == MJRefreshStateRefreshing) {
        
        // 记录刷新前的数量
        self.lastRefreshCount = self.scrollView.mj_totalDataCount;
        
        [UIView animateWithDuration:MJRefreshFastAnimationDuration animations:^{
            CGFloat bottom = self.mj_h + self.scrollViewOriginalInset.bottom;
            CGFloat deltaH = [self heightForContentBreakView];
            if (deltaH < 0) { // 如果内容高度小于view的高度
                bottom -= deltaH;
            }
            self.lastBottomDelta = bottom - self.scrollView.mj_insetB;
            if (bottom != 44)   bottom = 44;
            self.scrollView.mj_insetB = bottom;
            self.scrollView.mj_offsetY = [self happenOffsetY] + self.mj_h;
            //            NSLog(@"RefreshingoffsetY:%f,happenOffsetY:%f--height:%f",self.scrollView.mj_offsetY,[self happenOffsetY],self.mj_h);
        } completion:^(BOOL finished) {
            [self executeRefreshingCallback];
        }];
        
    }
    //FIXME: 由MJRefreshStateNoMoreData状态不初始化为MJRefreshStateIdle的bug，不知道在哪里的计算，把mj_insetB算成-44，在此矫正
    if (self.scrollView.mj_insetB == -44) {
        self.scrollView.mj_insetB = 0;
    }
    
}

#pragma mark - 合拢
- (void)closureWithState:(MJRefreshState)state {
    
    CGFloat closureTime = MJRefreshSlowAnimationDuration;
    // 无更多数据时间为1.0
    if (state == MJRefreshStateNoMoreData) closureTime = 1.0;
    //合拢
    [UIView animateWithDuration:closureTime animations:^{
        
        self.scrollView.mj_insetB = 0;
        // 自动调整透明度
        if (self.isAutomaticallyChangeAlpha) self.alpha = 0.0;
        
    } completion:^(BOOL finished) {
        
        self.pullingPercent = 0.0;
    }];
}

#pragma mark - 公共方法
- (void)endRefreshing
{
    if ([self.scrollView isKindOfClass:[UICollectionView class]]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [super endRefreshing];
        });
    } else {
        [super endRefreshing];
    }
}

- (void)noticeNoMoreData
{
    if ([self.scrollView isKindOfClass:[UICollectionView class]]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [super noticeNoMoreData];
        });
    } else {
        [super noticeNoMoreData];
    }
}

#pragma mark - 私有方法
#pragma mark 获得scrollView的内容 超出 view 的高度
- (CGFloat)heightForContentBreakView
{
    CGFloat h = self.scrollView.frame.size.height - self.scrollViewOriginalInset.bottom - self.scrollViewOriginalInset.top;
    return self.scrollView.contentSize.height - h;
}

#pragma mark 刚好看到上拉刷新控件时的contentOffset.y
- (CGFloat)happenOffsetY
{
    CGFloat deltaH = [self heightForContentBreakView];
    if (deltaH > 0) {
        return deltaH - self.scrollViewOriginalInset.top;
    } else {
        return - self.scrollViewOriginalInset.top;
    }
}
#pragma mark - 单纯做停留，不做网络请求
- (void)stopWithState:(MJRefreshState)state {
    
    [UIView animateWithDuration:MJRefreshFastAnimationDuration animations:^{
        // scrollViewOriginalInset是88
        CGFloat bottom = self.mj_h + self.scrollViewOriginalInset.bottom;
        CGFloat deltaH = [self heightForContentBreakView];
        if (deltaH < 0) { // 如果内容高度小于view的高度
            bottom -= deltaH;
        }
        
        self.lastBottomDelta = bottom - self.scrollView.mj_insetB;
        //FIXME: bottom算成了88，在此矫正
        if (bottom != 44) bottom = 44;
        //        if (bottom == 88) bottom = 44;
        self.scrollView.mj_insetB = bottom;
        self.scrollView.mj_offsetY = [self happenOffsetY] + self.mj_h;
        
    } completion:^(BOOL finished) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.65 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            // 合拢
            [self closureWithState:state];
            // stop后恢复无更多数据
            if (state == KYRefreshStateStop) {
                self.state = MJRefreshStateNoMoreData;
            }
        });
    }];
}
#pragma mark - 自动执行回调
- (void)autoExecBlockWithChange:(NSDictionary *)change {
    
    if (self.isRefreshing)                          return;
    
    // NSConcreteValue对象（NSValue）
    id new = change[@"new"];
    CGPoint newPoint = [new CGPointValue];
    CGFloat height = self.scrollView.frame.size.height;
    CGFloat contentOffsety = newPoint.y;
    CGFloat distance = self.scrollView.contentSize.height - height;
    CGFloat offDistance = distance- contentOffsety;
    
    if (offDistance <= -10) {   // 刚好到底部再偏移一点
        // 拦截上拉刷新，如果内容不足一屏，就不能上拉刷新
        if ([self heightForContentBreakView] < 0)   return;
        // 监控触摸事件，手指触摸的时候，就不自动触发刷新
        if (self.scrollView.isTracking == YES)      return;
        // 一下几种情况不继续做自动刷新网络请求，页面会很卡顿
        if (self.state == MJRefreshStateNoMoreData ||
            self.state == KYRefreshStateStop       ||
            self.state == KYRefreshStateNoResponse ||
            self.state == KYRefreshStateNoNetwork  ||
            self.state == KYRefreshStateFail       ||
            self.state == KYRefreshStateSuccess)    return;
        if ([Tools isNoNetwork])                    return;
        // 为了有小人跑的动画，做刷新
        [self beginRefreshing];
    }
}
/*
 // 刷新成功
 上拉状态->普通松开就可以进行刷新的状态->正在刷新中状态->请求成功状态->普通闲置状态
 // 无网络
 上拉状态->普通松开就可以进行刷新的状态->正在刷新中的状态->无网络状态->普通闲置状态
 
 */
@end
