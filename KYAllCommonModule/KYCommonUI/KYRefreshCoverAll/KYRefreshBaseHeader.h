//
//  KYRefreshBaseHeader.h
//  kyExpress_Internal
//
//  Created by MelissaShu on 17/4/21.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "MJRefreshComponent.h"

@interface KYRefreshBaseHeader : MJRefreshComponent


/** 这个key用来存储上一次下拉刷新成功的时间 */
@property (copy, nonatomic) NSString *lastUpdatedTimeKey;
/** 上一次下拉刷新成功的时间 */
@property (strong, nonatomic, readonly) NSDate *lastUpdatedTime;

/** 忽略多少scrollView的contentInset的top */
@property (assign, nonatomic) CGFloat ignoredScrollViewContentInsetTop;

@property (nonatomic, assign) BOOL isNotExecute; //执行刷新方法

//--- wnagkai   重新加载数据的回调
//@property (nonatomic, copy) ReloadDataBlock reloadDataBlock;
//
//- (void)setReloadDataBlock:(ReloadDataBlock)reloadDataBlock;

/** 结束刷新,刷入对应状态显示对应提示 */
- (void)endRefreshingWithState:(MJRefreshState)state;
- (void)endRefreshingFailureWithError:(NSError *)error;



/** 判断是否已经显示头部无网络视图 */
@property (nonatomic, assign) BOOL isDisplayHeader;

@end
