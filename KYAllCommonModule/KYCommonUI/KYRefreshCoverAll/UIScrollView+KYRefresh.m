//
//  UIScrollView+KYRefresh.m
//  kyExpress_Internal
//
//  Created by MelissaShu on 17/4/21.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "UIScrollView+KYRefresh.h"
#import <objc/runtime.h>
#import "KYRefreshFooter.h"
#import "KYRefreshHeader.h"

@implementation UIScrollView (KYRefresh)

//static const char KYRefreshHeaderKey = '\0';
//static const char KYRefreshFooterKey = '\0';

static char const * const KYRefreshHeaderKey = "KYRefreshHeaderKey";
static char const * const KYRefreshFooterKey = "KYRefreshFooterKey";

-(KYRefreshHeader *)ky_header
{
    
    return objc_getAssociatedObject(self, &KYRefreshHeaderKey);
}

-(KYRefreshFooter *)ky_footer
{
    
    return objc_getAssociatedObject(self, &KYRefreshFooterKey);
}



-(void)setKy_header:(KYRefreshHeader *)ky_header
{
    if (ky_header != self.ky_header) {
        // 删除旧的，添加新的
        [self.ky_header removeFromSuperview];
        [self insertSubview:ky_header atIndex:0];
        
        // 存储新的
        [self willChangeValueForKey:@"ky_header"]; // KVO
        objc_setAssociatedObject(self, &KYRefreshHeaderKey,
                                 ky_header, OBJC_ASSOCIATION_ASSIGN);
        [self didChangeValueForKey:@"ky_header"]; // KVO
    }
}


-(void)setKy_footer:(KYRefreshFooter *)ky_footer
{
    if (ky_footer != self.ky_footer) {
        // 删除旧的，添加新的
        [self.ky_footer removeFromSuperview];
        [self insertSubview:ky_footer atIndex:0];
        
        // 存储新的
        [self willChangeValueForKey:@"ky_footer"]; // KVO
        objc_setAssociatedObject(self, &KYRefreshFooterKey,
                                 ky_footer, OBJC_ASSOCIATION_ASSIGN);
        [self didChangeValueForKey:@"ky_footer"]; // KVO
    }
}



#pragma mark - other
- (NSInteger)getTotalDataCount
{
    NSInteger totalCount = 0;
    if ([self isKindOfClass:[UITableView class]]) {
        UITableView *tableView = (UITableView *)self;
        
        for (NSInteger section = 0; section<tableView.numberOfSections; section++) {
            totalCount += [tableView numberOfRowsInSection:section];
        }
    } else if ([self isKindOfClass:[UICollectionView class]]) {
        UICollectionView *collectionView = (UICollectionView *)self;
        
        for (NSInteger section = 0; section<collectionView.numberOfSections; section++) {
            totalCount += [collectionView numberOfItemsInSection:section];
        }
    }
    return totalCount;
}


@end
