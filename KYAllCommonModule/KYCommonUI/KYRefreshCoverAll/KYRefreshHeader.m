
//
//  KYRefreshHeader.m
//  kyExpress_Internal
//
//  Created by MelissaShu on 17/4/21.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "KYRefreshHeader.h"
#import "UITableView+NetDataStatus.h"
#import "UIScrollView+KYRefresh.h"
#import "UIView+NetDataStatus.h"
#import "Tools.h"


@interface KYRefreshHeader()
// 文字
@property (weak, nonatomic) UILabel *label;
// 上下箭头
@property (weak, nonatomic) UIImageView *arrowView;
// 小人跑
@property (weak, nonatomic) UIImageView *refreshingView;
// 状态图标
@property (weak, nonatomic) UIImageView *finalView;
// 记录上一个状态
@property (nonatomic, assign) MJRefreshState headOldState;
@end

@implementation KYRefreshHeader

{
    UITableView *_tableView;
}

#pragma mark - 构造方法
+ (instancetype)headerWithRefreshingBlock:(MJRefreshComponentRefreshingBlock)refreshingBlock
{
    KYRefreshHeader *cmp = [[self alloc] init];
    cmp.refreshingBlock = refreshingBlock;
    cmp.backgroundColor = [UIColor clearColor];
    cmp.scrollView.backgroundColor = KBack_Color2;
    
    return cmp;
}
+ (instancetype)headerWithRefreshingTarget:(id)target refreshingAction:(SEL)action
{
    KYRefreshHeader *cmp = [[self alloc] init];
    [cmp setRefreshingTarget:target refreshingAction:action];
    cmp.backgroundColor = [UIColor clearColor];
    cmp.scrollView.backgroundColor = KBack_Color2;
    
    return cmp;
}
/*
 [super beginRefreshing],无网络的时候不走这个方法，无论是在MJRefreshStatePulling还是默认进行beginRefreshing，都做了拦截
 */
- (void)beginRefreshing{
    
    
    //获取表视图
    [self getTableView];
    
    if ([Tools isNoNetwork]) {
        
        
        [_tableView setNoNet];
        [self EnabledScrollWithState:KYRefreshStateNoNetwork];
        
        //没有进行下拉
        //        if (self.state == MJRefreshStateIdle) {
        //            [_tableView setNoNet];
        //            [self EnabledScrollWithState:KYRefreshStateNoNetwork];
        //            // 解决无网络情况下还进行下拉刷新的bug
        ////            self.state = KYRefreshStateNoNetwork;
        ////            [self initialState];
        //        }
        
    }else{
        // 有缓存或者正常下拉才下拉刷新
        // 正常下拉刷新（有数据+有下拉）
        if ((self.scrollView.getTotalDataCount != 0) &&
            (self.state == MJRefreshStatePulling     ||
             self.state == MJRefreshStateRefreshing  ||
             self.state == MJRefreshStateDraging     ||
             self.state == MJRefreshStateIdle  )) {
                [super beginRefreshing];
                // 小人跑（无数据+没有下拉）
            } else if ((self.scrollView.getTotalDataCount == 0) &&
                       self.state == MJRefreshStateIdle) {
                [Tools showMBProgressHUD];
                [self executeRefreshingCallback];
                // 点击重载按钮后，无法连接服务器视图还在的bug
                
            }else {//（无数据+有下拉）
                // 直接小人跑
                [super beginRefreshing];
            }
    }
}

#pragma mark - 懒加载

#pragma mark - 重写方法
#pragma mark 在这里做一些初始化配置（比如添加子控件）
- (void)prepare
{
    [super prepare];
    
    // 添加label
    UILabel *label = [[UILabel alloc] init];
    label.textColor = [UIColor lightGrayColor];
    label.font = [UIFont systemFontOfSize:14];
    label.textAlignment = NSTextAlignmentCenter;
    [self addSubview:label];
    self.label = label;
    
    // 箭头
    UIImageView *arrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pull_down"]];
    arrowView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:arrowView];
    self.arrowView = arrowView;
    
    // 设置正在刷新状态的动画图片
    NSMutableArray *refreshingImages = [NSMutableArray array];
    for (NSUInteger i = 1; i<=8; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"dropdown_anim__000%zd", i]];
        [refreshingImages addObject:image];
    }
    UIImageView *refreshingImage = [[UIImageView alloc] init];
    refreshingImage.animationImages = refreshingImages;
    refreshingImage.animationDuration = 0.75;
    refreshingImage.hidden = YES;
    self.refreshingView = refreshingImage;
    [self addSubview:refreshingImage];
    
    
    // 最终状态显示图片
    UIImageView *finalView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"done"]];
    finalView.hidden = YES;
    [self addSubview:finalView];
    self.finalView = finalView;
}

#pragma mark 在这里设置子控件的位置和尺寸
- (void)placeSubviews {
    [super placeSubviews];
    
    self.label.frame = self.bounds;
    
    self.arrowView.center = CGPointMake(self.mj_w * 0.5 - 75, self.mj_h * 0.5);
    
    self.refreshingView.bounds = CGRectMake(0, 0, 50, 50);
    self.refreshingView.center = CGPointMake(self.mj_w * 0.5 - 75, self.mj_h * 0.5);
    
    self.finalView.center = CGPointMake(self.mj_w * 0.5 - 45, self.mj_h * 0.5);
}

#pragma mark 监听scrollView的contentOffset改变
- (void)scrollViewContentOffsetDidChange:(NSDictionary *)change {
    [super scrollViewContentOffsetDidChange:change];
}

#pragma mark 监听scrollView的contentSize改变
- (void)scrollViewContentSizeDidChange:(NSDictionary *)change {
    [super scrollViewContentSizeDidChange:change];
}

#pragma mark 监听scrollView的拖拽状态改变
- (void)scrollViewPanStateDidChange:(NSDictionary *)change {
    [super scrollViewPanStateDidChange:change];
}


#pragma mark 监听控件的刷新状态
- (void)setState:(MJRefreshState)state {
    
    
    MJRefreshCheckState;
    
    // 记录oldState
    self.headOldState = oldState;
    
    switch (state) {
        case MJRefreshStateIdle: {
            [UIView animateWithDuration:MJRefreshFastAnimationDuration animations:^{
                self.arrowView.transform = CGAffineTransformIdentity;
            }];
            self.refreshingView.hidden = NO;
            self.arrowView.hidden = YES;
            if (oldState == MJRefreshStateNoMoreData ||
                oldState == KYRefreshStateNoNetwork) {
                // 延迟隐藏
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    self.finalView.hidden = YES;
                });
            } else {
                self.finalView.hidden = NO;
            }
            //  延迟小人的消失
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.refreshingView stopAnimating];
            });
        }
            break;
            
        case MJRefreshStateDraging: {
            self.label.text = @"下拉刷新";
            [UIView animateWithDuration:MJRefreshFastAnimationDuration animations:^{
                self.arrowView.transform = CGAffineTransformIdentity;
            }];
            self.refreshingView.hidden = YES;
            self.arrowView.hidden = NO;
            self.finalView.hidden = YES;
            [self.refreshingView stopAnimating];
            self.label.hidden = NO;
        }
            break;
            
        case MJRefreshStatePulling: {
            self.label.text = @"释放立即刷新";
            //            self.arrowView.image = [UIImage imageNamed:@"pull_up"];
            [UIView animateWithDuration:MJRefreshFastAnimationDuration animations:^{
                self.arrowView.transform = CGAffineTransformMakeRotation(0.0001-M_PI);
            }];
            self.label.hidden = NO;
            self.refreshingView.hidden = YES;
            self.arrowView.hidden = NO;
            self.finalView.hidden = YES;
            [self.refreshingView stopAnimating];
        }
            break;
        case MJRefreshStateRefreshing:
            self.label.text = @"正在刷新...";
            
            self.label.hidden = NO;
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = NO;
            self.finalView.hidden = YES;
            [self.refreshingView startAnimating];
            break;
            
        case KYRefreshStateSuccess:
            
            self.label.text = @"刷新成功";
            
            self.label.hidden = NO;
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = YES;
            self.finalView.hidden = NO;
            self.finalView.image = [UIImage imageNamed:@"done"];
            [self.refreshingView stopAnimating];
            break;
            
        case KYRefreshStateFail:
            self.label.text = @"刷新失败";
            
            self.label.hidden = NO;
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = YES;
            self.finalView.hidden = NO;
            self.finalView.image = [UIImage imageNamed:@"failure"];
            [self.refreshingView stopAnimating];
            break;
        case KYRefreshStateNoNetwork:
            self.label.text = @"正在刷新...";
            
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = NO;
            self.finalView.hidden = YES;
            self.finalView.image = [UIImage imageNamed:@"failure"];
            [self.refreshingView startAnimating];
            
            self.label.hidden = NO;
            break;
        case KYRefreshStateBeyondNetwork:   // 超时
            self.label.text = @"刷新失败";
            
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = YES;
            self.finalView.hidden = NO;
            self.finalView.image = [UIImage imageNamed:@"failure"];
            [self.refreshingView stopAnimating];
            
            self.label.hidden = NO;
            break;
            
        case KYRefreshStateNoResponse:
            //            self.label.text = kNetError;
            self.label.text = @"刷新失败";
            
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = YES;
            self.finalView.hidden = NO;
            self.finalView.image = [UIImage imageNamed:@"failure"];
            [self.refreshingView stopAnimating];
            
            self.label.hidden = NO;
            break;
        case MJRefreshStateNoMoreData:
            self.finalView.hidden = NO;
            if (self.scrollView.getTotalDataCount == 0) {
                self.label.text = @"刷新成功";
                self.finalView.image = [UIImage imageNamed:@"done"];
            } else {
                self.label.text = @"没有更多数据";
                self.finalView.image = [UIImage imageNamed:@"failure"];
                self.finalView.hidden = YES;
            }
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = YES;
            [self.refreshingView stopAnimating];
            
            self.label.hidden = NO;
            break;
        default:
            break;
    }
}

- (void)endRefreshing{
    
    [self endRefreshingWithState:KYRefreshStateSuccess];
}

- (void)endRefreshingFailureWithError:(NSError *)error{
    
    // 判断是没有网络还是链接超时，还是服务器出错
    /*
     -1009 没有网络   -1001 网络超时 -1004 无法连接服务器
     -1011 坏的服务器响应
     */
    if (error.code == -1009) {
        [self endRefreshingWithState:KYRefreshStateNoNetwork];
    } else if (error.code == -1001 ||
               error.code == -1004) {
        [self endRefreshingWithState:KYRefreshStateBeyondNetwork];
    } else {
        [self endRefreshingWithState:KYRefreshStateNoResponse];
    }
}

- (void)endRefreshingWithState:(MJRefreshState)state {
    // 获取tableView，解决不走begin方法，tableView为nil的bug
    if (!_tableView) [self getTableView];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.35 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [Tools hideMBProgressHUD];
    });
    
    // 是否禁用滚动
    [self EnabledScrollWithState:state];
    // 解决点击重新加载的bug
    if (self.state == MJRefreshStateIdle &&
        state == MJRefreshStateNoMoreData) {    //点击重载的无更多数据
        if ([Tools isNoNetwork]) {
            [_tableView setNoNet];
        } else {
            [self header_setNoDataViewWithState:MJRefreshStateNoMoreData];
        }
    } else if (self.state == MJRefreshStateIdle &&
               state == KYRefreshStateSuccess) {//点击重载/默认加载的成功
        
        [self clearTableHeadView];
        [_tableView hideStatusView];
    } else if (self.state == MJRefreshStateIdle &&
               state == KYRefreshStateFail) {  // 点击重载/默认加载的失败
        
        [self clearTableHeadView];
        [self header_setNoDataViewWithState:KYRefreshStateFail];
    } else if(state == KYRefreshStateBeyondNetwork) {   // 超时
        
        [_tableView setNoNet];
        [self initialStateWithIsCleatHeaderView:NO];
    } else if(state == KYRefreshStateNoResponse) {      // 无响应
        
        [_tableView setNoResponse];
        [self initialStateWithIsCleatHeaderView:YES];
    } else if (state == KYRefreshStateNoNetwork) {      // 无网
        
        [_tableView setNoNet];
        [self initialStateWithIsCleatHeaderView:NO];
    }
    
    if (self.state != MJRefreshStateRefreshing &&
        self.state != MJRefreshStatePulling) {
        return;
    }
    
    if (self.state == state) return;
    
    //设置状态
    self.state = state;
    
    if (state == KYRefreshStateSuccess) {   // 下拉刷新的成功
        
        [self clearTableHeadView];
        [self initialStateWithIsCleatHeaderView:YES];
    } else if(state == KYRefreshStateFail) {// 下拉刷新的失败
        
        [self clearTableHeadView];
        [self initialStateWithIsCleatHeaderView:YES];
    } else if(state == MJRefreshStateNoMoreData) {//下拉刷新的无更多数据
        
        [self clearTableHeadView];
        [self initialStateWithIsCleatHeaderView:YES];
    }
}

/**
 状态初始化
 @param isClearHeadView 是否隐藏tableHeadView
 */
- (void)initialStateWithIsCleatHeaderView:(BOOL)isClearHeadView {
    
    if (self.state == MJRefreshStateIdle) return;
    
    CGFloat duration = 1;//无网络
    if (self.state == KYRefreshStateSuccess) {//成功
        duration = 0.35;
    }else if (self.state == MJRefreshStateRefreshing){//
        duration = 0.25;
    }else if (self.state == KYRefreshStateNoNetwork){
        duration = 0.25; // CAI 0.1
    }
    
    WS(weakSelf);
    // 回调bu在这里调用
    [self.scrollView setBlockReloadAction:^{
        // 移除表头,解决留白bug
        [weakSelf clearTableHeadView];
        if ([Tools isNoNetwork]) {  //  无网
            [Tools badNetWork];
        } else {                      //  有网
            // 在这里show
            [Tools showMBProgressHUD];
            [weakSelf executeRefreshingCallback];
        }
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(duration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //设置无数据视图
        if (self.state != KYRefreshStateNoNetwork  &&
            self.state != KYRefreshStateNoResponse &&
            self.state != KYRefreshStateBeyondNetwork) {
            
            // 是否清除表头
            if (isClearHeadView) {
                [self clearTableHeadView];
            }
            
            if ([_tableView getTotalDataCount] == 0) {
                [_tableView setNoData]; // 无数据视图
            }else{
                [_tableView hideStatusView];
            }
        }
        // 恢复状态
        self.state = MJRefreshStateIdle;
    });
    
    // 下拉刷新卡住的bug
    if (self.scrollView.scrollEnabled == NO &&
        self.scrollView.mj_insetT != 0  ) {
        self.scrollView.scrollEnabled = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (self.scrollView.getTotalDataCount == 0) {
                    self.scrollView.scrollEnabled = NO;
                }
            });
        });
    }
}
// 清除表头
- (void)clearTableHeadView {
    _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KSCREEN_W, CGFLOAT_MIN)];
}
#pragma mark - 禁用和开启滚动
- (void)EnabledScrollWithState:(MJRefreshState)state {
    self.scrollView.scrollEnabled = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            // 不能滚动的三种情况，无数据，无法连接服务器，服务器无响应
            if (self.scrollView.getTotalDataCount == 0) {
                self.scrollView.scrollEnabled = NO;
                [self clearTableHeadView];
            }
        });
    });
}
#pragma mark - 是否设置无数据水印
- (void)header_setNoDataViewWithState:(MJRefreshState)state {
    
    
    //FIXME: 点击重新加载，无数据视图还在的bug，要延迟无数据视图的出现，就是在这里做延迟,[_tableView getTotalDataCount]留计算时间
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        //设置无数据视图
        if (self.state != KYRefreshStateNoNetwork  &&
            self.state != KYRefreshStateNoResponse &&
            self.state != KYRefreshStateBeyondNetwork) {
            // 清除表头
            [self clearTableHeadView];
            
            if ([_tableView getTotalDataCount] == 0) {
                [_tableView setNoData]; // 无数据视图
            } else {
                [_tableView hideStatusView];
            }
        }
    });
    
    //    CGFloat duration = 1;//无网络
    //    if (self.state == KYRefreshStateSuccess) {//成功
    //        duration = 0.35;
    //    }else if (self.state == MJRefreshStateRefreshing){//
    //        duration = 0.25;
    //    }else if (self.state == KYRefreshStateNoNetwork){
    //        duration = 0.25; // CAI 0.1
    //    }
    //
    
    //
    //        //设置无数据视图
    //        if (self.state != KYRefreshStateNoNetwork  &&
    //            self.state != KYRefreshStateNoResponse &&
    //            self.state != KYRefreshStateBeyondNetwork) {
    //                // 清除表头
    //                [self clearTableHeadView];
    //
    //                if ([_tableView getTotalDataCount] == 0) {
    //                    [_tableView setNoData]; // 无数据视图
    //                } else {
    //                    [_tableView hideStatusView];
    //                }
    //        }
    //    });
}
#pragma mark - 获取tableView视图
- (void)getTableView {
    if ([self.scrollView isKindOfClass:[UITableView class]]) {
        _tableView = (UITableView *)self.scrollView;
    }
}
@end
