//
//  UIScrollView+KYRefresh.h
//  kyExpress_Internal
//
//  Created by MelissaShu on 17/4/21.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJRefreshConst.h"

@class KYRefreshFooter,KYRefreshHeader;

@interface UIScrollView (KYRefresh)
/** 下拉刷新控件 */
@property (strong, nonatomic) KYRefreshHeader *ky_header;

/** 上拉刷新控件 */
@property (strong, nonatomic) KYRefreshFooter *ky_footer;

#pragma mark - other
- (NSInteger)getTotalDataCount;

@property (copy, nonatomic) void (^mj_reloadDataBlock)(NSInteger totalDataCount);
@end
