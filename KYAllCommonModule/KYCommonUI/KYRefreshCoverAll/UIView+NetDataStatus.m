//
//  UIView+NetDataStatus.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 17/3/24.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "UIView+NetDataStatus.h"
#import "UIView+MJExtension.h"
#import "ReloadView.h"
#import "Tools.h"
#import "UIScrollView+KYRefresh.h"
#import "UIScrollView+KYERefresh.h"
#import "KYRefreshHeader.h"

#import <objc/runtime.h>


#define kSCREEN_WIDTH  [UIScreen mainScreen].bounds.size.width
#define kSCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height


static const CGFloat MJDuration = 1.0;

static char const * const kNoDataView = "noDataView";
static char const * const kReloadView = "reloadView";
static const void * const kBlockReloadAction = "blockReloadAction";

@interface UIView()

//@property (nonatomic, readonly) NoDataView *noDataView;
@property (nonatomic, readonly) ReloadView *reloadView;
@end

@implementation UIView (NetDataStatus)

@dynamic blockReloadAction;

-(BlockReloadAction)blockReloadAction
{
    return objc_getAssociatedObject(self, kBlockReloadAction);
}

-(void)setBlockReloadAction:(BlockReloadAction)blockReloadAction
{
    
    objc_setAssociatedObject(self, kBlockReloadAction, blockReloadAction, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    [self.reloadView setBlockReloadAction:blockReloadAction];
}

//-(NoDataView *)noDataView
//{
//    NoDataView *noDataView = objc_getAssociatedObject(self, kNoDataView);
//
//    if (!noDataView)
//    {
//        noDataView = [[NoDataView alloc]initWithFrame:CGRectMake((kSCREEN_WIDTH-300)*0.5, (self.mj_h - 130)*0.5, 300, 130)];
//        noDataView.hidden = NO;
//        //noDataView.backgroundColor = [UIColor redColor];
//
//        //[noDataView addTarget:self action:@selector(noDataViewClick) forControlEvents:UIControlEventTouchUpInside];
//
//        [self setNoDataView:noDataView];
//    }
//    return noDataView;
//}

-(ReloadView *)reloadView
{
    
    ReloadView *reloadView = objc_getAssociatedObject(self, kReloadView);
    
    if (!reloadView) {
        CGFloat reloadH = 225;
        CGFloat reloadY = (self.mj_h - reloadH - 64 - 49 ) * 0.5;
        reloadView = [[ReloadView alloc]initWithFrame:CGRectMake(0, reloadY, kSCREEN_WIDTH, reloadH)];
        reloadView.hidden = NO;
        
        
        if (self.blockReloadAction) {
            [reloadView setBlockReloadAction:self.blockReloadAction];
        }
        
        [self setReloadView:reloadView];
    }
    return reloadView;
}

- (void)adjustFrame{
    CGFloat reloadH = 225;
    CGFloat reloadY = self.mj_h == 0 ? (KSCREEN_H-64-reloadH)/2 : (self.mj_h - reloadH)/2;
    self.reloadView.frame = CGRectMake(0, reloadY, kSCREEN_WIDTH, reloadH);
    
    NSLog(@"\n %@ - %@",NSStringFromCGRect(self.frame),NSStringFromCGRect(self.reloadView.frame));
}


//-(void)setNoDataView:(NoDataView *)noDataView
//{
//    objc_setAssociatedObject(self, kNoDataView, noDataView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
//}

-(void)setReloadView:(ReloadView *)reloadView
{
    
    objc_setAssociatedObject(self, kReloadView, reloadView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

//- (void)noDataViewClick{
//    [self hideNodataView];
//}

#pragma mark - 无数据视图
- (void)showNodataView{
    
    ReloadView *reloadView = self.reloadView;
    [reloadView setNoDataStyle];
    [self adjustFrame];
    
    
    if (!reloadView.superview) {
        
        //        if (([self isKindOfClass:[UITableView class]] || [self isKindOfClass:[UICollectionView class]]) && self.subviews.count > 1) {
        //
        //            [self insertSubview:reloadView atIndex:0];
        //
        //        }else {
        [self addSubview:reloadView];
        
        //        }
    }else{
        
    }
    
}

- (void)hideNodataView{
    
    if (self.reloadView) {
        [self.reloadView removeFromSuperview];
    }
    
}



////显示状态视图
//- (void)showStatusViewWithKYEState:(KYEState)kyeState{
//
//    switch (kyeState) {
//        case KYEStateNoData:
//            [self showNodataView];
//            break;
//
//        case KYEStateNoNet:
//            [self showNetErrorView];
//            break;
//
//        case KYEStateNoResponse:
//            [self showNoResponseView];
//            break;
//
//        default:
//            break;J
//    }
//
//}

- (void)hideStatusView{
    
    if (self.reloadView) {
        
        [UIView animateWithDuration:0.5 animations:^{
            
        } completion:^(BOOL finished) {
            [self.reloadView removeFromSuperview];
        }];
    }
}

#pragma mark - 无法连接服务器视图
- (void)showNetErrorView{
    
    ReloadView *reloadView = self.reloadView;
    [self adjustFrame];
    [reloadView setNetErrorStyle];
    
    // 重新加载
    WS(weakSelf);
#pragma mark - 无法连接服务器的回调
    // 点击重现加载的回调，如果是tableview下拉刷新的话，就调用下拉刷新的回调
    [reloadView setBlockReloadAction:^{
        
        if ([weakSelf isKindOfClass:[UITableView class]]) {
            __weak UITableView *table = (UITableView *)weakSelf;
            [Tools showMBProgressHUDAddView:table.superview];
            [table.kye_header executeRefreshingCallback];
        }
        
//        if ([Tools isNoNetwork]) {
//            [Tools badNetWork];
//        }else{
//            
//            if ([weakSelf isKindOfClass:[UITableView class]]) {
//                __weak UITableView *table = (UITableView *)weakSelf;
//                [Tools showMBProgressHUDAddView:table.superview];
//                [table.ky_header executeRefreshingCallback];
//                // 测试
//                [table.kye_header executeRefreshingCallback];
//            }
//        }
    }];
    
    if (!reloadView.superview) {
        
        //        if (([self isKindOfClass:[UITableView class]] || [self isKindOfClass:[UICollectionView class]]) && self.subviews.count > 1) {
        //
        //            [self insertSubview:reloadView atIndex:0];
        //
        //        }else {
        [self addSubview:reloadView];
        
        //        }
    }
    NSLog(@"%@",NSStringFromCGRect(self.frame));
    NSLog(@"%@",NSStringFromCGRect(reloadView.frame));
}

- (void)hideNetErrorView{
    
    if (self.reloadView) {
        [self.reloadView removeFromSuperview];
    }
}


//- (void)showHeadNetErrorWithFrame:(CGRect)frame{
//
//    ReloadView *reloadView = self.reloadView;
//    //reloadView.frame = CGRectMake(0, 0, kSCREEN_WIDTH, 40);
//    reloadView.frame = frame;
//    [self.reloadView setHeadNetErrorStyle];
//
//    // 2.模拟2秒后
//
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MJDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//
//        if (!reloadView.superview) {
//            if (([self isKindOfClass:[UITableView class]] || [self isKindOfClass:[UICollectionView class]]) && self.subviews.count > 1) {
//                [self insertSubview:reloadView atIndex:0];
//            }
//            else {
//                [self addSubview:reloadView];
//            }
//        }
//
//    });
//
//}


//- (void)showHeadNoResponseWithFrame:(CGRect)frame{
//
//    ReloadView *reloadView = self.reloadView;
//    //reloadView.frame = CGRectMake(0, 0, kSCREEN_WIDTH, 40);
//    reloadView.frame = frame;
//    [self.reloadView setHeadNoResponseStyle];
//
//    if (!reloadView.superview) {
//
//        if (([self isKindOfClass:[UITableView class]] || [self isKindOfClass:[UICollectionView class]]) && self.subviews.count > 1) {
//
//            [self insertSubview:reloadView atIndex:0];
//
//        }
//        else {
//            [self addSubview:reloadView];
//
//        }
//    }
//}


#pragma mark - 服务器无响应视图
- (void)showNoResponseView{
    
    ReloadView *reloadView = self.reloadView;
    [self adjustFrame];
    [reloadView setNoResponseStyle];
    
    WS(weakSelf);
#pragma mark - 服务器连接异常的回调
    [reloadView setBlockReloadAction:^{
        
        if ([weakSelf isKindOfClass:[UITableView class]]) {
            
            __weak UITableView *table = (UITableView *)weakSelf;
            [Tools showMBProgressHUDAddView:table.superview];
            [table.kye_header executeRefreshingCallback];
        }
        
//        if ([Tools isNoNetwork]) {
//            [Tools badNetWork];
//        }else{
//            
//            if ([weakSelf isKindOfClass:[UITableView class]]) {
//                
//                __weak UITableView *table = (UITableView *)weakSelf;
//                [Tools showMBProgressHUDAddView:table.superview];
//                [table.ky_header executeRefreshingCallback];
//                // 测试
//                [table.kye_header executeRefreshingCallback];
//            }
//        }
    }];
    
    if (!reloadView.superview) {
        
        //        if (([self isKindOfClass:[UITableView class]] || [self isKindOfClass:[UICollectionView class]]) && self.subviews.count > 1) {
        //            [self insertSubview:reloadView atIndex:0];
        //
        //        }
        //        else {
        [self addSubview:reloadView];
        //        }
    }
}

- (void)hideNoResponseView{
    
    [self.reloadView removeFromSuperview];
    
    //    if (self.reloadView) {
    //        [self.reloadView removeFromSuperview];
    //    }
}

#pragma mark - 无数据视图充满tableView
- (void)showNodataViewWithFrame:(CGRect)frame {
    CGFloat viewW = frame.size.width ?: KSCREEN_W;
    CGFloat viewH = frame.size.height ?: (KSCREEN_H-64);
    self.reloadView.frame = CGRectMake(0, 0, viewW, viewH);
    
    ReloadView *reloadView = self.reloadView;
    [reloadView setNoDataStyle2];
    
    if (!reloadView.superview) {
        [self addSubview:reloadView];
    }
}
#pragma mark - 无网络视图充满tableView
- (void)showNetErrorViewWithFrame:(CGRect)frame {
    CGFloat viewW = frame.size.width ?: KSCREEN_W;
    CGFloat viewH = frame.size.height ?: (KSCREEN_H-64);
    self.reloadView.frame = CGRectMake(0, 0, viewW, viewH);
    
    ReloadView *reloadView = self.reloadView;
    [reloadView setNetErrorStyle2];
    
    // 重新加载
    WS(weakSelf);
    
    // 点击重现加载的回调，如果是tableview下拉刷新的话，就调用下拉刷新的回调
    [reloadView setBlockReloadAction:^{
        
        if ([weakSelf isKindOfClass:[UITableView class]]) {
            __weak UITableView *table = (UITableView *)weakSelf;
            [Tools showMBProgressHUDAddView:table.superview];
            [table.kye_header executeRefreshingCallback];
        }
    }];
    
    if (!reloadView.superview) {
        [self addSubview:reloadView];
    }
}
#pragma mark - 无响应视图充满tableView
- (void)showNoResponseViewWithFrame:(CGRect)frame {
    CGFloat viewW = frame.size.width ?: KSCREEN_W;
    CGFloat viewH = frame.size.height ?: (KSCREEN_H-64);
    self.reloadView.frame = CGRectMake(0, 0, viewW, viewH);
    
    ReloadView *reloadView = self.reloadView;
    [reloadView setNoResponseStyle2];
    
    WS(weakSelf);
    
    [reloadView setBlockReloadAction:^{
        if ([weakSelf isKindOfClass:[UITableView class]]) {
            __weak UITableView *table = (UITableView *)weakSelf;
            [Tools showMBProgressHUDAddView:table.superview];
            [table.kye_header executeRefreshingCallback];
        }
    }];
    
    if (!reloadView.superview) {
        [self addSubview:reloadView];
    }
}



@end
