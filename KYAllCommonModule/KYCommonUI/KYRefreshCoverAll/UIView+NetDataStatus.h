//
//  UIView+NetDataStatus.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 17/3/24.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^BlockReloadAction)(void);

@interface UIView (NetDataStatus)

@property (nonatomic, copy) BlockReloadAction blockReloadAction;

#pragma mark - 无数据视图
- (void)showNodataView;
- (void)showNodataViewWithFrame:(CGRect)frame;
- (void)hideNodataView;
- (void)showNodataViewWithFrame:(CGRect)frame;


#pragma mark - 服务器无相应视图
- (void)showNoResponseView;
- (void)showNoResponseViewWithFrame:(CGRect)frame;
- (void)hideNoResponseView;

#pragma mark - 无网络视图
- (void)showNetErrorView;
- (void)showNetErrorViewWithFrame:(CGRect)frame;
- (void)hideNetErrorView;


//显示状态视图
//- (void)showStatusViewWithKYEState:(KYEState)kyeState;
//隐藏状态视图
- (void)hideStatusView;
//- (void)showHeadNetError;

-(void)setBlockReloadAction:(BlockReloadAction)blockReloadAction;

//- (void)showHeadNetErrorWithFrame:(CGRect)frame;
//- (void)showHeadNoResponseWithFrame:(CGRect)frame;

@end
