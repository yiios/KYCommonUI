//
//  ReloadView.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 17/2/6.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "ReloadView.h"
#import "UIView+MJExtension.h"
#import "Tools.h"
#import "UIColor+HcdCustom.h"
#define btnW 120

//字体
#define KTextFont [UIFont systemFontOfSize:16.0f]

#define kFont14 [UIFont systemFontOfSize:14.0f]
#define kFont16 [UIFont systemFontOfSize:16.0f]

#define kSCREEN_WIDTH  [UIScreen mainScreen].bounds.size.width
#define kSCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

#define kColorTextGray  [UIColor grayColor] //普通灰色文本
#define kColorTextBlack [UIColor colorWithHexString:@"333333"]  //普通黑色文本

@interface ReloadView()

@property (nonatomic,strong) UIImageView *imgView;

@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UILabel *detailLabel;

@property (nonatomic,strong) UIButton *reloadBtn;

@end

@implementation ReloadView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setUpView];
    }
    return self;
}

- (void)setUpView{
    self.backgroundColor = [UIColor clearColor];
    
    CGFloat imgW = 162;
    CGFloat imgX = (self.frame.size.width - imgW) * 0.5;
    
    
    self.imgView.frame = CGRectMake(imgX, 0, 162, 180);
    
    self.reloadBtn.frame = CGRectMake((self.frame.size.width - btnW) * 0.5, CGRectGetMaxY(self.imgView.frame) + 10, btnW, 35);
    
    
    [self addSubview:self.imgView];
    [self addSubview:self.reloadBtn];
    [self addSubview:self.titleLabel];
    
    [self setNoDataStyle];
    
}

//无数据样式
- (void)setNoDataStyle{
    
    self.backgroundColor = [UIColor clearColor];
    
    [self.imgView setImage:[UIImage imageNamed:@"noData"]];
    
    CGFloat imgW = 50;
    CGFloat imgX = (self.frame.size.width - imgW) * 0.5;
    CGFloat imgY = (self.mj_h - 50 - 20 - 15) * 0.5;
    
    self.reloadBtn.hidden = YES;
    self.titleLabel.hidden = NO;
    self.titleLabel.textColor = [UIColor grayColor];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    self.imgView.frame = CGRectMake(imgX, imgY, imgW, 50);
    self.titleLabel.text = @"暂无相关数据";
    self.titleLabel.frame = CGRectMake(0, CGRectGetMaxY(self.imgView.frame) + 15, self.frame.size.width, 20);
    
}


//网络请求失败
- (void)setNetErrorStyle{
    
    [self setReloadStyle];
    [self.imgView setImage:[UIImage imageNamed:@"RequestFailure"]];
}

//服务器无响应
- (void)setNoResponseStyle{
    
    [self setReloadStyle];
    
    [self.imgView setImage:[UIImage imageNamed:@"NoResponse2"]];
}


- (void)setHeadNetErrorStyle{
    
    [self setHeadStyle];
    
    self.titleLabel.text = @"网络请求失败，请稍后再试";
    
}

- (void)setHeadNoResponseStyle{
    
    [self setHeadStyle];
    
    self.titleLabel.text = @"服务器无响应，请稍后再试";
    
}

- (void)setReloadStyle{
    self.backgroundColor = [UIColor clearColor];
    
    CGFloat imgW = 162;
    CGFloat imgX = (self.frame.size.width - imgW) * 0.5;
    self.imgView.frame = CGRectMake(imgX, 0, imgW, 180);
    
    self.reloadBtn.frame = CGRectMake((self.frame.size.width - btnW) * 0.5, CGRectGetMaxY(self.imgView.frame) + 10, btnW, 35);
    
    self.reloadBtn.hidden = NO;
    self.titleLabel.hidden = YES;
    
}

- (void)setHeadStyle{
    self.backgroundColor = [UIColor colorWithHexString:@"ffd4bf"];
    
    self.reloadBtn.hidden = YES;
    self.titleLabel.hidden = NO;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    self.titleLabel.font = kFont14;
    self.titleLabel.textColor = kColorTextBlack;
    
    [self.imgView setImage:[UIImage imageNamed:@"warning"]];
    self.imgView.frame =  CGRectMake(5, 10, 19, 19);
    CGFloat labelX = CGRectGetMaxX(self.imgView.frame) + 5;
    self.titleLabel.frame = CGRectMake(labelX, 0, kSCREEN_WIDTH - labelX, 40);
}

-(UIImageView *)imgView
{
    if (!_imgView) {
        _imgView = [[UIImageView alloc]init];
    }
    return _imgView;
}

-(UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.textColor = [UIColor grayColor];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = kFont16;
        _titleLabel.hidden = YES;
    }
    return _titleLabel;
}

-(UILabel *)detailLabel
{
    if (!_detailLabel) {
        _detailLabel = [[UILabel alloc]init];
        _detailLabel.textColor = kColorTextGray;
        _detailLabel.textAlignment = NSTextAlignmentCenter;
        _detailLabel.font = kFont14;
    }
    return _detailLabel;
}

-(UIButton *)reloadBtn
{
    if (!_reloadBtn) {
        _reloadBtn = [[UIButton alloc]init];
        _reloadBtn.layer.borderWidth = 1;
        _reloadBtn.layer.borderColor = [UIColor colorWithHexString:@"cccccc"].CGColor;
        _reloadBtn.layer.cornerRadius = 5;
        _reloadBtn.layer.masksToBounds = YES;
        
        _reloadBtn.titleLabel.font = kFont14;
        [_reloadBtn setTitle:@"重新加载" forState:UIControlStateNormal];
        
        [_reloadBtn setTitleColor:[UIColor colorWithHexString:@"999999"] forState:UIControlStateNormal];
        
        [_reloadBtn addTarget:self action:@selector(reloadAction) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _reloadBtn;
}

- (void)reloadAction{
    
    if (_blockReloadAction) {
        _blockReloadAction();
    }else{
        
        if ([Tools isNoNetwork]) {
            [Tools badNetWork];
        }
    }
}

-(void)setBlockReloadAction:(BlockReloadAction)blockReloadAction
{
    _blockReloadAction = blockReloadAction;
}
#pragma mark - 无数据样式2，视图覆盖tableView
- (void)setNoDataStyle2{
    
    self.backgroundColor = [UIColor whiteColor];
    
    [self.imgView setImage:[UIImage imageNamed:@"noData"]];
    
    CGFloat imgW = 50;
    CGFloat imgH = imgW;
    CGFloat imgX = (self.frame.size.width - imgW) * 0.5;
    CGFloat imgY = (self.frame.size.height - imgH - 20 - 15) * 0.5;
    
    self.reloadBtn.hidden = YES;
    self.titleLabel.hidden = NO;
    self.titleLabel.textColor = [UIColor grayColor];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    self.imgView.frame = CGRectMake(imgX, imgY, imgW, imgH);
    self.titleLabel.text = @"暂无相关数据";
    self.titleLabel.frame = CGRectMake(0, CGRectGetMaxY(self.imgView.frame) + 15, self.frame.size.width, 20);
    
}
#pragma mark - 无网络样式2，视图覆盖tableView
- (void)setNetErrorStyle2{
    
    self.backgroundColor = [UIColor whiteColor];
    
    CGFloat imgW = 162;
    CGFloat imgH = 180;
    CGFloat imgX = (self.frame.size.width - imgW) * 0.5;
    CGFloat imgY = (self.frame.size.height - imgH - 35 - 10) * 0.5;;
    
    self.imgView.frame = CGRectMake(imgX, imgY, imgW,imgH);
    self.reloadBtn.frame = CGRectMake((self.frame.size.width - btnW) * 0.5, CGRectGetMaxY(self.imgView.frame) + 10, btnW, 35);
    
    self.reloadBtn.hidden = NO;
    self.titleLabel.hidden = YES;
    
    [self.imgView setImage:[UIImage imageNamed:@"RequestFailure"]];
}
#pragma mark - 无响应样式2，视图覆盖tableView
- (void)setNoResponseStyle2 {
    
    self.backgroundColor = [UIColor whiteColor];
    
    CGFloat imgW = 162;
    CGFloat imgH = 180;
    CGFloat imgX = (self.frame.size.width - imgW) * 0.5;
    CGFloat imgY = (self.frame.size.height - imgH - 35 - 10) * 0.5;;
    
    self.imgView.frame = CGRectMake(imgX, imgY, imgW,imgH);
    self.reloadBtn.frame = CGRectMake((self.frame.size.width - btnW) * 0.5, CGRectGetMaxY(self.imgView.frame) + 10, btnW, 35);
    
    self.reloadBtn.hidden = NO;
    self.titleLabel.hidden = YES;
    
    [self.imgView setImage:[UIImage imageNamed:@"NoResponse2"]];
}
@end

