//
//  KYRefreshBackFooter.h
//  kyExpress_Internal
//
//  Created by MelissaShu on 17/4/21.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "MJRefreshFooter.h"
#import "KYRefreshBaseFooter.h"

@interface KYRefreshBackFooter : KYRefreshBaseFooter

- (void)setFooterScreenNum5:(NSInteger)screenNum5  screenNum6:(NSInteger)screenNum6  screenNum6p:(NSInteger)screenNum6p;

@property (nonatomic, assign) MJRefreshState oldState2; //比oldState更old得state

@end
