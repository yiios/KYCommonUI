//
//  ReloadView.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 17/2/6.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BlockReloadAction)(void);

@interface ReloadView : UIView

@property (nonatomic, copy) BlockReloadAction blockReloadAction;

//无数据样式
- (void)setNoDataStyle;
- (void)setNoDataStyle2;

//网络请求失败
- (void)setNetErrorStyle;
- (void)setNetErrorStyle2;

//服务器无响应
- (void)setNoResponseStyle;
- (void)setNoResponseStyle2;

- (void)setHeadNoResponseStyle;
- (void)setHeadNetErrorStyle;

-(void)setBlockReloadAction:(BlockReloadAction)blockReloadAction;

@end
