//
//  KYRefreshBaseFooter.h
//  kyExpress_Internal
//
//  Created by MelissaShu on 17/4/21.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "MJRefreshComponent.h"

@interface KYRefreshBaseFooter : MJRefreshComponent

@property (nonatomic,assign) NSInteger onePageNum; //一页数据的行数 - 用于不足一页时不进行下拉刷新

/** 创建footer */
+ (instancetype)footerWithRefreshingBlock:(MJRefreshComponentRefreshingBlock)refreshingBlock;
/** 创建footer */
+ (instancetype)footerWithRefreshingTarget:(id)target refreshingAction:(SEL)action;




/** 提示没有更多的数据 */
- (void)endRefreshingWithNoMoreData;


/*刷新出错*/
- (void)endRefreshingWithError;

- (void)noticeNoMoreData MJRefreshDeprecated("使用endRefreshingWithNoMoreData");

/** 结束刷新,刷入对应状态显示对应提示 */
- (void)endRefreshingWithState:(MJRefreshState)state;
- (void)endRefreshingFailureWithError:(NSError *)error;

/** 重置没有更多的数据（消除没有更多数据的状态） */
- (void)resetNoMoreData;

/** 忽略多少scrollView的contentInset的bottom */
@property (assign, nonatomic) CGFloat ignoredScrollViewContentInsetBottom;

/** 自动根据有无数据来显示和隐藏（有数据就显示，没有数据隐藏。默认是NO） */
@property (assign, nonatomic, getter=isAutomaticallyHidden) BOOL automaticallyHidden;
/** 是否可以做上拉刷新的开关（YES就禁止上拉刷新，NO就可以上拉，默认是NO） */
@property (assign, nonatomic, getter=isProhibitFooter) BOOL prohibitFooter;

/** 判断是否已经显示头部无网络视图 */
@property (nonatomic, assign) BOOL isDisplayHeader;
@end
