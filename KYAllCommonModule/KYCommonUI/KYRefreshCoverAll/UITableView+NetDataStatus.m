//
//  UITableView+NetDataStatus.m
//  kyExpress_Internal
//
//  Created by MelissaShu on 17/4/10.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "UITableView+NetDataStatus.h"
#import "NetStatusView.h"
#import "UIView+NetDataStatus.h"
#import "UIScrollView+KYRefresh.h"

@implementation UITableView (NetDataStatus)


#pragma mark -

// 设置无网络
- (void)setNoNet{
    
    NSInteger section = self.getTotalDataCount;
    
    if (section > 0) {
        [self setHeaderNoNet];  // 添加tableHeaderView，移除reloadView
    }else{
        [self setContentNoNet];
    }
    
}

// 设置无响应
- (void)setNoResponse{
    
    NSInteger section = self.getTotalDataCount;
    //    NSLog(@"设置无响应：section : %ld",(long)section);
    
    if (section > 0) {
        [self setHeaderNoResponse];
    }else{
        [self setContentNoResponse];
    }
}

// 设置无数据
- (void)setNoData{
    
    NSInteger section = self.getTotalDataCount;
    //    NSLog(@"设置无数据：section : %ld",(long)section);
    
    if (section > 0) {
        [self hideStatusView];
    }else{
        [self setContentNoData];
    }
}


#pragma mark -
- (void)setHeaderNoNet{
    
    [self hideStatusView];
    
    NetStatusView *view = [[NetStatusView alloc] initWithFrame:CGRectMake(0, -1, 330, 40)];
    [view setNetErrorStyle];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        self.tableHeaderView = view;
    });
    
    
}

- (void)setHeaderNoResponse{
    
    [self hideStatusView];
    
    NetStatusView *view = [[NetStatusView alloc] initWithFrame:CGRectMake(0, -1, 330, 40)];
    [view setNoResponseStyle];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.25 animations:^{
            self.tableHeaderView = view;
        }];
    });
}


#pragma mark -
- (void)setContentNoNet{
    
    [self showNetErrorView];
    [self clearTableHeadView];
}

- (void)setContentNoResponse{
    
    [self clearTableHeadView];
    [self showNoResponseView];
    
}


- (void)setContentNoData{
    
    [self clearTableHeadView];
    [self showNodataView];
    
}
#pragma mark - 清除表头
// 清除表头
- (void)clearTableHeadView {
    self.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KSCREEN_W, CGFLOAT_MIN)];
}

@end
