//
//  KYRefreshFooter.m
//  kyExpress_Internal
//
//  Created by MelissaShu on 17/4/21.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "KYRefreshFooter.h"
#import "UITableView+NetDataStatus.h"
#import "UIScrollView+KYRefresh.h"
#import "UIView+NetDataStatus.h"
#import "Tools.h"

@interface KYRefreshFooter()

@property (weak, nonatomic) UILabel *label;
@property (weak, nonatomic) UIImageView *arrowView;
@property (weak, nonatomic) UIImageView *refreshingView;
@property (weak, nonatomic) UIImageView *finalView;


@end

@implementation KYRefreshFooter
{
    UITableView *_tableView;
}

#pragma mark - 重写方法

+ (instancetype)footerWithRefreshingBlock:(MJRefreshComponentRefreshingBlock)refreshingBlock
{
    KYRefreshFooter *cmp = [[self alloc] init];
    cmp.refreshingBlock = refreshingBlock;
    cmp.backgroundColor = [UIColor clearColor];
    cmp.scrollView.backgroundColor = KBack_Color2;
    
    return cmp;
}
+ (instancetype)footerWithRefreshingTarget:(id)target refreshingAction:(SEL)action
{
    KYRefreshFooter *cmp = [[self alloc] init];
    [cmp setRefreshingTarget:target refreshingAction:action];
    cmp.backgroundColor = [UIColor clearColor];
    cmp.scrollView.backgroundColor = KBack_Color2;
    return cmp;
}



- (void)beginRefreshing{
    
    
    [super beginRefreshing];
    
    
    //    if ([Tools isNoNetwork]) {
    //        // beginRefreshing的继承链调用在这里断掉，状态没有恢复成MJRefreshStateRefreshing，导致无网络上拉刷新失败
    //        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    //            NSLog(@"footer 无网络结束刷新");
    //            [self endRefreshingWithState:KYRefreshStateNoNetwork];
    //        });
    //
    //    }else{
    //        [super beginRefreshing];
    //        // 不在这里隐藏视图
    ////        [_tableView hideStatusView];
    //    }
}


#pragma mark 在这里做一些初始化配置（比如添加子控件）
- (void)prepare
{
    [super prepare];
    
    // 添加label
    UILabel *label = [[UILabel alloc] init];
    label.textColor = [UIColor lightGrayColor];
    label.font = [UIFont systemFontOfSize:14];
    label.textAlignment = NSTextAlignmentCenter;
    [self addSubview:label];
    self.label = label;
    
    // 箭头
    UIImageView *arrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pull_up"]];
    arrowView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:arrowView];
    self.arrowView = arrowView;
    
    // 设置正在刷新状态的动画图片
    NSMutableArray *refreshingImages = [NSMutableArray array];
    for (NSUInteger i = 1; i<=8; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"dropdown_anim__000%zd", i]];
        [refreshingImages addObject:image];
    }
    UIImageView *refreshingImage = [[UIImageView alloc] init];
    [self addSubview:refreshingImage];
    refreshingImage.animationImages = refreshingImages;
    refreshingImage.animationDuration = 0.75;
    refreshingImage.hidden = YES;
    self.refreshingView = refreshingImage;
    
    // 最终状态显示图片
    UIImageView *finalView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"done"]];
    finalView.hidden = YES;
    [self addSubview:finalView];
    self.finalView = finalView;
    
    
    self.refreshingView.hidden = YES;
    self.finalView.hidden = YES;
    self.arrowView.hidden = YES;
}

#pragma mark 在这里设置子控件的位置和尺寸
- (void)placeSubviews
{
    [super placeSubviews];
    
    self.label.frame = self.bounds;
    
    self.arrowView.center = CGPointMake(self.mj_w * 0.5 - 75, self.mj_h * 0.5);
    
    self.refreshingView.bounds = CGRectMake(0, 0, 35, 35);
    self.refreshingView.center = CGPointMake(self.mj_w * 0.5 - 75, self.mj_h * 0.5);
    
    self.finalView.center = CGPointMake(self.mj_w * 0.5 - 60, self.mj_h * 0.5);
}

#pragma mark 监听scrollView的contentOffset改变
- (void)scrollViewContentOffsetDidChange:(NSDictionary *)change
{
    [super scrollViewContentOffsetDidChange:change];
}

#pragma mark 监听scrollView的contentSize改变
- (void)scrollViewContentSizeDidChange:(NSDictionary *)change
{
    [super scrollViewContentSizeDidChange:change];
}

#pragma mark 监听scrollView的拖拽状态改变
- (void)scrollViewPanStateDidChange:(NSDictionary *)change
{
    [super scrollViewPanStateDidChange:change];
}

#pragma mark 监听控件的刷新状态

- (void)setState:(MJRefreshState)state
{
    
    MJRefreshCheckState;
    
    switch (state) {
        case MJRefreshStateIdle:
            // [self heightForContentBreakView] < 0，基本就不能上拉刷新了
            if ([self heightForContentBreakView] < 0){
                self.label.text = @"";
            } else if (oldState == KYRefreshStateNoNetwork   ||
                       oldState == KYRefreshStateNoResponse){
                self.label.text = kNetError;
            } else {
                self.label.text = @"查看更多"; // 与安卓统一
            }
            self.arrowView.image = [UIImage imageNamed:@"pull_up"];
            [self.refreshingView stopAnimating];
            self.refreshingView.hidden = YES;
            self.finalView.hidden = YES;
            self.arrowView.hidden = YES;
            self.label.hidden  = NO;
            break;
            
        case MJRefreshStateDraging:
            //            self.label.text = @"上拉刷新";
            self.label.text = @"查看更多";
            self.arrowView.image = [UIImage imageNamed:@"pull_up"];
            self.refreshingView.hidden = YES;
            self.arrowView.hidden = YES;
            self.finalView.hidden = YES;
            [self.refreshingView stopAnimating];
            
            self.label.hidden = NO;
            
            break;
            
        case MJRefreshStatePulling:
            //            self.label.text = @"释放立即刷新";
            self.label.text = @"查看更多";
            self.arrowView.image = [UIImage imageNamed:@"pull_down"];
            
            self.label.hidden = NO;
            self.refreshingView.hidden = YES;
            self.arrowView.hidden = YES;
            self.finalView.hidden = YES;
            [self.refreshingView stopAnimating];
            break;
        case MJRefreshStateRefreshing:
            self.label.text = @"加载中，请稍候...";
            
            self.label.hidden = NO;
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = NO;
            self.finalView.hidden = YES;
            [self.refreshingView startAnimating];
            break;
            
        case MJRefreshStateNoMoreData:
        case KYRefreshStateStop:
            self.label.text = @"没有更多数据";
            
            self.label.hidden = NO;
            self.arrowView.image = [UIImage imageNamed:@"pull_up"];
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = YES;
            self.finalView.hidden = YES;
            self.finalView.image = [UIImage imageNamed:@"failure"];
            [self.refreshingView stopAnimating];
            // 是否要清除TableHeaderView
            break;
            
            
        case KYRefreshStateSuccess:
            self.label.text = @"刷新成功";
            
            self.label.hidden = NO;
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = YES;
            self.finalView.hidden = NO;
            self.finalView.image = [UIImage imageNamed:@"done"];
            [self.refreshingView stopAnimating];
            break;
        case KYRefreshStateFail:
            self.label.text = @"刷新失败";
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = YES;
            self.finalView.hidden = YES;
            self.finalView.image = [UIImage imageNamed:@"failure"];
            [self.refreshingView stopAnimating];
            break;
            
        case KYRefreshStateNoNetwork:
            self.label.text = kNetError;
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = YES;
            self.finalView.hidden = YES;
            self.finalView.image = [UIImage imageNamed:@"failure"];
            [self.refreshingView stopAnimating];
            break;
            
        case KYRefreshStateNoResponse:
            self.label.text = kNetError;
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = YES;
            self.finalView.hidden = YES;
            self.finalView.image = [UIImage imageNamed:@"failure"];
            [self.refreshingView stopAnimating];
            break;
        default:
            break;
    }
    
}

- (void)endRefreshing {
    [self endRefreshingWithState:KYRefreshStateSuccess];
}

- (void)endRefreshingFailureWithError:(NSError *)error{
    
    // 判断是没有网络还是链接超时，还是服务器出错
    // - 1009 没有网络   -1001 网络超时
    // footer不做超时和无网的区分，header是因为超时要提示失败，无网显示正在刷新
    if (error.code == -1009 || error.code == -1001 ) {
        [self endRefreshingWithState:KYRefreshStateNoNetwork];
    } else {
        [self endRefreshingWithState:KYRefreshStateNoResponse];
    }
}

- (void)endRefreshingWithState:(MJRefreshState)state {
    // 内容不足一屏幕 or 禁止上拉刷新，上拉无文字显示
    if ([self heightForContentBreakView] < 0 ||
        self.isProhibitFooter){
        self.label.text = @"";
    }
    // 延迟小人消失
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [Tools hideMBProgressHUD];
    });
    // 是否禁用滚动
    [self EnabledScrollWithState:state];
    
    // 解决无网络不能上拉刷新bug
    if (state == KYRefreshStateNoNetwork) {
        self.state = state;
        [_tableView setNoNet];
        [self initialState];
    }
    
    
    if (self.state != MJRefreshStateRefreshing &&
        self.state != MJRefreshStateNoMoreData ) {
        return;
    }
    
    self.oldState2 = self.state;
    
    self.state = state;
    
    //获取表视图
    if ([self.scrollView isKindOfClass:[UITableView class]]) {
        _tableView = (UITableView *)self.scrollView;
    }
    
    if (state == MJRefreshStateNoMoreData) {
        // 移除头视图
        [self clearTableHeadView];
        // 不重置为初始值MJRefreshStateIdle
        // [self initialState];
    }else if (state == KYRefreshStateSuccess) {
        // 移除头视图
        [self clearTableHeadView];
        [self initialState];
    }else if(state == KYRefreshStateFail) {
        
        [self initialState];
        
    }else if(state == KYRefreshStateNoNetwork) {
        
        self.isDisplayHeader = YES;
        [_tableView setNoNet];
        [self initialState];
        
    }else if(state == KYRefreshStateNoResponse) {
        
        self.isDisplayHeader = YES;
        [_tableView setNoResponse];
        [self initialState];
    }
}


- (void)initialState {
    
    if (self.state == MJRefreshStateIdle ||
        self.state == MJRefreshStateNoMoreData) return;
    
    CGFloat duration = 1;//无网络
    if (self.state == KYRefreshStateSuccess) {//成功
        duration = 0.35;
    }else if (self.state == MJRefreshStateRefreshing){//
        duration = 0.25;
    }else if (self.state == KYRefreshStateNoNetwork){
        duration = 0.25; // CAI 0.1
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(duration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //设置无数据视图
        if (self.state != KYRefreshStateNoNetwork &&
            self.state != KYRefreshStateNoResponse ) {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                // 移除表头
                [self clearTableHeadView];
                
                if ([_tableView getTotalDataCount] == 0) {
                    [_tableView setNoData];
                }else{
                    [_tableView hideStatusView];
                }
            });
        }
        // 恢复初始值
        self.state = MJRefreshStateIdle;
    });
}

#pragma mark 监听拖拽比例（控件被拖出来的比例）
- (void)setPullingPercent:(CGFloat)pullingPercent
{
    [super setPullingPercent:pullingPercent];
    
    // 1.0 0.5 0.0
    // 0.5 0.0 0.5
    //    CGFloat red = 1.0 - pullingPercent * 0.5;
    //    CGFloat green = 0.5 - 0.5 * pullingPercent;
    //    CGFloat blue = 0.5 * pullingPercent;
    //    self.label.textColor = [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
}
#pragma mark - 清除表头
- (void)clearTableHeadView {
    _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KSCREEN_W, CGFLOAT_MIN)];
    
}
#pragma mark - 禁用和开启滚动
- (void)EnabledScrollWithState:(MJRefreshState)state {
    self.scrollView.scrollEnabled = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (self.scrollView.getTotalDataCount == 0) {
                self.scrollView.scrollEnabled = NO;
            }
        });
    });
    
    
    // 只在无法连接服务器or超时的时候才不能滚动
    //    if (state == KYRefreshStateNoNetwork  ||
    //        state == KYRefreshStateNoResponse ) {
    //        if (self.scrollView.getTotalDataCount == 0) {
    //            self.scrollView.scrollEnabled = NO;
    //        }
    //    }
}
#pragma mark - 根据网络状态清除表头
- (void)clearTableHeadViewByNetWork {
    if ([Tools isNoNetwork]) {
        // doNothing
    } else {
        // 有网就清除
        [self clearTableHeadView];
    }
}
#pragma mark 获得scrollView的内容 超出 view 的高度
- (CGFloat)heightForContentBreakView
{
    CGFloat h = self.scrollView.frame.size.height - self.scrollViewOriginalInset.bottom - self.scrollViewOriginalInset.top;
    return self.scrollView.contentSize.height - h;
}

@end
