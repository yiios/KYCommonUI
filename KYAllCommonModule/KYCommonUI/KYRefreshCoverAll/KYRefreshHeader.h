//
//  KYRefreshHeader.h
//  kyExpress_Internal
//
//  Created by MelissaShu on 17/4/21.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "MJRefreshHeader.h"
#import "KYRefreshBaseHeader.h"

@interface KYRefreshHeader : KYRefreshBaseHeader

/** 创建header */
+ (instancetype)headerWithRefreshingBlock:(MJRefreshComponentRefreshingBlock)refreshingBlock;
/** 创建header */
+ (instancetype)headerWithRefreshingTarget:(id)target refreshingAction:(SEL)action;

@end
