//
//  UITableView+NetDataStatus.h
//  kyExpress_Internal
//
//  Created by MelissaShu on 17/4/10.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (NetDataStatus)
- (void)setNoResponse;
- (void)setNoNet;
- (void)setNoData;
- (void)setContentNoData;
@end
