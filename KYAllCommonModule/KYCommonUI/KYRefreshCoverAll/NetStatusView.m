//
//  NetStatusView.m
//  CustomView
//
//  Created by iOS_Chris on 17/3/23.
//  Copyright © 2017年 iOS_Chris. All rights reserved.
//

#import "NetStatusView.h"
//#import "UIColor+Hexadecimal.h"

@interface NetStatusView()

@property (nonatomic,strong) UIImageView *imgView;
@property (nonatomic,strong) UILabel *label;

@end

@implementation NetStatusView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self setUpView];
    return self;
}

- (void)setUpView{
    
    self.backgroundColor = [UIColor colorWithHexString:@"ffd4bf"];
    
    self.imgView.frame =  CGRectMake(8, 10, 19, 19);
    CGFloat labelX = CGRectGetMaxX(self.imgView.frame) + 5;
    self.label.frame = CGRectMake(labelX, 0, kSCREEN_WIDTH - labelX, 40);
    
    [self addSubview:self.imgView];
    [self addSubview:self.label];
    
    [self setNetErrorStyle];

}


//网络请求失败
- (void)setNetErrorStyle{
    
    self.label.text = kNetError;
}

//服务器无响应
- (void)setNoResponseStyle{
    
    self.label.text = kNetError;
}


-(UIImageView *)imgView
{
    if (!_imgView) {
        _imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"warning"]];
        _imgView.frame = CGRectMake(0, 0, 19, 19);
    }
    return _imgView;
}

-(UILabel *)label
{
    if (!_label) {
        _label = [[UILabel alloc]init];
        _label.font = [UIFont systemFontOfSize:12];
        _label.textColor = [UIColor colorWithHexString:@"616161"];
    }
    return _label;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
