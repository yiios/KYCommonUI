//
//  KYRefreshBaseHeader.m
//  kyExpress_Internal
//
//  Created by MelissaShu on 17/4/21.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "KYRefreshBaseHeader.h"
#import "Tools.h"

@interface KYRefreshBaseHeader()
@property (assign, nonatomic) CGFloat insetTDelta;
@end

@implementation KYRefreshBaseHeader


#pragma mark - 覆盖父类的方法
- (void)prepare
{
    [super prepare];
    
    // 设置key
    self.lastUpdatedTimeKey = MJRefreshHeaderLastUpdatedTimeKey;
    
    // 设置高度
    self.mj_h = MJRefreshHeaderHeight;
}

- (void)placeSubviews
{
    [super placeSubviews];
    
    // 设置y值(当自己的高度发生改变了，肯定要重新调整Y值，所以放到placeSubviews方法中设置y值)
    self.mj_y = - self.mj_h - self.ignoredScrollViewContentInsetTop;
    
}

- (void)scrollViewContentOffsetDidChange:(NSDictionary *)change
{
    [super scrollViewContentOffsetDidChange:change];
    
    //FIXME: 修复无网络状态，下拉刷新insetT计算不正确
    if (self.state == KYRefreshStateNoNetwork) {
        [UIView animateWithDuration:MJRefreshFastAnimationDuration animations:^{
            self.scrollView.mj_insetT = MJRefreshHeaderHeight;
        }];
    }
    
    // 在刷新的refreshing状态
    if (self.state == MJRefreshStateRefreshing) {
        if (self.window == nil) return;
        
        // sectionheader停留解决
        CGFloat insetT = - self.scrollView.mj_offsetY > _scrollViewOriginalInset.top ? - self.scrollView.mj_offsetY : _scrollViewOriginalInset.top;
        insetT = insetT > self.mj_h + _scrollViewOriginalInset.top ? self.mj_h + _scrollViewOriginalInset.top : insetT;
        self.scrollView.mj_insetT = insetT;
        
        self.insetTDelta = _scrollViewOriginalInset.top - insetT;
        return;
    }
    
    // 跳转到下一个控制器时，contentInset可能会变
    _scrollViewOriginalInset = self.scrollView.contentInset;
    
    // 当前的contentOffset
    CGFloat offsetY = self.scrollView.mj_offsetY;
    
    // 头部控件刚好出现的offsetY
    CGFloat happenOffsetY = - self.scrollViewOriginalInset.top;
    
    // 如果是向上滚动到看不见头部控件，直接返回
    // >= -> >
    if (offsetY > happenOffsetY) return;
    
    // 普通 和 即将刷新 的临界点
    CGFloat normal2pullingOffsetY = happenOffsetY - self.mj_h;
    CGFloat pullingPercent = (happenOffsetY - offsetY) / self.mj_h;
    
    if (self.scrollView.isDragging) { // 如果正在拖拽
        
        self.pullingPercent = pullingPercent;
        
        if (self.state == MJRefreshStateIdle) {
            
            self.state = MJRefreshStateDraging;
        }else{
            //            NSLog(@"drag state : %d",self.state);
        }
        
        //        NSLog(@"offsetY : %f - %f",normal2pullingOffsetY,offsetY);
        
        if (self.state == MJRefreshStateDraging && offsetY < normal2pullingOffsetY) {
            // 转为即将刷新状态
            self.state = MJRefreshStatePulling;
            
        } else if (self.state == MJRefreshStatePulling && offsetY >= normal2pullingOffsetY) {
            // 转为普通状态
            self.state = MJRefreshStateDraging;
        }
    } else if (self.state == MJRefreshStatePulling) {// 即将刷新 && 手松开
        if ([Tools isNoNetwork]) {
            
            [self endRefreshingWithState:KYRefreshStateNoNetwork];
        }else{
            // 开始刷新
            [self beginRefreshing];
        }
    } else if (pullingPercent < 1) {
        //        self.state = MJRefreshStateIdle;
        self.pullingPercent = pullingPercent;
        
    }
}

- (void)setState:(MJRefreshState)state
{
    MJRefreshCheckState
    
    // 根据状态做事情
    if (state == MJRefreshStateIdle) {
        
        //刷新完毕
        if (oldState == MJRefreshStateRefreshing || oldState > 4) {
            
            // 保存刷新时间
            [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:self.lastUpdatedTimeKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            // 恢复inset和offset
            [UIView animateWithDuration:MJRefreshSlowAnimationDuration animations:^{
                
                //FIXME: 跳动
                //self.scrollView.mj_insetT += self.insetTDelta;
                self.scrollView.mj_insetT = 0;
                
                // 自动调整透明度
                if (self.isAutomaticallyChangeAlpha) self.alpha = 0.0;
            } completion:^(BOOL finished) {
                self.pullingPercent = 0.0;
                
                if (self.endRefreshingCompletionBlock) {
                    self.endRefreshingCompletionBlock();
                }
            }];
        };
        
        // 无网络也执行回调
    } else if (state == MJRefreshStateRefreshing ||
               state == KYRefreshStateNoNetwork) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:MJRefreshFastAnimationDuration animations:^{
                
                CGFloat top = self.scrollViewOriginalInset.top + self.mj_h;
                
                // 增加滚动区域top
                self.scrollView.mj_insetT = top;
                // 设置滚动位置
                [self.scrollView setContentOffset:CGPointMake(0, -top) animated:NO];
            } completion:^(BOOL finished) {
                
                [self executeRefreshingCallback];
            }];
        });
    }
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
}

#pragma mark - 公共方法
- (void)endRefreshing
{
    if ([self.scrollView isKindOfClass:[UICollectionView class]]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [super endRefreshing];
        });
    } else {
        [super endRefreshing];
    }
}

- (NSDate *)lastUpdatedTime
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:self.lastUpdatedTimeKey];
}

//----王凯  重新加载数据的回调（在显示无网络，或是服务器无响应视图，点击的时候调用）
//- (void)setReloadDataBlock:(ReloadDataBlock)reloadDataBlock
//{
//    _reloadDataBlock = reloadDataBlock;
//}


- (void)endRefreshingWithState:(MJRefreshState)state {}

- (void)endRefreshingFailureWithError:(NSError *)error{}



@end
