//
//  VertifTableViewCell.h
//  kyExpress
//
//  Created by 陈志刚 on 15/12/7.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^BlockToCountDown)(void);
typedef void(^BlockToString)(NSString *string);
typedef void(^BlockBeginEdit)(void);


@interface newVertifTableViewCell : UITableViewCell

@property (nonatomic, copy) BlockToCountDown  blockToCountDown;
@property (nonatomic, copy) BlockToString  blockToString;

@property (nonatomic, copy) BlockBeginEdit blockbeginEdit;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UITextField *textField;

@property (weak, nonatomic) IBOutlet UIButton *sendVerBtn;



- (void)setBlockToCountDown:(BlockToCountDown)blockToCountDown;
- (void)setBlockToString:(BlockToString)blockToString;

+ (UINib *)nib;

- (void)refreshUIWithTitle:(NSString *)title place:(NSString *)place;

- (void)setupCountDownLabel;

- (void)stopCountDownLabel;


@end
