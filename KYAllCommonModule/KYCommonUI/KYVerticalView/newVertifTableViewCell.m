//
//  VertifTableViewCell.m
//  kyExpress
//
//  Created by 陈志刚 on 15/12/7.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import "newVertifTableViewCell.h"
#import <MZTimerLabel.h>

@interface newVertifTableViewCell ()<UITextFieldDelegate>

//@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
//@property (weak, nonatomic) IBOutlet UIImageView *imagePic;

//@property (weak, nonatomic) IBOutlet UILabel *sendVerBtn;


@property (nonatomic, assign) BOOL isFirstAuth;

@property (nonatomic, strong) MZTimerLabel * timer;

@end


@implementation newVertifTableViewCell

+ (UINib *)nib{
    return [UINib nibWithNibName:@"newVertifTableViewCell" bundle:nil];
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    self.titleLabel.font = KTextFont;
    self.titleLabel.textColor = kColorTextBlack;

    
    self.backgroundColor = [UIColor colorWithHexString:@"0xf5f5f5"];

    self.sendVerBtn.layer.cornerRadius = 5;
    self.sendVerBtn.clipsToBounds = YES;
    
    //self.buttonImage.hidden = YES;
    //self.sendVerBtn.backgroundColor = KMain_Color;
    
    //self.titleLabel.font = KTextFont;
    self.textField.font = STextFont;
    self.textField.textColor = kColorTextBlack;
   // self.titleLabel.textColor = kColorTextBlack;
    
    
    [self.sendVerBtn addTarget:self action:@selector(onTime) forControlEvents:UIControlEventTouchUpInside];
    
    [self.sendVerBtn setTitleColor:[UIColor colorWithHexString:@"0x7c0fbb"] forState:UIControlStateNormal];    
    self.sendVerBtn.backgroundColor = [UIColor colorWithHexString:@"0xf5f5f5"];


    
    self.sendVerBtn.layer.borderWidth = 1;
    self.sendVerBtn.layer.borderColor = [[UIColor colorWithHexString:@"0x7c0fbb"]  CGColor];
    self.textField.delegate = self;
}

- (void)setBlockToCountDown:(BlockToCountDown)blockToCountDown{
    _blockToCountDown = [blockToCountDown copy];
}
- (void)setBlockToString:(BlockToString)blockToString{
    _blockToString = [blockToString copy];
}

- (void)onTime{
    
   // self.sendVerBtn.backgroundColor = KMain_Color;

    if (_blockToCountDown) {
        _blockToCountDown();
    }
    
}


- (void)setupCountDownLabel
{
    MZTimerLabel * timer = [[MZTimerLabel alloc]initWithLabel:self.sendVerBtn.titleLabel andTimerType:MZTimerLabelTypeTimer];
    timer.timeFormat = @"(mm:ss)秒";
    [timer setCountDownTime:120];
    [timer start];

    self.timer = timer;

    self.sendVerBtn.userInteractionEnabled = NO;
    [timer startWithEndingBlock:^(NSTimeInterval countTime) {
        self.sendVerBtn.userInteractionEnabled = YES;
        timer.timeLabel.text = @"获取验证码";
        self.isFirstAuth=NO;
    }];
}

- (void)stopCountDownLabel
{
    [self.timer pause];
    self.sendVerBtn.userInteractionEnabled = YES;
    
    self.timer.timeLabel.text = @"获取验证码";
    
    //    [timer startWithEndingBlock:^(NSTimeInterval countTime) {
    //        self.sendVerBtn.userInteractionEnabled = YES;
    //        timer.timeLabel.text = @"获取验证码";
    //        self.isFirstAuth=NO;
    //    }];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (_blockToString) {
        _blockToString(textField.text);
    }
}


- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (_blockbeginEdit) {
        _blockbeginEdit();
    }
}


//-(UIImageView *)imageView
//{
//    if (!_imageView) {
//        _imageView  = [[UIImageView alloc] initWithFrame:CGRectMake(100, 300, 100, 100)];
//        
//        _imageView.image = [UIImage imageNamed:@"2393517689941ABF8E71AB0474438797.jpg"];
//        
//    }
//    return _imageView;
//}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)refreshUIWithTitle:(NSString *)title place:(NSString *)place{
    [self.titleLabel setText:title];
   // self.imagePic.image = [UIImage imageNamed:title];
    [self.textField setPlaceholder:place];
}



@end
