//
//  KyeImageView.m
//  SizeImage
//
//  Created by wangkai on 16/8/5.
//  Copyright © 2016年 wangkai. All rights reserved.
//

#import "KyeImageView.h"
#import "Tools.h"

@interface KyeImageView ()<UIScrollViewDelegate>

@end

@implementation KyeImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
        self.delegate = self;
        self.frame = CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT);
    }
    return self;
}

- (void)initImageViewWithImage:(UIImage *)image{
    
    NSLog(@"kyeImageView image: %f , %f",image.size.width , image.size.height);
    
    self.image = image;
    // 根据屏幕的宽度，算出同比缩小的图片的高度
    
    NSLog(NSStringFromCGRect(self.frame));
    
    CGFloat height = image.size.height / image.size.width * CGRectGetWidth(self.frame);
    
    NSLog(@"等比例缩放后 ： %f , %f",CGRectGetWidth(self.frame), height);
    
    // 设置imageview
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), height)];
    imageView.image = image;
    imageView.userInteractionEnabled = YES;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.backgroundColor = [UIColor blackColor];
    
    
    UITapGestureRecognizer *tapGesture =[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(tapGesture:)];
    UITapGestureRecognizer* tapTwo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(twpTapGesture:)];
    tapTwo.numberOfTapsRequired = 2;
    
    [tapGesture requireGestureRecognizerToFail:tapTwo];  // 检测不到taptwo手势时才执行第一个手势
    [imageView addGestureRecognizer:tapGesture];
    [imageView addGestureRecognizer:tapTwo];
    
    
    UILongPressGestureRecognizer *longPressGes = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(LongPressGesAction:)];
    longPressGes.minimumPressDuration = 1;
    [imageView addGestureRecognizer:longPressGes];
    
    
    // 设置scrollview的滚动范围
    self.contentSize = CGSizeMake(CGRectGetWidth(self.frame), height);
    
    
    if(height < CGRectGetHeight(self.frame))
    {
        imageView.center = self.center;  // 如果图片的高度小于当前页面的高度，就让该图片居中
    }
    self.maximumZoomScale = 4.0;
    [self addSubview:imageView];
    self.backgroundColor = [UIColor blackColor];
    
}

- (void)initImageViewWithImageCode:(NSString *)imageCode
{
    // 把base64编码转化成图片
    if (imageCode == NULL) {
        [Tools myToast:@"该资产没有图片档案"];
        return;
    }
    
    NSData *imageData = [[NSData alloc] initWithBase64EncodedString:imageCode options:NSDataBase64DecodingIgnoreUnknownCharacters];
    
    UIImage *image = [UIImage imageWithData:imageData];
    //NSLog(@"image = %@",image);
    
    self.image = image;
    if (image != NULL) {
        [self initImageViewWithImage:image];
        
    }else{
        [Tools myToast:@"该资产没有图片档案"];
        
    }
    
}

-(void)tapGesture:(UITapGestureRecognizer *)gesture
{
    
    if (self.blockImgDidTap) {
        _blockImgDidTap();
        return;
    }
    [self removeFromSuperview];
    
}

- (void)twpTapGesture:(UITapGestureRecognizer *)geture
{
    [self setZoomScale:1 animated:YES];
    NSLog(@"点击了两次还原图片");
}


-(void)LongPressGesAction:(UILongPressGestureRecognizer *)longPressGes{
    
    //NSLog(@"点击图片执行分享");
    if (_blockLongPressGesAction) {
        _blockLongPressGesAction(longPressGes);
    }
    
}

#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return imageView;
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    scrollView.scrollEnabled = YES;
    [scrollView setZoomScale:scale animated:NO];
    
}

//实现图片在缩放过程中居中
- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?(scrollView.bounds.size.width - scrollView.contentSize.width)/2 : 0.0;
    CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)?(scrollView.bounds.size.height - scrollView.contentSize.height)/2 : 0.0;
    imageView.center = CGPointMake(scrollView.contentSize.width/2 + offsetX,scrollView.contentSize.height/2 + offsetY);
}

#pragma mark -

-(void)setBlockImgDidTap:(BlockImgDidTap)blockImgDidTap
{
    _blockImgDidTap = blockImgDidTap;
}


@end
