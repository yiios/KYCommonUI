//
//  KyeImageView.h
//  SizeImage
//
//  Created by wangkai on 16/8/5.
//  Copyright © 2016年 wangkai. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^BlockImgDidTap)(void);
typedef void(^BlockLongPressGesAction)();


@interface KyeImageView : UIScrollView

{
    UIImageView *imageView;
}

@property (nonatomic, retain) UIImageView *imageView;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic,copy) BlockImgDidTap blockImgDidTap;
@property (nonatomic,copy) BlockLongPressGesAction blockLongPressGesAction;


/**
 *   根据图片创建一个自定义的imageview
 *
 *  @param image 需要展示的iamge
 */
- (void)initImageViewWithImage:(UIImage *)image;

/**
 *  根据base64编码加载图片
 *
 *  @param imageCode 图片的base64编码
 */
- (void)initImageViewWithImageCode:(NSString *)imageCode;

-(void)setBlockImgDidTap:(BlockImgDidTap)blockImgDidTap;
@end
