//
//  BorderInputView.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/11/10.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "BorderInputView.h"

@interface BorderInputView()

@property (nonatomic, copy) NSString *placeHolder;

@end

@implementation BorderInputView

- (void)setPlaceStr:(NSString *)placeStr{

    self.placeHolder = placeStr;
    
    self.text = self.placeHolder;
    self.textColor = kColorPlaceHolder;
//    self.textColor = KColorRed;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    
    self = [super initWithFrame:frame];
    
//    [self setUpViews];
    
    return self;
}

- (NSString *)getText{

    if ([self.text isEqualToString:self.placeHolder]) {
        return @"";
    }
    
    return self.text;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
   
    self.layer.cornerRadius = 3;
//    self.layer.shouldRasterize = YES;
    self.clipsToBounds = YES;
    self.layer.borderColor = KBack_Color.CGColor;
    self.layer.borderWidth = 1;
    self.backgroundColor = [UIColor whiteColor];
//    self.frame = CGRectMake(kMargin, detailY, kSCREEN_WIDTH - kMargin*2, detailH);
    
    self.font = KTextFont;
    
    if ([self.text isEqualToString:self.placeHolder]) {
        
        self.textColor = kColorPlaceHolder;
    
    }else{
//        self.text = self.text;
        self.textColor = [UIColor blackColor];
    }

}


@end
