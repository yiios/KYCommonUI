//
//  BorderInputView.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/11/10.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BorderInputView : UITextView

- (void)setPlaceStr:(NSString *)placeStr;

- (NSString *)getText;

@end


