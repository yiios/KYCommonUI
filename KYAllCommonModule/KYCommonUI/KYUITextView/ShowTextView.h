//
//  ShowTextView.h
//  161209-Paste
//
//  Created by iOS_Chris on 16/12/10.
//  Copyright © 2016年 iOS_Chris. All rights reserved.
//  用于展示IDETextView,可复制，不能粘贴、剪切

#import <UIKit/UIKit.h>

@interface ShowTextView : UITextView

@end
