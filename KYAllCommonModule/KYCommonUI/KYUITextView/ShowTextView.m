//
//  ShowTextView.m
//  161209-Paste
//
//  Created by iOS_Chris on 16/12/10.
//  Copyright © 2016年 iOS_Chris. All rights reserved.
//

#import "ShowTextView.h"

@implementation ShowTextView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
//    NSLog(@"action : %@",NSStringFromSelector(action));
    
    if (action == @selector(cut:)){
        NSLog(@"禁用剪切 cut ");
        return NO;
    }
    
    if (action == @selector(paste:)){
        NSLog(@"禁用粘贴 cut ");
        return NO;
    }
    
    return [super canPerformAction:action withSender:sender];
}

@end
