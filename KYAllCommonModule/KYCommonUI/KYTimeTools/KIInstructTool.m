//
//  KIInstructTools.m
//  kyExpress_Internal
//
//  Created by wangkai on 2017/12/26.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "KIInstructTool.h"

@implementation KIInstructTool
static KIInstructTool *_instance;
+(instancetype)allocWithZone:(struct _NSZone *)zone{
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (_instance == nil) {
            
            _instance = [super allocWithZone:zone];
            
        }
    });
    return _instance;
}
+(instancetype)shareInstructTool
{
    //return _instance;
    return [[self alloc]init];
}
// 为了严谨，也要重写copyWithZone 和 mutableCopyWithZone
-(id)copyWithZone:(NSZone *)zone
{
    return _instance;
}
-(id)mutableCopyWithZone:(NSZone *)zone
{
    return _instance;
}
//- (void)AlertNSArrayPickType:(PGDatePickerMode)pickType DefaultData:(NSString *)defaultData TimePickerClick:(void(^)(NSString *str,NSDate *date))timePickerClick{
//}
                                                                                                                    
- (void)AlertNStimePickType:(PGDatePickerMode)pickType DefaultData:(NSString *)defaultData TimePickerClick:(void(^)(NSString *str,NSDate *date))timePickerClick{
    if(pickType==nil){
        self.pickType=pickType;
    }
    
    [self AlertNStimeView:nil PickType:self.pickType DefaultData:defaultData MinDate:self.minimumDate MaxDate:self.maximumDate TimePickerClick:timePickerClick];
    
}

- (void)AlertPickerArray:(NSArray *)array TimePickerClick:(void(^)(NSString *str,NSDate *date))timePickerClick{
    
    _timePickerClose=nil;
    PGDatePicker *datePicker = [[PGDatePicker alloc]init];
    datePicker.arrayList=array;
    datePicker.delegate = self;
    [datePicker show];
    datePicker.datePickerType = PGPickerViewType3;
    
    datePicker.datePickerMode =PGDatePickerModeArray;
    datePicker.titleLabel.text = @"";
    //设置线条的颜色
    datePicker.lineBackgroundColor =[UIColor colorWithHexString:@"7C0FBB"];
    //设置选中行的字体颜色
    datePicker.textColorOfSelectedRow =[UIColor colorWithHexString:@"7C0FBB"];
    //设置未选中行的字体颜色
    datePicker.textColorOfOtherRow = [UIColor blackColor];
    //设置取消按钮的字体颜色
    datePicker.cancelButtonTextColor = [UIColor grayColor];
    
    //设置取消按钮的字
    datePicker.cancelButtonText = @"取消";
    //设置取消按钮的字体大小
    datePicker.cancelButtonFont = [UIFont boldSystemFontOfSize:15];
    
    datePicker.confirmButtonTextColor = [UIColor colorWithHexString:@"7C0FBB"];
    //设置确定按钮的字
    datePicker.confirmButtonText = @"确定";
    //设置确定按钮的字体大小
    datePicker.confirmButtonFont = [UIFont boldSystemFontOfSize:15];
    _timePickerClick=timePickerClick;
}
- (void)AlertNStimeView:(UIView *)pView PickType:(PGDatePickerMode)pickType DefaultData:(NSString *)defaultData MinDate:(NSDate *)minData MaxDate:(NSDate *)maxDate TimePickerClick:(void(^)(NSString *str,NSDate *date))timePickerClick{
    _timePickerClose=nil;
    PGDatePicker *datePicker = [[PGDatePicker alloc]init];
    datePicker.delegate = self;
    [datePicker show];
    datePicker.datePickerType = PGPickerViewType3;
    
    datePicker.datePickerMode =
    pickType;
    if(minData!=nil) {
        datePicker.minimumDate=minData;
    }
    if(maxDate!=nil) {
        datePicker.maximumDate=maxDate;
    }

    if(defaultData!=nil && ![defaultData isEqualToString:@""]){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
  
        if(pickType==PGDatePickerModeYear)
        {
            dateFormatter.dateFormat=@"yyyy";
        }else if(pickType==PGDatePickerModeYearAndMonth){
              dateFormatter.dateFormat=@"yyyy-MM";
            
        }else if(pickType==PGDatePickerModeDate){
              dateFormatter.dateFormat=@"yyyy-MM-dd";
        }else if(pickType==PGDatePickerModeDateHourMinute){
             dateFormatter.dateFormat=@"yyyy-MM-dd HH:mm";
        }else if(pickType==PGDatePickerModeDateHourMinuteSecond){
            dateFormatter.dateFormat=@"yyyy-MM-dd HH:mm:ss";
        }else if(pickType==PGDatePickerModeTime){
              dateFormatter.dateFormat=@"HH:mm";
        }else if(pickType==PGDatePickerModeTimeAndSecond){
            dateFormatter.dateFormat=@"HH:mm:ss";
        }

        NSDate *date = [dateFormatter dateFromString:defaultData];
        
        [datePicker setDate:date animated:NO];
    }
    
    datePicker.titleLabel.text = @"";
    //设置线条的颜色
    datePicker.lineBackgroundColor =[UIColor colorWithHexString:@"7C0FBB"];
    //设置选中行的字体颜色
    datePicker.textColorOfSelectedRow =[UIColor colorWithHexString:@"7C0FBB"];
    //设置未选中行的字体颜色
    datePicker.textColorOfOtherRow = [UIColor blackColor];
    //设置取消按钮的字体颜色
    datePicker.cancelButtonTextColor = [UIColor grayColor];

    //设置取消按钮的字
    datePicker.cancelButtonText = @"取消";
    //设置取消按钮的字体大小
    datePicker.cancelButtonFont = [UIFont boldSystemFontOfSize:15];
    
    datePicker.confirmButtonTextColor = [UIColor colorWithHexString:@"7C0FBB"];
    //设置确定按钮的字
    datePicker.confirmButtonText = @"确定";
    //设置确定按钮的字体大小
    datePicker.confirmButtonFont = [UIFont boldSystemFontOfSize:15];
    _timePickerClick=timePickerClick;
   
}
-(void)setTimePickerClose:(void (^)())timePickerClose
{
    _timePickerClose=timePickerClose;
}
-(void)datePickerClose:(PGDatePicker *)datePicker
{
    self.minimumDate=nil;
    self.maximumDate=nil;
    if(self.timePickerClose!=nil){
        self.timePickerClose();
    }
}

- (void)close:(UITapGestureRecognizer *)gesture
{
   
}

- (void)datePicker:(PGDatePicker *)datePicker didSelectDate:(NSDateComponents *)dateComponents {
    
    _timePickerClose=nil;
    self.minimumDate=nil;
    self.maximumDate=nil;
     NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    NSString  *month=(dateComponents.month<10)?[NSString stringWithFormat:@"0%ld",dateComponents.month]:[NSString stringWithFormat:@"%ld",dateComponents.month];
    
    NSString  *day=(dateComponents.day<10)?[NSString stringWithFormat:@"0%ld",dateComponents.day]:[NSString stringWithFormat:@"%ld",dateComponents.day];
    
    NSString  *hour=(dateComponents.hour<10)?[NSString stringWithFormat:@"0%ld",dateComponents.hour]:[NSString stringWithFormat:@"%ld",dateComponents.hour];
    
    NSString  *minute=(dateComponents.minute<10)?[NSString stringWithFormat:@"0%ld",dateComponents.minute]:[NSString stringWithFormat:@"%ld",dateComponents.minute];
    
    NSString  *second=(dateComponents.second<10)?[NSString stringWithFormat:@"0%ld",dateComponents.second]:[NSString stringWithFormat:@"%ld",dateComponents.second];
    NSString *wholeResult=@"";
    if(datePicker.datePickerMode==PGDatePickerModeYear)
    {
        wholeResult = [NSString stringWithFormat:@"%ld",dateComponents.year];
        dateFormatter.dateFormat=@"yyyy";
    }else if(datePicker.datePickerMode==PGDatePickerModeYearAndMonth){
        wholeResult = [NSString stringWithFormat:@"%ld-%@",dateComponents.year,month];
         dateFormatter.dateFormat=@"yyyy-MM";
    }else if(datePicker.datePickerMode==PGDatePickerModeDate){
        wholeResult = [NSString stringWithFormat:@"%ld-%@-%@",dateComponents.year,month,day];
         dateFormatter.dateFormat=@"yyyy-MM-dd";
    }else if(datePicker.datePickerMode==PGDatePickerModeDateHourMinute){
         wholeResult= [NSString stringWithFormat:@"%ld-%@-%@ %@:%@",dateComponents.year,month,day,hour,minute];
        dateFormatter.dateFormat=@"yyyy-MM-dd HH:mm";
        
    }else if(datePicker.datePickerMode==PGDatePickerModeDateHourMinuteSecond){
         wholeResult = [NSString stringWithFormat:@"%ld-%@-%@ %@:%@:%@",dateComponents.year,month,day,hour,minute,second];
        dateFormatter.dateFormat=@"yyyy-MM-dd HH:mm:ss";
    }else if(datePicker.datePickerMode==PGDatePickerModeTime){
        wholeResult = [NSString stringWithFormat:@"%@:%@",hour,minute];
         dateFormatter.dateFormat=@"HH:mm";
    }else if(datePicker.datePickerMode==PGDatePickerModeTimeAndSecond){
        wholeResult = [NSString stringWithFormat:@"%@:%@:%@",hour,minute,second];
        dateFormatter.dateFormat=@"HH:mm:ss";
    }
//    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
//    [dateFormatter setTimeZone:timeZone];
    NSDate *date = [dateFormatter dateFromString:wholeResult];
    NSLog(@"dateComponent__________%@___________%@", date,wholeResult);
   
    self.timePickerClick(wholeResult,date);
   
}

-(void)datePicker:(PGDatePicker *)datePicker didSelectStr:(NSString *)selectStr
{
    _timePickerClose=nil;
     self.timePickerClick(selectStr,nil);
}

@end

