//
//  KIInstructTools.h
//  kyExpress_Internal
//
//  Created by wangkai on 2017/12/26.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PGDatePicker.h"

@interface KIInstructTool : NSObject<PGDatePickerDelegate>

@property (nonatomic, copy) void(^timePickerClick)(NSString *str,NSDate *date);
@property (nonatomic, copy) void(^timePickerClose)();
@property (nonatomic, copy) NSDate *maximumDate ;
@property (nonatomic, copy) NSDate *minimumDate ;
@property (nonatomic, assign) PGDatePickerMode pickType;

+(instancetype)shareInstructTool;

- (void)AlertPickerArray:(NSArray *)array TimePickerClick:(void(^)(NSString *str,NSDate *date))timePickerClick;

- (void)AlertNStimePickType:(PGDatePickerMode)pickType DefaultData:(NSString *)defaultData TimePickerClick:(void(^)(NSString *str,NSDate *date))timePickerClick;

- (void)AlertNStimeView:(UIView *)pView PickType:(PGDatePickerMode)pickType DefaultData:(NSString *)defaultData MinDate:(NSDate *)minData MaxDate:(NSDate *)maxDate TimePickerClick:(void(^)(NSString *str,NSDate *date))timePickerClick; 
@end
