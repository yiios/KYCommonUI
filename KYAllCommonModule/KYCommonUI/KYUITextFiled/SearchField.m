//
//  SearchField.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/9/14.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "SearchField.h"
#import "UIView+MJExtension.h"

@implementation SearchField

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    [self setUp];
    
    self.layer.cornerRadius = self.mj_h * 0.5;
    self.layer.masksToBounds = YES;
    
    
    return self;
}

- (void)setWhiteStyle{
    UIImageView * imgView = [[UIImageView alloc]init];
    imgView.image = [UIImage imageNamed:@"magnifier"];
    imgView.frame = CGRectMake(4, 0, 13, 13);
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    
    self.leftView = imgView;
    self.backgroundColor = [UIColor whiteColor];
    self.textColor = kColorTextBlack;
    [self setValue:kColorBorderTri forKeyPath:@"_placeholderLabel.textColor"];
}

- (void)setUp{
    
    //设置放大镜
    UIImageView * imgView = [[UIImageView alloc]init];
    imgView.image = [UIImage imageNamed:@"magnifier2"];
    imgView.frame = CGRectMake(4, 0, 13, 13);
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    
    self.leftView = imgView;
    self.leftViewMode = UITextFieldViewModeUnlessEditing;
    
    self.font = kFont14;
    self.textColor = KWhite_Color;

    self.backgroundColor = KMain_Color;
    self.placeholder = @"请输入主题关键字或发布人姓名";
    
    
    [self setValue:[UIColor colorWithWhite:0.9 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self setValue:kFont14 forKeyPath:@"_placeholderLabel.font"];
}

- (CGRect)leftViewRectForBounds:(CGRect)bounds
{
//    CGFloat margin = 3;
//    CGFloat imgH = self.mj_h - margin*2;

    
    CGFloat imgH = 13;
    CGFloat margin = (self.mj_h - imgH) * 0.5;
    CGRect imgRect = CGRectMake(5, margin, imgH, imgH );
    
    
    return imgRect;
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    CGRect editRect = CGRectMake(5, 0, self.mj_w - 4, self.mj_h );
    return editRect;
}

- (CGRect)placeholderRectForBounds:(CGRect)bounds
{
//    NSLog(@"leftView : %@",NSStringFromCGRect(self.leftView.frame));
    CGFloat placeX =CGRectGetMaxX(self.leftView.frame) + 2;
    return CGRectMake(placeX, 0, self.mj_w - placeX, self.mj_h );
//    CGFloat textW = 130;
//     return CGRectMake((self.mj_w - textW) * 0.5, 0, textW, self.mj_h);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
