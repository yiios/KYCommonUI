//
//  BorderTextField.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/11/10.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "BorderTextField.h"

@implementation BorderTextField



-(instancetype)initWithFrame:(CGRect)frame
{
    
    self = [super initWithFrame:frame];
    
//    [self setUpViews];
    
    return self;
}


- (void)drawRect:(CGRect)rect {
    
    self.layer.cornerRadius = 3;
//    self.layer.shouldRasterize = YES;
    self.clipsToBounds = YES;
    self.layer.borderColor = KBack_Color.CGColor;
    self.layer.borderWidth = 1;
    self.font = KTextFont;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 1, 5, 34)];
    paddingView.backgroundColor = [UIColor clearColor];
    self.leftView = paddingView;
    self.leftViewMode = UITextFieldViewModeAlways;
   
}


@end
