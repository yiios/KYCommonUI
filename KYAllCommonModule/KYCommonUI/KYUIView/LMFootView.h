//
//  LMFootView.h
//  kyExpress_Internal
//
//  Created by limo on 2017/3/7.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LMFootViewButtonDelegate <NSObject>

-(void)footViewDidSelectedButton:(UIButton *)sender andIndex:(NSInteger)index andButtonTitle:(NSString *)titleText;

@end


@interface LMFootView : UIView

@property (nonatomic, strong) NSMutableArray<UIButton *> *buttonArr;   //!<  保存按钮数组

@property (nonatomic, weak) id <LMFootViewButtonDelegate>delegate;

/**
 初始化

 @param viewController 视图所在的控制器
 @param footViewHeight 视图的高度
 @param titleStrArr    标签的数组(原则上不多于3个)

 @return <#return value description#>
 */
- (instancetype)initWithController:(UIViewController<LMFootViewButtonDelegate> *)viewController andFootViewHeight:(CGFloat)footViewHeight andTitleStrArr:(NSArray<NSString *> *)titleStrArr;

/**
 重设按钮

 @param titles NSString
 */
- (void)setFooterViewWithTitles:(NSArray<NSString *> *)titles;

/**
 设置顶部分隔线是否显示(默认隐藏)

 @param isHide 是否隐藏
 */
- (void)setTopDividerHide:(BOOL)isHide;

@end
