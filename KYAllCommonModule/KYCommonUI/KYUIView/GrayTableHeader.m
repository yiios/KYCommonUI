//
//  GrayTableHeader.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/9/10.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "GrayTableHeader.h"
#import "UIView+MJExtension.h"
#import "Tools.h"

@implementation GrayTableHeader

-(instancetype)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    [self initView];
    return self;
}

- (void)initView{
    [self setAttri];
    [self setLayOut];
}

- (void)setAttri{

    self.backgroundColor = KBack_Color;
    self.label1.font = kFont14;
    self.label2.font = kFont14;
    self.label3.font = kFont14;
    self.label4.font = kFont14;
    
    self.label1.textAlignment = NSTextAlignmentLeft;
    self.label2.textAlignment = NSTextAlignmentLeft;
    self.label4.textAlignment = NSTextAlignmentLeft;
    
    self.label3.textAlignment = NSTextAlignmentCenter;
}

- (void)setLayOut{

    CGFloat label2W = 100;
    CGFloat label3W = 50;
    
    CGFloat label3X = kSCREEN_WIDTH - label3W - 5;
    CGFloat label2X = kSCREEN_WIDTH - label3W - label2W - 10;
    
    CGFloat label1W = kSCREEN_WIDTH - label2X - 25;
    
    self.label1.frame = CGRectMake(15, 5, label1W, 21);
    self.label2.frame = CGRectMake(label2X, 5, label2W, 21);
    self.label3.frame = CGRectMake(label3X, 5, label3W, 21);
    
//    self.label2.backgroundColor = [UIColor cyanColor];
    
    [self addSubview:self.label1];
    [self addSubview:self.label2];
    [self addSubview:self.label3];
    [self addSubview:self.label4];
}

//绩效主界面
- (void)refreshUIForKPIOWithTitles:(NSArray *)titles{

    self.label1.text = titles[0];
    self.label2.text = titles[1];
   
    if (IS_IPHONE_5) {
        
        self.label1.font = kFont12;
        self.label2.font = kFont12;
        
    }else{
        
        self.label1.font = kFont14;
        self.label2.font = kFont14;
    }
    
    CGFloat label1W = 130;
    CGFloat label2W = 130;
    
    self.label1.frame = CGRectMake(15, 0, label1W, self.mj_h);
    self.label2.frame = CGRectMake(CGRectGetMaxX(self.label1.frame), 0, label2W, self.mj_h);
    
}

//绩效详情
- (void)refreshUIForKPIODetailWithTitles:(NSArray *)titles{
    
    self.label1.text = titles[0];
    self.label2.text = titles[1];
    self.label3.text = titles[2];
    self.label4.text = titles[3];
    
    CGFloat label1W = 120; //运单号
 
    CGFloat label3W = 55;
    CGFloat label4W = 55;
    
    self.label3.textAlignment = NSTextAlignmentCenter;
    self.label4.textAlignment = NSTextAlignmentCenter;
    
    self.label1.frame = CGRectMake(kMargin, 0, label1W, self.mj_h);
    self.label3.frame = CGRectMake(KSCREEN_W - label4W - label3W, 0, label3W, self.mj_h);
    self.label4.frame = CGRectMake(KSCREEN_W - label4W  , 0, label4W, self.mj_h);
    
      CGFloat label2W = CGRectGetMinX(self.label3.frame) - CGRectGetMaxX(self.label1.frame) - kMargin;
    self.label2.frame = CGRectMake(KSCREEN_W - label1W - label2W, 0, label2W, self.mj_h);
    
    self.label4.hidden = NO;
    
    
}

//绩效详情-打包件数
- (void)refreshUIForKPIO3DetailWithTitles:(NSArray *)titles{
    
    self.label1.text = titles[0];
    self.label2.text = titles[1];
    self.label3.text = titles[2];
    self.label4.hidden = YES;
    
    CGFloat label1W = 180; //运单号
    
    CGFloat label3W = 65;
   
    
    self.label3.textAlignment = NSTextAlignmentCenter;
    
    self.label1.frame = CGRectMake(20, 0, label1W, self.mj_h);
    self.label3.frame = CGRectMake(KSCREEN_W - label3W, 0, label3W, self.mj_h);
  
    CGFloat label2W = CGRectGetMinX(self.label3.frame) - CGRectGetMaxX(self.label1.frame) - kMargin;
    self.label2.frame = CGRectMake(label1W + 25, 0, label2W, self.mj_h);
    
    
    
}


//绩效详情- 本月绩效
- (void)refreshUIForKPIO3WithTitles:(NSArray *)titles
{
    self.label1.text = titles[0];
    self.label2.text = titles[1];
    self.label3.text = titles[2];
    self.label4.hidden = YES;
    
    CGFloat label1W = 100; //运单号
    
    CGFloat label3W = 100;
    
    
    self.label3.textAlignment = NSTextAlignmentCenter;
    
    self.label1.frame = CGRectMake(20, 0, label1W, self.mj_h);
    self.label3.frame = CGRectMake(KSCREEN_W - label3W - 10, 0, label3W, self.mj_h);
    
    CGFloat label2W = KSCREEN_W - label1W - label3W - 60;
    self.label2.frame = CGRectMake(label1W + 40, 0, label2W, self.mj_h);
}

//绩效详情2
- (void)refreshUIForKPIODetailTwoWithTitles:(NSArray *)titles{
    
    self.label1.text = titles[0];
    self.label2.text = titles[1];
    
    CGFloat label1W = 200; //运单号
    
    
    self.label1.frame = CGRectMake(kMargin, 0, label1W, self.mj_h);
    
    CGFloat label2W = kSCREEN_WIDTH - label1W - kMargin;
    self.label2.frame = CGRectMake( label1W + kMargin, 0, label2W, self.mj_h);
    
    self.label4.hidden = YES;
    self.label3.hidden = YES;
    
}

//通知详情-评论
- (void)refreshUIForNoticeCommentWithUnreadNum:(NSString *)unreadNum
{
    CGFloat labelH = 40;
    
    CGFloat label2W = 150;
    CGFloat label1W = KSCREEN_W - label2W - kMargin;
    
    self.label1.textColor = kColorTextBlack;
    self.label2.textColor = kColorTextGray;
    
    self.label1.font = kFont16;
    
    self.label3.hidden = YES;
    
    self.label1.frame = CGRectMake(kMargin, 0, label1W, labelH);
  
    self.label2.frame = CGRectMake(KSCREEN_W - label2W, 0, label2W, labelH);
    
    self.label1.text = @"热门评论";
    
    if ([Tools isBlankString:unreadNum] || [unreadNum isEqualToString:@"0"]) {
        self.label2.text = @"";
        self.label2.hidden = YES;
    }else{
        
        //设置：在0-3个单位长度内的内容显示成红色
        NSString *contentStr = [NSString stringWithFormat:@"%@ 条未读评论",unreadNum];
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:contentStr];
        NSRange range = [contentStr rangeOfString:unreadNum];
        
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
        self.label2.attributedText = str;
        
        
        self.label2.userInteractionEnabled = YES;
        [self.label2 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(label3OnClick)]];
    }

    
    self.label2.textAlignment = NSTextAlignmentCenter;
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, labelH - 1, kSCREEN_WIDTH, 0.5)];
    lineView.backgroundColor = kColorBorderTri;
    [self addSubview:lineView];
  
}

-(void)setBlockGrayHeaderLabel3OnClick:(BlockGrayHeaderLabel3OnClick)blockGrayHeaderLabel3OnClick
{
    _blockGrayHeaderLabel3OnClick = blockGrayHeaderLabel3OnClick;
}

- (void)label3OnClick{

    if (_blockGrayHeaderLabel3OnClick) {
        _blockGrayHeaderLabel3OnClick();
    }
}


- (void)refreshUIForNoticeListWithTitles:(NSArray *)titles
{
    CGFloat labelH = 30;
    
    CGFloat label1W = 100;
    CGFloat label2W = 55;
    CGFloat label3W = 95;
   
    CGFloat label2X = kSCREEN_WIDTH - label2W - label3W - 7; //
    
    self.label1.frame = CGRectMake(25, 0, label1W, labelH);
    self.label3.frame = CGRectMake(kSCREEN_WIDTH - label3W - 8, 0, label3W, labelH);
    self.label2.frame = CGRectMake(label2X, 0, label2W, labelH);
    
    self.label1.text = titles[0];
    self.label2.text = titles[1];
    self.label3.text = titles[2];
    
    self.label2.textAlignment = NSTextAlignmentCenter;
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, self.mj_h - 1, kSCREEN_WIDTH, 0.5)];
    lineView.backgroundColor = kColorBorderTri;
    [self addSubview:lineView];
    
    self.label1.textColor = kColorTextGray;
    self.label2.textColor = kColorTextGray;
    self.label3.textColor = kColorTextGray;
}

// 招标评审
- (void)refreshUIForTenderReviewWithTitles:(NSArray *)titles{
    
    self.backgroundColor = [UIColor colorWithHexString:@"0xf5f5f5"];
    
    self.label1.text = titles.count>0?titles[0]:@"";
    self.label2.text = titles.count>1?titles[1]:@"";
    self.label3.text = titles.count>2?titles[2]:@"";
    self.label4.hidden = YES;
    
    [self.label1 setTextColor:[UIColor colorWithHexString:@"0x999999"]];
    [self.label2 setTextColor:[UIColor colorWithHexString:@"0x999999"]];
    [self.label3 setTextColor:[UIColor colorWithHexString:@"0x999999"]];
    
    self.label1.textAlignment = NSTextAlignmentLeft;
    self.label2.textAlignment = NSTextAlignmentCenter;
    self.label3.textAlignment = NSTextAlignmentCenter;
    
    CGFloat label2W = 105;
    CGFloat label3W = 80;
    CGFloat label1W = KSCREEN_W - label2W - label3W - kMargin; //运单号
    
    self.label1.frame = CGRectMake(kMargin, 0, label1W, self.mj_h);
    self.label2.frame = CGRectMake(self.label1.mj_x+self.label1.mj_w, 0, label2W, self.mj_h);
    self.label3.frame = CGRectMake(self.label2.mj_x+self.label2.mj_w, 0, label3W, self.mj_h);
    
}


#pragma mark - Getter

-(UILabel *)label1
{
    if (!_label1) {
        _label1 = [[UILabel alloc]init];
    }
    return _label1;
}

-(UILabel *)label2
{
    if (!_label2) {
        _label2 = [[UILabel alloc]init];
    }
    return _label2;
}


-(UILabel *)label3
{
    if (!_label3) {
        _label3 = [[UILabel alloc]init];
    }
    return _label3;
}

-(UILabel *)label4
{
    if (!_label4) {
        _label4 = [[UILabel alloc]init];
        _label4.hidden = YES;
    }
    return _label4;
}

@end
