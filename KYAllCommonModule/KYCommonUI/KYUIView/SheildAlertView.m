//
//  SheildAlertView.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/8/4.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "SheildAlertView.h"

@interface SheildAlertView()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;//标题-温馨提示
//@property (weak, nonatomic) IBOutlet UILabel *messageLabel;//标题-温馨提示
@property (weak, nonatomic) IBOutlet UIButton *cancerBtn;

@property (weak, nonatomic) IBOutlet UIView *alertView;

@end

@implementation SheildAlertView


-(void)setMessage:(NSString *)string
{
    self.messageLabel.text = string;
}

- (instancetype)initSheild
{
    //    self = [super init];
    
    self = [[NSBundle mainBundle] loadNibNamed:@"SheildAlertView" owner:nil options:nil][0];
    self.messageLabel.numberOfLines = 0;
    return self;
}

-(void)setBlockSheildAlertCertainBlock:(BlockSheildAlertCertainBlock)blockSheildAlertCertainBlock
{
    _blockSheildAlertCertainBlock = blockSheildAlertCertainBlock;
}

- (void)setSheildWithTitle:(NSString *)title message:(NSString *)message CertainBlock:(BlockSheildAlertCertainBlock)CertainBlock  CanceBlock:(CanceBlock)CanceBlock
{
    self.titleLabel.text = title;
    self.messageLabel.text = message;
    
    _canceBlock = CanceBlock;
    _blockSheildAlertCertainBlock = CertainBlock;
}


- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message CertainBlock:(BlockSheildAlertCertainBlock)CertainBlock
                   CanceBlock:(CanceBlock)CanceBlock
{
    //    self = [super init];
    
    self = [[NSBundle mainBundle] loadNibNamed:@"SheildAlertView" owner:nil options:nil][0];
    
    self.titleLabel.text = title;
    self.messageLabel.text = message;
    
    _canceBlock = CanceBlock;
    _blockSheildAlertCertainBlock = CertainBlock;
    
    return self;
}
//取消
- (IBAction)canceButton:(id)sender {
    
    if (_canceBlock) {
        _canceBlock();
    }else{
        self.hidden = YES;
    }
}

//确定
- (IBAction)certainButton:(id)sender {
    
    [self endEditing:YES];
    
    if (_blockSheildAlertCertainBlock) {
        
        _blockSheildAlertCertainBlock();
        
    }
}

- (void)drawRect:(CGRect)rect {
    
    [super drawRect:rect];
    
    // Drawing code
    self.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.5];
//    self.tanView.backgroundColor = KWhite_Color;
//    self.tanView.layer.cornerRadius = 5;
//    self.tanView.layer.masksToBounds = YES;
//    self.tanView.layer.borderColor = [[UIColor whiteColor] CGColor];
//    self.tanView.layer.borderWidth = 1.0;
    [self.cancerBtn setTitle:@"取消" forState:UIControlStateNormal];
    [self.certainBtn setTitle:@"确定" forState:UIControlStateNormal];
    
    [self.cancerBtn setTitleColor:KMain_Color forState:UIControlStateNormal];
    [self.certainBtn setTitleColor:KMain_Color forState:UIControlStateNormal];
    
    self.alertView.layer.cornerRadius = 5;
    self.alertView.layer.masksToBounds = YES;
    
}

-(void)setCanceBlock:(CanceBlock)canceBlock
{
    _canceBlock = canceBlock;
}


@end
