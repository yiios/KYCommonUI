//
//  BottomView.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/8/30.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "BottomView.h"

@interface BottomView()

@end

@implementation BottomView

-(instancetype)init
{
    self = [super init];
    self.frame = CGRectMake(0, 0, 0, kBottomH);
    self.backgroundColor = KBack_Color;
    return self;
    
}

-(void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    [self addSubview:self.btn0];
    
    
}


- (instancetype)initWithTitle:(NSString *)title
                         blockBottom1DidClick:(BlockBottom1DidClick)blockBottom1DidClick
{
    self = [super init];
    [self addSubview:self.btn1];
    [self.btn1 setTitle:title forState:UIControlStateNormal];
    self.blockBottom1DidClick = blockBottom1DidClick;
    
    return self;
}

//设置样式2-紫色边框和文字、白色背景
- (void)setSingleBtnStyle2{

    CGFloat btnW = 150;
    CGFloat btnH = 36;
    self.btn1.frame = CGRectMake((kSCREEN_WIDTH - btnW) * 0.5, (kBottomH - btnH) * 0.5, btnW, btnH);
    self.btn1.layer.cornerRadius = btnH * 0.5;
    self.btn1.layer.borderWidth = 1;
    self.btn1.backgroundColor = KWhite_Color;
    
    self.btn1.layer.borderColor = KMain_Color.CGColor;
    self.btn1.layer.masksToBounds = YES;
    
    self.btn1.titleLabel.font = kFont14;
    [self.btn1 setTitleColor:KMain_Color forState:UIControlStateNormal];
    
    self.backgroundColor = KWhite_Color;
    self.lineView.hidden = YES;
}

- (void)setBottomWithTitle:(NSString *)title
       blockBottom1DidClick:(BlockBottom1DidClick)blockBottom1DidClick{
    
    [self removeAllSubviews];
    
    self.backgroundColor = KBack_Color;
    [self addSubview:self.lineView];
    

    self.btn1.frame = CGRectMake((KSCREEN_W-315)/2, 23/2, 315, 44);

    [self addSubview:self.btn1];
    [self.btn1 setTitle:title forState:UIControlStateNormal];
     _blockBottom1DidClick = blockBottom1DidClick;
}

- (void)setTwoButtonWithTitle0:(NSString *)title0 title1:(NSString *)title1
          blockBottom0DidClick:(BlockBottom0DidClick)blockBottom0DidClick
          blockBottom1DidClick:(BlockBottom1DidClick)blockBottom1DidClick{

    
    [self removeAllSubviews];
    
    self.backgroundColor = KBack_Color;
//    self.backgroundColor = [UIColor yellowColor];
    
    [self addSubview:self.lineView];
    
    self.btn0.backgroundColor = KMain_Color;
    [self.btn0 setTitleColor:KWhite_Color forState:UIControlStateNormal];
    self.btn1.backgroundColor = KMain_Color;
    [self.btn1 setTitleColor:KWhite_Color forState:UIControlStateNormal];
   
    [self.btn0 setTitle:title0 forState:UIControlStateNormal];
    [self.btn1 setTitle:title1 forState:UIControlStateNormal];
    
    CGFloat margin = 8;
    CGFloat btnW = (kSCREEN_WIDTH - margin * 3) * 0.5;
    CGFloat btnH = (kBottomH - margin * 2);
    self.btn0.frame = CGRectMake(margin, margin, btnW, btnH);
    self.btn1.frame =CGRectMake(margin*2 + btnW, margin, btnW, btnH);
    
    [self addSubview:self.btn0];
    [self addSubview:self.btn1];
    
//    NSLog(@"frame :\n %@ \n %@ %@",NSStringFromCGRect(self.frame),NSStringFromCGRect(self.btn0.frame),NSStringFromCGRect(self.btn0.frame));
    
    _blockBottom0DidClick = blockBottom0DidClick;
    _blockBottom1DidClick = blockBottom1DidClick;
}

- (void)setThreeButtonWithTitle0:(NSString *)title0
          blockBottom0DidClick:(BlockBottom0DidClick)blockBottom0DidClick
                        title1:(NSString *)title1
          blockBottom1DidClick:(BlockBottom1DidClick)blockBottom1DidClick
                        title2:(NSString *)title2
          blockBottom2DidClick:(BlockBottom1DidClick)blockBottom2DidClick{
    
    [self removeAllSubviews];
    
    self.backgroundColor = KBack_Color;
    [self addSubview:self.lineView];
    
    [self.btn0 setTitleColor:KWhite_Color forState:UIControlStateNormal];
    self.btn0.backgroundColor = KMain_Color;
    
    [self.btn0 setTitle:title0 forState:UIControlStateNormal];
    [self.btn1 setTitle:title1 forState:UIControlStateNormal];
    [self.btn2 setTitle:title2 forState:UIControlStateNormal];
    
    CGFloat margin = 8;
    CGFloat btnW = (kSCREEN_WIDTH - margin * 4) / 3;
    CGFloat btnH = (kBottomH - margin * 2);
    
    self.btn0.frame = CGRectMake(margin, margin, btnW, btnH);
    self.btn1.frame = CGRectMake(margin*2 + btnW, margin, btnW, btnH);
    self.btn2.frame = CGRectMake(margin*3 + btnW * 2, margin, btnW, btnH);
    
    [self addSubview:self.btn0];
    [self addSubview:self.btn1];
    [self addSubview:self.btn2];
    
//    NSLog(@"Three - %@,%@,%@ ;\n %@",title0,title1,title2,self.subviews);
    
    _blockBottom0DidClick = blockBottom0DidClick;
    _blockBottom1DidClick = blockBottom1DidClick;
    _blockBottom2DidClick = blockBottom2DidClick;
}

- (void)removeAllSubviews{
    NSArray *views = [self subviews];
    for(UIView* view in views)
    {
        [view removeFromSuperview];
    }
}

- (void)setBtnGray{
    self.btn0.backgroundColor = [UIColor lightGrayColor];
    self.btn1.backgroundColor = [UIColor lightGrayColor];
    self.btn2.backgroundColor = [UIColor lightGrayColor];
    
    self.btn0.layer.borderColor =[UIColor lightGrayColor].CGColor;
    self.btn1.layer.borderColor =[UIColor lightGrayColor].CGColor;
    self.btn2.layer.borderColor =[UIColor lightGrayColor].CGColor;
    
    self.btn0.enabled = NO;
    self.btn1.enabled = NO;
    self.btn2.enabled = NO;
    
}

#pragma mark - Getter

-(UIButton *)btn0
{
    if (!_btn0) {
        _btn0 = [[UIButton alloc]init];
        
        _btn0.layer.cornerRadius = 3;
        _btn0.layer.borderColor = KMain_Color.CGColor;
        _btn0.layer.borderWidth = 1;
        _btn0.layer.masksToBounds = YES;
//        _btn0.layer.shouldRasterize = YES;
        _btn0.backgroundColor = KWhite_Color;
        [_btn0 setTitleColor:KMain_Color forState:UIControlStateNormal];
        [_btn0 addTarget:self action:@selector(btn0OnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btn0;
}

-(UIButton *)btn1
{
    if (!_btn1) {
        _btn1 = [[UIButton alloc]init];
        
        _btn1.layer.cornerRadius = 3;
//        _btn1.layer.shouldRasterize = YES;
        _btn1.layer.masksToBounds = YES;
        
        [_btn1 setBackgroundColor:KMain_Color];
        [_btn1 setTitleColor:KWhite_Color forState:UIControlStateNormal];
        
        
        [_btn1 addTarget:self action:@selector(btn1OnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _btn1;
}

-(UIButton *)btn2
{
    if (!_btn2) {
        _btn2 = [[UIButton alloc]init];
        
        _btn2.layer.cornerRadius = 3;
        _btn2.layer.masksToBounds = YES;
//        _btn2.layer.shouldRasterize = YES;
        
        [_btn2 setBackgroundColor:KMain_Color];
        [_btn2 setTitleColor:KWhite_Color forState:UIControlStateNormal];
        [_btn2 addTarget:self action:@selector(btn2OnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _btn2;
}

-(UIView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 1)];
        _lineView.backgroundColor = kColorBorderTri;
       
    }
    return _lineView;
}

#pragma mark - Funcs

- (void)btn0OnClick{
    if (self.blockBottom0DidClick) {
        self.blockBottom0DidClick();
    }
}

- (void)btn1OnClick{
    if (self.blockBottom1DidClick) {
        self.blockBottom1DidClick();
    }
}

- (void)btn2OnClick{
    if (self.blockBottom2DidClick) {
        self.blockBottom2DidClick();
    }
}

#pragma mark - Blocks

-(void)setBlockBottom0DidClick:(BlockBottom0DidClick)blockBottom0DidClick
{
    _blockBottom0DidClick = blockBottom0DidClick;
}

-(void)setBlockBottom1DidClick:(BlockBottom1DidClick)blockBottom1DidClick
{
    self.blockBottom1DidClick = blockBottom1DidClick;
}

-(void)setBlockBottom2DidClick:(BlockBottom1DidClick)blockBottom2DidClick
{
    self.blockBottom2DidClick = blockBottom2DidClick;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
