//
//  NoDataView.m
//  kyExpress_Internal
//
//  Created by wangkai on 16/6/22.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "NoDataView.h"

@implementation NoDataView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setUpView];
    }
    return self;
}

- (void)setUpView{
    self.backgroundColor = [UIColor clearColor];
    
    //        NSLog(@"NoDataView.frame : %@", NSStringFromCGRect(frame));
    
    CGFloat imgW = 50;
    CGFloat imgX = (self.frame.size.width - imgW) * 0.5;
    
    UIImageView *imageV = [[UIImageView alloc]initWithFrame:CGRectMake(imgX, 40, imgW, 50)];
    [imageV setImage:IMAGE(@"noData")];
    [self addSubview:imageV];
    
    UILabel *labelTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 110, self.frame.size.width, 20)];
    labelTitle.backgroundColor = [UIColor clearColor];
    labelTitle.textColor = [UIColor grayColor];
    labelTitle.textAlignment = NSTextAlignmentCenter;
    labelTitle.font = [UIFont systemFontOfSize:14];
    labelTitle.text = @"暂无相关数据";
    
    if (self.index==1) {
        labelTitle.text= @"暂无空闲司机";
    }
    self.labelTitle = labelTitle;
    [self addSubview:labelTitle];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
