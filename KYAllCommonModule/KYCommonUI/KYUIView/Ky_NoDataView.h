//
//  Ky_NoDataView.h
//  kyExpress_Internal
//
//  Created by alangavin on 16/9/9.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Ky_NoDataView : UIView
/**
 *  显示的文字
 */
@property (nonatomic,strong)  UILabel *labelTitle;

@end
