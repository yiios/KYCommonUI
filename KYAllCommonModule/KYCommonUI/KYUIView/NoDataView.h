//
//  NoDataView.h
//  kyExpress_Internal
//
//  Created by wangkai on 16/6/22.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoDataView : UIView

@property (nonatomic,strong)  UILabel *labelTitle;

@property (nonatomic, assign) int index;//1.市场约车行政端

@end
