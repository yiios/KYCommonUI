//
//  BottomView.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/8/30.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BlockBottom0DidClick)(void);
typedef void(^BlockBottom1DidClick)(void);
typedef void(^BlockBottom2DidClick)(void);

@interface BottomView : UIView

@property (nonatomic,strong) UIButton *btn0;//
@property (nonatomic,strong) UIButton *btn1;//
@property (nonatomic,strong) UIButton *btn2;//

@property (nonatomic,strong) UIView *lineView;

@property (nonatomic, copy) BlockBottom0DidClick blockBottom0DidClick;
@property (nonatomic, copy) BlockBottom1DidClick blockBottom1DidClick;
@property (nonatomic, copy) BlockBottom2DidClick blockBottom2DidClick;



- (void)setBottomWithTitle:(NSString *)title
       blockBottom1DidClick:(BlockBottom1DidClick)blockBottom1DidClick;

- (void)setTwoButtonWithTitle0:(NSString *)title0 title1:(NSString *)title1
          blockBottom0DidClick:(BlockBottom0DidClick)blockBottom0DidClick
          blockBottom1DidClick:(BlockBottom1DidClick)blockBottom1DidClick;

- (void)setThreeButtonWithTitle0:(NSString *)title0
          blockBottom0DidClick:(BlockBottom0DidClick)blockBottom0DidClick
                        title1:(NSString *)title1
          blockBottom1DidClick:(BlockBottom1DidClick)blockBottom1DidClick
                        title2:(NSString *)title2
          blockBottom2DidClick:(BlockBottom1DidClick)blockBottom2DidClick;

- (void)setBtnGray;

//设置样式2
- (void)setSingleBtnStyle2;

@end
