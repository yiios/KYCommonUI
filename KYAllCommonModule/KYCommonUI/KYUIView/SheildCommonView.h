//
//  SheildCommonView.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/9/9.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>


#define kFeedBackStr @"请输入您的反馈意见"

typedef void(^BlockSheildEnsureOnClick)(NSString * string);

typedef void(^HideBlock)(void);

@interface SheildCommonView : UIView

@property (nonatomic,strong) UIView *bgView;
@property (nonatomic,strong) UILabel *myTitleLabel;
@property (nonatomic,strong) UILabel *detailLabel;

@property (nonatomic,strong) UILabel *countLabel;
@property (nonatomic,strong) UIImageView *imgView;

@property (nonatomic,strong) UITextView *detailTxView;

@property (nonatomic,strong) UITextView *inputTxView;

@property (nonatomic,strong) UIView *lineView;
@property (nonatomic,strong) UIView *lineVertiView;

@property (nonatomic,strong) UIButton *cancelButton;
@property (nonatomic,strong) UIButton *ensureButton;

@property (nonatomic,copy) BlockSheildEnsureOnClick blockSheildEnsureOnClick;

@property(nonatomic,copy)HideBlock hideblock;

- (void)resetInpuViewWithPlaceHolder:(NSString *)placeHolder;

-(void)setBlockSheildEnsureOnClick:(BlockSheildEnsureOnClick)blockSheildEnsureOnClick;
-(void)setHideblock:(HideBlock)hideblock;

- (void)refreshUIForKPIO;

- (instancetype)initWithFrameKPIO:(CGRect)frame;

#pragma mark - 操作绩效查询弹窗
- (instancetype)initWithFrameKPIOSearch:(CGRect)frame;


- (instancetype)initForKPIOFeedBackWithFrame:(CGRect)frame;

#pragma mark - 新增预约弹窗
- (instancetype)initWithFrameVisitAdd:(CGRect)frame;

#pragma mark - 使用label显示
- (instancetype)initLabelSheildWithFrame:(CGRect)frame;
- (void)refreshTitle:(NSString *)title detailLabelText:(NSString *)detail btnTitle:(NSString *)btnTitle;

@end
