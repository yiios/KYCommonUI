//
//  GrayTableHeader.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/9/10.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BlockGrayHeaderLabel3OnClick)(void);

@interface GrayTableHeader : UIView
@property (nonatomic,strong) UILabel *label1;
@property (nonatomic,strong) UILabel *label2;
@property (nonatomic,strong) UILabel *label3;
@property (nonatomic,strong) UILabel *label4;

@property (nonatomic,copy) BlockGrayHeaderLabel3OnClick blockGrayHeaderLabel3OnClick;

-(void)setBlockGrayHeaderLabel3OnClick:(BlockGrayHeaderLabel3OnClick)blockGrayHeaderLabel3OnClick;

- (void)refreshUIForKPIOWithTitles:(NSArray *)titles;

//通知列表
- (void)refreshUIForNoticeListWithTitles:(NSArray *)titles;

//通知详情-评论
- (void)refreshUIForNoticeCommentWithUnreadNum:(NSString *)unreadNum;

//绩效详情
- (void)refreshUIForKPIODetailWithTitles:(NSArray *)titles;
//绩效详情2
- (void)refreshUIForKPIODetailTwoWithTitles:(NSArray *)titles;
//绩效详情-打包件数
- (void)refreshUIForKPIO3DetailWithTitles:(NSArray *)titles;

//绩效详情- 本月绩效
- (void)refreshUIForKPIO3WithTitles:(NSArray *)titles;

// 招标评审
- (void)refreshUIForTenderReviewWithTitles:(NSArray *)titles;

@end
