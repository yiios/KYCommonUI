//
//  SheildAlertView.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/8/4.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BlockSheildAlertCertainBlock)(void);
typedef void(^CanceBlock)(void);

@interface SheildAlertView : UIView


@property (weak, nonatomic) IBOutlet UILabel *messageLabel;//标题-温馨提示
@property (weak, nonatomic) IBOutlet UIButton *certainBtn;

@property (nonatomic, copy) CanceBlock canceBlock;
@property (nonatomic, copy) BlockSheildAlertCertainBlock blockSheildAlertCertainBlock;


- (instancetype)initSheild;

-(void)setMessage:(NSString *)string;

-(void)setBlockSheildAlertCertainBlock:(BlockSheildAlertCertainBlock)blockSheildAlertCertainBlock;

- (void)setSheildWithTitle:(NSString *)title message:(NSString *)message CertainBlock:(BlockSheildAlertCertainBlock)CertainBlock  CanceBlock:(CanceBlock)CanceBlock;

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message CertainBlock:(BlockSheildAlertCertainBlock)CertainBlock  CanceBlock:(CanceBlock)CanceBlock;

@end
