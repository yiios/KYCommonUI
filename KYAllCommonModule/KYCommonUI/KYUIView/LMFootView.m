//
//  LMFootView.m
//  kyExpress_Internal
//
//  Created by limo on 2017/3/7.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

// 卡住了
#import "LMFootView.h"
#import "UIImage+Common.h"

@interface LMFootView()

/** footView 底部的高 */
@property (nonatomic, assign) CGFloat footViewHeight;

@property (nonatomic, strong) UIView *topDividerView;

@end

@implementation LMFootView
-(instancetype)initWithController:(UIViewController<LMFootViewButtonDelegate> *)viewController andFootViewHeight:(CGFloat)footViewHeight andTitleStrArr:(NSArray<NSString *> *)titleStrArr{
    
    CGFloat footViewY = CGRectGetMaxY(viewController.view.frame)- kStatusBarAddNavigationBarHeight - footViewHeight;
//    kStatusbarHeight
    CGRect frame = CGRectMake(0, footViewY-kSafeAreaInsetsBottom, kSCREEN_WIDTH, footViewHeight+kSafeAreaInsetsBottom);

    if ([super initWithFrame:frame]) {

        self.footViewHeight = footViewHeight;
        
        [self addSubview:self.topDividerView];
        
        [self setTopDividerHide:YES];
        
        [self setFooterViewWithTitles:titleStrArr];
        
        self.delegate = viewController;
    }
    
    return self;
}

-(void)setFooterViewWithTitles:(NSArray<NSString *> *)titles {
    
    [self.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj != self.topDividerView) {
            [obj removeFromSuperview];
        }
    }];
    
    if (titles.count < 1) {
        return;
    }
    
    float width = (self.frame.size.width - 60.f - ((titles.count - 1) * 15.f))/titles.count;

//    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 0.5)];
//    lineView.backgroundColor = [UIColor clearColor];
//    [self addSubview:lineView];

    
    for (NSInteger i = 0; i<titles.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake((width+15.f)*i + 30.f, (self.footViewHeight-44.f)/2, width, 44.f);
        button.layer.cornerRadius = 5.0f;
        button.clipsToBounds = YES;

        [button setTitle:titles[i] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        button.titleLabel.font = [UIFont systemFontOfSize:17];
        button.tag = i+1;
        [button setBackgroundColor:KMain_Color];
        [button setBackgroundImage:[UIImage imageWithColor:KMain_Color] forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageWithColor:KBlackGray] forState:UIControlStateDisabled];
        [button addTarget:self action:@selector(buttonEventTouch:) forControlEvents:UIControlEventTouchUpInside];
        [self.buttonArr addObject:button];
        [self addSubview:button];
    }
}

- (void)setTopDividerHide:(BOOL)isHide {
    self.topDividerView.hidden = isHide;
    
    if (isHide == YES) {
        // 如果不显示顶部分隔线 应该是在tableView 的 footView 上 背景颜色即为 eeeeee
        self.backgroundColor = [UIColor colorWithHexString:@"0xeeeeee"];
    } else {
        // 如果显示顶部分隔线 应该是在 self.view 底部 背景颜色为 fafafa
        self.backgroundColor = [UIColor colorWithHexString:@"0xfafafa"];
    }
    
}


-(void)buttonEventTouch:(UIButton *)sender {
    if ([_delegate respondsToSelector:@selector(footViewDidSelectedButton:andIndex:andButtonTitle:)]) {
        [_delegate footViewDidSelectedButton:sender andIndex:sender.tag-1 andButtonTitle:sender.titleLabel.text];
    }
}

- (NSMutableArray<UIButton *> *)buttonArr {
    if (_buttonArr == nil) {
        _buttonArr = [NSMutableArray array];
    }
    return _buttonArr;
}

- (UIView *)topDividerView {
    if (_topDividerView == nil) {
        _topDividerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, 0.5)];
        _topDividerView.backgroundColor = [UIColor colorWithHexString:@"0xd6d6d6"];
    }
    return _topDividerView;
}

@end
