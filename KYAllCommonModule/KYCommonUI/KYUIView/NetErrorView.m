//
//  NetErrorView.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/10/10.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "NetErrorView.h"
#import "UIView+MJExtension.h"

@interface NetErrorView()

@property (nonatomic,strong) UILabel *errorLabel;

@end

@implementation NetErrorView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
      
        [self setUpView];
        
    }
    return self;
}

- (void)setUpView{
    self.backgroundColor = [UIColor clearColor];
    
    UILabel *label0 = [[UILabel alloc]initWithFrame:CGRectMake(15, 15, kSCREEN_WIDTH - 30, 21)];
    label0.text = @"找不到网页";
    label0.font = [UIFont boldSystemFontOfSize:24];
    
    CGFloat errorY =CGRectGetMaxY(label0.frame) + 5;
    CGFloat errorH = self.mj_h - errorY;
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(15, errorY, kSCREEN_WIDTH - 30, errorH)];
    label1.backgroundColor = [UIColor clearColor];
    label1.textColor = kColorTextBlack;
    label1.textAlignment = NSTextAlignmentCenter;
    label1.font = [UIFont systemFontOfSize:14];
    label1.text = @"暂无相关数据";
    label1.numberOfLines = 0;
    
    self.errorLabel = label1;
    
    [self addSubview:label0];
    [self addSubview:label1];
}


- (void)refreshUIWithErrorURLStr:(NSString *)errorURLStr{

    NSString *wholeStr = [NSString stringWithFormat:@"无法载入 ： %@",errorURLStr];
//    NSString *backStr = @" 无法载入";
    
    NSRange wholeRange = [wholeStr rangeOfString:wholeStr];
//    NSRange urlRange  = [wholeStr rangeOfString:errorURLStr]; //网址的range

    NSMutableAttributedString *resultStr = [[NSMutableAttributedString alloc]initWithString:wholeStr];
    
    NSMutableDictionary *contentAttriDict = (NSMutableDictionary *)[StringTools getContentAttributeDict];
    NSMutableDictionary *urlAttriDict = (NSMutableDictionary *)[StringTools getURLAttributeDict];
    
    
//    NSMutableAttributedString *urlAttriStr = [[NSMutableAttributedString alloc]initWithString:errorURLStr attributes:urlAttriDict];
//    NSMutableAttributedString *backAttriStr = [[NSMutableAttributedString alloc]initWithString:backStr attributes:contentAttriDict];
//    [urlAttriStr appendAttributedString:backAttriStr];
//    NSLog(@"urlAttriStr : %@",urlAttriStr);
    
    [resultStr addAttributes:contentAttriDict range:wholeRange];
//    [resultStr addAttributes:urlAttriDict range:urlRange];

    self.errorLabel.attributedText = resultStr;
//    self.errorLabel.copyingEnabled = YES;
    
    [self.errorLabel sizeToFit];
//    self.errorLabel.backgroundColor = [UIColor redColor];
    
    CGFloat errorH = [StringTools getHeightForStr:resultStr.string attriDict:urlAttriDict width:kSCREEN_WIDTH - 30] + 5;
    
    self.errorLabel.frame = CGRectMake(15, 46, kSCREEN_WIDTH - 30, errorH);
    self.errorHeight = 46 + 15 + errorH;
   
    NSLog(@"errorH : %f , errorHeight : %f",errorH,self.errorHeight);
    
}




@end
