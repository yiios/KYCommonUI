//
//  SheildCommonView.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/9/9.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "SheildCommonView.h"
#import <Masonry.h>
#import "UIView+MJExtension.h"
#import <CoreText/CTStringAttributes.h>


@interface SheildCommonView ()<UITextViewDelegate>

@end

@implementation SheildCommonView


-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.5];
    [self initView];
    self.detailTxView.tag = 1;
    return self;
}

- (instancetype)initWithFrameKPIO:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.5];
        [self initKPIOView];
    }
    return self;
}



#pragma mark - 使用label显示
- (instancetype)initLabelSheildWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.5];
        [self setupLabelView];
    }
    return self;
}

//
- (void)setupLabelView{
    
    CGFloat bgW = 260;
    CGFloat bgX = (self.mj_w - bgW)*0.5;
    CGFloat bgH = 180;
    CGFloat bgY = (self.mj_h - 80 - bgH)*0.5;
    self.bgView.frame = CGRectMake(bgX, bgY, bgW, bgH);
    self.bgView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.bgView.layer.shadowOffset =CGSizeMake(14, 14);
    self.bgView.layer.shadowOpacity = 0.8;
    self.bgView.layer.shadowRadius = 2.0;
    [self addSubview:_bgView];
    
    
    self.myTitleLabel = [[UILabel alloc] init];
    self.myTitleLabel.textAlignment = NSTextAlignmentCenter;
    self.myTitleLabel.frame = CGRectMake(0, 0, bgW, 50);
    self.myTitleLabel.text = @"温馨提示";
    self.myTitleLabel.font = [UIFont systemFontOfSize:20];
    
    
    NSLog(@"w : %f , h : %f",self.mj_w,self.mj_h);
    CGFloat btnH = 30;
    CGFloat detailH = bgH - btnH - self.myTitleLabel.mj_h - 12 - 20;
    
    //    self.detailLabel.backgroundColor = [UIColor cyanColor];
    
    self.detailLabel.frame = CGRectMake(15, self.myTitleLabel.mj_h + 3, bgW - 30, detailH);
    self.detailLabel.textAlignment = NSTextAlignmentCenter;
    self.detailLabel.font = kFont16;
    self.detailLabel.numberOfLines = 0;
    
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(self.detailLabel.frame)+5, bgW - 20, 1)];
    line.backgroundColor = [UIColor lightGrayColor];
    
    CGFloat btnY = (self.bgView.mj_h - CGRectGetMaxY(line.frame) - btnH) * 0.5 + CGRectGetMaxY(line.frame);
    
    self.ensureButton.frame = CGRectMake(bgW * 0.7 / 2, btnY, bgW * 0.3, btnH);
    self.ensureButton.backgroundColor = [UIColor whiteColor];
    self.ensureButton.layer.borderWidth = 1;
    self.ensureButton.layer.borderColor = KMain_Color.CGColor;
    self.ensureButton.layer.cornerRadius = btnH * 0.5;
    
    
    [_bgView addSubview:self.myTitleLabel];
    [_bgView addSubview:line];
    [_bgView addSubview:_ensureButton];
    [_bgView addSubview:_detailLabel];
}


#pragma mark - 新增预约弹窗
- (instancetype)initWithFrameVisitAdd:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.5];
        [self setUpVisitAdd];
    }
    return self;
}

- (void)setUpVisitAdd
{
    [self addSubview:self.bgView];
    
    self.bgView.backgroundColor = KWhite_Color;
    self.bgView.layer.cornerRadius = 5;
    self.bgView.layer.masksToBounds = YES;
    
    //阴影
    self.bgView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.bgView.layer.shadowOffset = CGSizeMake(5, 5);
    self.bgView.layer.shadowOpacity = 0.5;
    
    
    self.myTitleLabel.text = @"预约须知";
    self.myTitleLabel.font = kFont18;
    
    self.imgView.image = IMAGE(@"yyxz");
    
    [self.bgView addSubview:self.myTitleLabel];
    [self.bgView addSubview:self.imgView];
    [self.bgView addSubview:self.ensureButton];
    [self.bgView addSubview:self.lineView];
    
    self.myTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    [self.myTitleLabel makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.bgView);
        make.top.mas_equalTo(self.bgView).with.mas_offset(20);
        make.width.mas_equalTo(self.bgView);
        make.height.mas_equalTo(17);
    }];
    
    [self.ensureButton makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.bgView);
        make.bottom.mas_equalTo(self.bgView.mas_bottom);
        make.width.mas_equalTo(self.bgView);
        make.height.mas_equalTo(44);
    }];
    
    
    [self.lineView makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(1);
        make.left.mas_equalTo(self.bgView);
        make.width.mas_equalTo(self.bgView);
        make.bottom.mas_equalTo(self.ensureButton.mas_top);
    }];
    
    [self.imgView makeConstraints:^(MASConstraintMaker *make) {
        //        make.leading.mas_equalTo(self.bgView).with.mas_offset(10);
        //        make.trailing.mas_equalTo(self.bgView).with.mas_offset(-10);
        //make.width.mas_equalTo(self.bgView.mj_w - 20);
        //        make.bottom.mas_equalTo(self.lineView.mas_top);
        
        make.top.mas_equalTo(self.myTitleLabel.mas_bottom).with.mas_offset(2);
        make.size.mas_equalTo(CGSizeMake(250, 230));
        make.centerX.mas_equalTo(self.bgView);
        
    }];
    
    
    
    CGFloat bgH = 40 + 17 + 22 + 226;
    
    [self.bgView makeConstraints:^(MASConstraintMaker *make) {
        //        make.top.mas_equalTo(80);
        //        make.centerX.mas_equalTo(self);
        make.center.mas_equalTo(self);
        //        make.width.mas_equalTo(kSCREEN_WIDTH - 70);
        make.width.mas_equalTo(250);
        make.height.mas_equalTo(bgH);
        
    }];
    
    [self.ensureButton setTitle:@"返回" forState:UIControlStateNormal];
    [self.ensureButton setTitleColor:KMain_Color forState:UIControlStateNormal];
    [self.ensureButton addTarget:self action:@selector(remove) forControlEvents:UIControlEventTouchUpInside];
}

- (CGFloat)getHeightForTxViewWithStr:(NSAttributedString *)attriStr width:(CGFloat)width maxHeight:(CGFloat)maxHeight{
    
    
    UITextView *txView = [[UITextView alloc]initWithFrame:CGRectMake(0, 0, width, 100)];
    txView.attributedText = attriStr;
    
    CGRect frame = txView.frame;
    CGSize constraintSize = CGSizeMake(frame.size.width, MAXFLOAT);
    CGSize size = [txView sizeThatFits:constraintSize];
    
    return size.height + 18 < maxHeight ?size.height + 18:maxHeight;
}

//行高10，字间距1
- (NSDictionary *)getContentAttributeDict{
    
    CGFloat lineSpace = 10.f;
    CGFloat wordSpace = 1;
    
    CGFloat fontSize = 16;
    
    if (IS_IPHONE_5) {
        fontSize = 14;
    }else{
        fontSize = 16;
    }
    
    
    //行间距
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineSpacing = lineSpace;
    //字间距
    long number = wordSpace;
    CFNumberRef num = CFNumberCreate(kCFAllocatorDefault,kCFNumberSInt8Type,&number);
    
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    [attributes setObject:[UIFont systemFontOfSize:fontSize] forKey:NSFontAttributeName];
    [attributes setObject:paragraphStyle forKey:NSParagraphStyleAttributeName];  //行距
    [attributes setObject:(__bridge id)num forKey:(id)kCTKernAttributeName];//字间距
    [attributes setObject:[UIColor colorWithHexString:@"333333"]  forKey:NSForegroundColorAttributeName];
    
    return attributes;
}

- (void)remove{
    //    [self removeFromSuperview];
    self.hidden = YES;
}

- (void)refreshTitle:(NSString *)title detailLabelText:(NSString *)detail btnTitle:(NSString *)btnTitle{
    
    self.myTitleLabel.text = title;
    self.detailLabel.text = detail;
    [self.ensureButton setTitle:btnTitle forState:UIControlStateNormal];
}

#pragma mark - 操作绩效查询弹窗
- (instancetype)initWithFrameKPIOSearch:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.5];
        [self setupKPIOSearchAlert];
    }
    return self;
}

//
- (void)setupKPIOSearchAlert{
    
    CGFloat bgW = 260;
    CGFloat bgX = (self.mj_w - bgW)*0.5;
    CGFloat bgH = 180;
    CGFloat bgY = (self.mj_h - bgH)*0.5;
    self.bgView.frame = CGRectMake(bgX, bgY, bgW, bgH);
    [self addSubview:_bgView];
    
    self.myTitleLabel = [[UILabel alloc] init];
    self.myTitleLabel.textAlignment = NSTextAlignmentCenter;
    self.myTitleLabel.frame = CGRectMake(0, 0, bgW, 40);
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(10, self.myTitleLabel.mj_h, bgW - 20, 1)];
    line.backgroundColor = [UIColor lightGrayColor];
    
    
    NSLog(@"w : %f , h : %f",self.mj_w,self.mj_h);
    CGFloat btnH = 50;
    CGFloat detailH = bgH - btnH - self.myTitleLabel.mj_h;
    
    //    self.detailLabel.backgroundColor = [UIColor cyanColor];
    
    self.detailLabel.frame = CGRectMake(7, self.myTitleLabel.mj_h + 5, bgW - 15, detailH);
    self.detailLabel.textAlignment = NSTextAlignmentCenter;
    self.detailLabel.font = kFont16;
    
    
    self.ensureButton.frame = CGRectMake(bgW * 0.7 / 2, self.detailLabel.mj_h + self.detailLabel.mj_y + 10, bgW * 0.3, btnH - 20);
    self.ensureButton.backgroundColor = [UIColor whiteColor];
    self.ensureButton.layer.borderWidth = 1;
    self.ensureButton.layer.borderColor = KMain_Color.CGColor;
    self.ensureButton.layer.cornerRadius = (btnH -20) * 0.5;
    
    
    [_bgView addSubview:self.myTitleLabel];
    [_bgView addSubview:line];
    [_bgView addSubview:_ensureButton];
    [_bgView addSubview:_detailLabel];
}

#pragma mark

- (void)initView{
    [self addAlert];
}

- (void)initKPIOView
{
    [self addKPIOAlert];
}

- (void)addAlert{
    
    CGFloat bgW = 260;
    CGFloat bgX = (self.mj_w - bgW)*0.5;
    CGFloat bgH = 180;
    CGFloat bgY = (self.mj_h - 80 - bgH)*0.5;
    self.bgView.frame = CGRectMake(bgX, bgY, bgW, bgH);
    [self addSubview:_bgView];
    
    CGFloat btnH = 50;
    self.ensureButton.frame = CGRectMake(0, bgH - btnH, bgW, btnH);
    self.lineView.frame = CGRectMake(0, bgH - btnH - 1, bgW, 1);
    
    CGFloat detailH = bgH - btnH - 1 - 5 *2;
    
    self.detailTxView.frame = CGRectMake(7, 5, bgW - 15, detailH);
    
    [_bgView addSubview:_ensureButton];
    [_bgView addSubview:_lineView];
    [_bgView addSubview:_detailTxView];
    
}

- (void)addKPIOAlert
{
    CGFloat bgW = 260;
    CGFloat bgX = (self.mj_w - bgW)*0.5;
    CGFloat bgH = 180 + 60;
    CGFloat bgY = (self.mj_h - bgH)*0.5;
    self.bgView.frame = CGRectMake(bgX, bgY, bgW, bgH);
    //     self.bgView.frame = CGRectMake(100, 100, 300, 300);
    [self addSubview:_bgView];
    
    self.myTitleLabel = [[UILabel alloc] init];
    self.myTitleLabel.textAlignment = NSTextAlignmentCenter;
    self.myTitleLabel.frame = CGRectMake(0, 0, bgW, 40);
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(10, self.myTitleLabel.mj_h, bgW - 20, 1)];
    line.backgroundColor = [UIColor lightGrayColor];
    
    [_bgView addSubview:self.myTitleLabel];
    [_bgView addSubview:line];
    
    NSLog(@"w : %f , h : %f",self.mj_w,self.mj_h);
    CGFloat btnH = 50;
    CGFloat detailH = bgH - btnH - self.myTitleLabel.mj_h;
    self.detailTxView.frame = CGRectMake(7, self.myTitleLabel.mj_h + 5, bgW - 15, detailH);
    
    
    self.ensureButton.frame = CGRectMake(bgW * 0.7 / 2, self.detailTxView.mj_h + self.detailTxView.mj_y + 10, bgW * 0.3, btnH - 20);
    self.ensureButton.backgroundColor = [UIColor whiteColor];
    self.ensureButton.layer.borderWidth = 1;
    self.ensureButton.layer.borderColor = KMain_Color.CGColor;
    self.ensureButton.layer.cornerRadius = (btnH -20) * 0.5;
    
    //    self.detailTxView.backgroundColor = [UIColor cyanColor];
    
    [_bgView addSubview:_ensureButton];
    [_bgView addSubview:_detailTxView];
    
}

- (void)hide{
    self.hidden = YES;
    [self.superview endEditing:YES];
    
    if(_hideblock)
    {
        _hideblock();
    }
}


-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    NSLog(@"tag : %d",textView.tag);
    
    if (textView.tag == 1) {
        return NO;
    }
    return YES;
}

#pragma mark - 操作绩效反馈

- (instancetype)initForKPIOFeedBackWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.5];
        
        [self setUpKPIOFeedBack];
        
    }
    return self;
}

- (void)setUpKPIOFeedBack{
    
    CGFloat bgX = 30;
    CGFloat bgW = self.mj_w - bgX * 2 ;
    CGFloat bgH = 260;
    CGFloat bgY = 30;
    self.bgView.frame = CGRectMake(bgX, bgY, bgW, bgH);
    [self addSubview:self.bgView];
    
    NSLog(@"w : %f , h : %f",self.mj_w,self.mj_h);
    CGFloat btnH = 50;
    
    self.myTitleLabel.frame = CGRectMake(0, 0, bgW - 20, 50);
    self.myTitleLabel.text = @"填写反馈意见";
    
    CGFloat countW = 100;
    self.countLabel.frame = CGRectMake(bgW - countW - kMargin, 0, countW, self.myTitleLabel.mj_h);
    
    CGFloat inputY = CGRectGetMaxY(self.myTitleLabel.frame) + 2;
    CGFloat inputH = bgH - inputY - btnH - kMargin ;
    self.inputTxView.frame = CGRectMake(kMargin,inputY, bgW - kMargin * 2, inputH);
    self.inputTxView.text = kFeedBackStr;
    self.inputTxView.textColor = kColorTextGray;
    self.inputTxView.tag = 2;
    
    self.lineView.frame = CGRectMake(0, bgH - btnH - 1, bgW, 1);
    self.lineVertiView.frame = CGRectMake(bgW * 0.5, CGRectGetMaxY(self.lineView.frame), 1, btnH);
    
    
    self.cancelButton.frame = CGRectMake(0, bgH - btnH, bgW * 0.5, btnH);
    self.ensureButton.frame = CGRectMake(bgW * 0.5, bgH - btnH, bgW * 0.5, btnH);
    
    [self.ensureButton setTitle:@"确定" forState:UIControlStateNormal];
    
    
    [self.bgView addSubview:self.myTitleLabel];
    [self.bgView addSubview:self.countLabel];
    [self.bgView addSubview:self.inputTxView];
    
    [self.bgView addSubview:self.cancelButton];
    [self.bgView addSubview:self.ensureButton];
    
    [self.bgView addSubview:self.lineView];
    [self.bgView addSubview:self.lineVertiView];
}

- (void)resetInpuViewWithPlaceHolder:(NSString *)placeHolder{
    
    self.inputTxView.text = placeHolder;
    self.inputTxView.textColor = kColorTextGray;
    self.countLabel.text = [NSString stringWithFormat:@"0/500字"];
}


#pragma mark - UITextViewDelegate


- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    if ([textView.text isEqualToString:kFeedBackStr]) {
        textView.text = @"";
        textView.textColor = kColorTextBlack;
    }
}

-(void)textViewDidChange:(UITextView *)textView
{
    
    if ([textView.text isEqualToString:kFeedBackStr]) {
        self.countLabel.text = [NSString stringWithFormat:@"0/500字"];
    }else{
        self.countLabel.text = [NSString stringWithFormat:@"%lu/500字",(unsigned long)textView.text.length];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if (textView.text.length<1) {
        textView.text = kFeedBackStr;
        textView.textColor = kColorTextGray;
    }
}

#pragma mark -


- (void)ensureBtnOnClick{
    
    [self.inputTxView endEditing:YES];
    
    if (_blockSheildEnsureOnClick) {
        
        //        NSString *inputStr = self.inputTxView.text;
        
        _blockSheildEnsureOnClick(self.inputTxView.text);
        
        
    }else{
        
        [self hide];
    }
    
    
    
}


#pragma mark -
-(void)setHideblock:(HideBlock)hideblock
{
    _hideblock = hideblock;
}


-(void)setBlockSheildEnsureOnClick:(BlockSheildEnsureOnClick)blockSheildEnsureOnClick
{
    _blockSheildEnsureOnClick = blockSheildEnsureOnClick;
}

-(UITextView *)inputTxView
{
    if (!_inputTxView) {
        _inputTxView = [[UITextView alloc]init];
        _inputTxView.backgroundColor = KBack_Color;
        _inputTxView.delegate = self;
        _inputTxView.layer.cornerRadius = 3;
        _inputTxView.layer.masksToBounds = YES;
        _inputTxView.layer.borderColor = kColorBorderTri.CGColor;
        _inputTxView.layer.borderWidth = 1;
    }
    return _inputTxView;
}



-(UITextView *)detailTxView
{
    if (!_detailTxView) {
        _detailTxView = [[UITextView alloc]init];
        _detailTxView.textColor = kColorTextBlack;
        _detailTxView.font = kFont14;
        _detailTxView.delegate = self;
        _detailTxView.editable = YES;
    }
    
    return _detailTxView;
}


-(UIButton *)ensureButton
{
    if (!_ensureButton) {
        _ensureButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_ensureButton setTitle:@"返回" forState:UIControlStateNormal];
        [_ensureButton setTitleColor:KMain_Color forState:UIControlStateNormal];
        [_ensureButton addTarget:self action:@selector(ensureBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _ensureButton;
}

-(UIButton *)cancelButton
{
    if (!_cancelButton) {
        _cancelButton = [[UIButton alloc]init];
        [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        [_cancelButton setTitleColor:KMain_Color forState:UIControlStateNormal];
        [_cancelButton addTarget:self action:@selector(hide) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _cancelButton;
}

-(UILabel *)myTitleLabel
{
    if (!_myTitleLabel) {
        _myTitleLabel = [[UILabel alloc]init];
        _myTitleLabel.font = kFont16;
        _myTitleLabel.textAlignment = NSTextAlignmentCenter;
        
    }
    return _myTitleLabel;
}

-(UILabel *)detailLabel
{
    if (!_detailLabel) {
        _detailLabel = [[UILabel alloc]init];
        _detailLabel.textColor = kColorTextBlack;
    }
    return _detailLabel;
}

-(UILabel *)countLabel
{
    if (!_countLabel) {
        _countLabel = [[UILabel alloc]init];
        _countLabel.textColor = [UIColor orangeColor];
        _countLabel.textAlignment = NSTextAlignmentRight;
        _countLabel.text = [NSString stringWithFormat:@"0/500字"];
        _countLabel.font = kFont14;
    }
    return _countLabel;
}

-(UIView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = kColorBorderTri;
    }
    return _lineView;
}

-(UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = KWhite_Color;
        _bgView.layer.cornerRadius = 3;
        _bgView.layer.masksToBounds = YES;
    }
    return _bgView;
}

-(UIView *)lineVertiView
{
    if (!_lineVertiView) {
        _lineVertiView = [[UIView alloc]init];
        _lineVertiView.backgroundColor = kColorBorderTri;
    }
    return _lineVertiView;
}

-(UIImageView *)imgView
{
    if (!_imgView) {
        _imgView = [[UIImageView alloc]init];
    }
    return _imgView;
}


- (void)drawRect:(CGRect)rect {
    // Drawing code
    //    [super drawRect:rect];
    //    [self initView];
}

@end
