//
//  Ky_NoDataView.m
//  kyExpress_Internal
//
//  Created by alangavin on 16/9/9.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "Ky_NoDataView.h"

@implementation Ky_NoDataView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        
        NSLog(@"=========%f",CGRectGetMaxY(frame)/2-20);
        
        CGFloat imgW = 50;
        CGFloat imgX = (frame.size.width - imgW) * 0.5;
        
        UIImageView *imageV = [[UIImageView alloc]initWithFrame:CGRectMake(imgX, CGRectGetMaxY(frame)/4-20, imgW, 50)];
//        imageV.center = self.center;
        [imageV setImage:IMAGE(@"noData")];
        [self addSubview:imageV];
        
        UILabel *labelTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(imageV.frame), frame.size.width, 20)];
        labelTitle.backgroundColor = [UIColor clearColor];
        labelTitle.textColor = [UIColor grayColor];
        labelTitle.textAlignment = NSTextAlignmentCenter;
        labelTitle.font = [UIFont systemFontOfSize:14];
        labelTitle.text = @"暂无相关数据";
        self.labelTitle = labelTitle;
        [self addSubview:labelTitle];
    }
    return self;
}


@end
