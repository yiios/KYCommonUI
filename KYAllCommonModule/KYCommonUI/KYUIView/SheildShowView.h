//
//  SheildShowView.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/8/29.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BlockSheildShowCertain)(void);

@interface SheildShowView : UIView
/**
 *  背景View
 */
@property (weak, nonatomic) IBOutlet UIView *bgView;
/**
 *  弹窗抬头(标题)
 */
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
/**
 *  描述(中间文字)
 */
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
/**
 *  确定按钮
 */
@property (weak, nonatomic) IBOutlet UIButton *ensureBtn;

@property (nonatomic, copy) BlockSheildShowCertain certainBlock;

- (instancetype)initSheild;
- (void)setSheildWithTitle:(NSString *)title message:(NSString *)message CertainBlock:(BlockSheildShowCertain)certainBlock;

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message CertainBlock:(BlockSheildShowCertain)CertainBlock;

@end
