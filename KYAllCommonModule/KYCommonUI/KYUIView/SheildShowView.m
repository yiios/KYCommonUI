//
//  SheildShowView.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/8/29.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "SheildShowView.h"

@implementation SheildShowView
-(void)awakeFromNib {
    [super awakeFromNib];
    self.detailLabel.adjustsFontSizeToFitWidth = YES;
}

- (instancetype)initSheild{
    self = [[NSBundle mainBundle] loadNibNamed:@"SheildShowView" owner:nil options:nil][0];
    return self;
}

- (void)setSheildWithTitle:(NSString *)title message:(NSString *)message CertainBlock:(BlockSheildShowCertain)certainBlock
{
    self.titleLabel.text = title;
    self.detailLabel.text = message;
    
    _certainBlock = certainBlock;
}

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message CertainBlock:(BlockSheildShowCertain)certainBlock
{
    self = [[NSBundle mainBundle] loadNibNamed:@"SheildShowView" owner:nil options:nil][0];
    
    self.titleLabel.text = title;
    self.detailLabel.text = message;
    
    _certainBlock = certainBlock;
    
    return self;
}


//确定
- (void)certainBtnClick{
    
    [self endEditing:YES];
    
    if (self.certainBlock) {
        self.certainBlock();
    }
}

- (void)drawRect:(CGRect)rect {
    
    [super drawRect:rect];
    
    // Drawing code
    self.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.5];
    //    self.bgView.backgroundColor = KWhite_Color;
    //    self.bgView.layer.cornerRadius = 5;
    //    self.bgView.layer.masksToBounds = YES;
    //    self.bgView.layer.borderColor = [[UIColor whiteColor] CGColor];
    //    self.bgView.layer.borderWidth = 1.0;
   
    self.detailLabel.backgroundColor = KWhite_Color;
    self.detailLabel.textColor = [UIColor redColor];

    
    self.detailLabel.numberOfLines = 0;
    [self.ensureBtn setTitle:@"确定" forState:UIControlStateNormal];
    [self.ensureBtn addTarget:self action:@selector(certainBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.bgView.layer.cornerRadius = 5;
    self.bgView.layer.masksToBounds = YES;
    
}

-(void)setCertainBlock:(BlockSheildShowCertain)certainBlock
{
    _certainBlock = certainBlock;
}

@end
