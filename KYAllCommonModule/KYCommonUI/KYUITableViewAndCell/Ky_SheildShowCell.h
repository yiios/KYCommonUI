//
//  Ky_SheildShowCell.h
//  kyExpress_Internal
//
//  Created by alangavin on 2016/10/20.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Ky_SheildShowCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *describeLabel;

@end
