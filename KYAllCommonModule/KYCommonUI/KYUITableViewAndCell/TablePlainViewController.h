//
//  TablePlainViewController.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/8/30.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "InPutTableViewCell.h"
#import "ShowResultTableViewCell.h"
#import "AddressTableViewCell.h"
#import "InputMoreTableCell.h"
#import "SelectTimeTableViewCell.h"
#import "SelectTableViewCell.h"
#import "LineTableViewCell.h"


#import "BottomView.h"
#import "NoDataView.h"
#import "SheildInputView.h"
#import "SheildShowView.h"
#import "SheildAlertView.h"

#import "GrayTableHeader.h"

#define keyRow00 @"keyRow00"
#define keyRow01 @"keyRow01"
#define keyRow02 @"keyRow02"
#define keyRow03 @"keyRow03"

#define keyRow10 @"keyRow10"
#define keyRow11 @"keyRow11"
#define keyRow12 @"keyRow12"
#define keyRow13 @"keyRow13"

#define keyRow20 @"keyRow20"
#define keyRow21 @"keyRow21"
#define keyRow22 @"keyRow22"
#define keyRow23 @"keyRow23"

@interface TablePlainViewController : UIViewController

@property (nonatomic, strong) SheildInputView *sheildInputView;
@property (nonatomic, strong) SheildShowView *sheildShowView;
@property (nonatomic, strong) SheildAlertView *sheildAlertView;

@property (nonatomic, strong) NoDataView *noDataView;
@property (nonatomic ,strong) BottomView *bottomView;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic ,strong) NSArray *titleArray;
@property (nonatomic ,strong) NSMutableArray *detailArray;
@property (nonatomic ,strong) NSArray *placeHolderArray;

@property (nonatomic ,strong) NSMutableArray *dataArray;

@property (nonatomic ,strong) NSMutableDictionary *mutableDic;

@end
