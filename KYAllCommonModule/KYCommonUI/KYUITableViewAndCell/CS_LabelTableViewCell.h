//
//  CS_LabelTableViewCell.h
//  kyExpress_Internal
//
//  Created by caichaosen on 2017/4/5.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CS_LabelTableViewCell : UITableViewCell

@property(nonatomic, copy) NSString *titleString;
@property(nonatomic, copy) NSString *contentString;

@end
