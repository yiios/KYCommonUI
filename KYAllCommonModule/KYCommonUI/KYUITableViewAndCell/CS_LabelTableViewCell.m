//
//  CS_LabelTableViewCell.m
//  kyExpress_Internal
//
//  Created by caichaosen on 2017/4/5.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "CS_LabelTableViewCell.h"
#import "Tools.h"

@interface CS_LabelTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end

@implementation CS_LabelTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - setter
-(void)setTitleString:(NSString *)titleString{
    _titleString = titleString;
    self.titleLabel.text = [Tools isBlankString:titleString] ? @"" : titleString ;
}
-(void)setContentString:(NSString *)contentString{
    _contentString = contentString;
    self.contentLabel.text = [Tools isBlankString:contentString] ? @"" : contentString;
}
@end
