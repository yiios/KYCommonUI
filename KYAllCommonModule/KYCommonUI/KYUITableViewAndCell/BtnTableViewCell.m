//
//  BtnTableViewCell.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/8/25.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "BtnTableViewCell.h"

@interface BtnTableViewCell()

@end

@implementation BtnTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.btn.backgroundColor = KMain_Color;
    self.btn.titleLabel.font = KTextFont;
    
    self.btn.layer.cornerRadius = 3;
    self.btn.layer.masksToBounds = YES;
    [self.btn addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
}


+ (UINib *)nib{
    return [UINib nibWithNibName:@"BtnTableViewCell" bundle:nil];
}

- (void)btnClick{
    
    if (_blockBtnDidClick) {
        _blockBtnDidClick();
    }
}



- (void)setCellWithTitle:(NSString *)title blockBtnDidClick:(BlockBtnDidClick)blockBtnDidClick{

    [self.btn setTitle:title forState:UIControlStateNormal];
    self.blockBtnDidClick = blockBtnDidClick;
}

-(void)setBlockBtnDidClick:(BlockBtnDidClick)blockBtnDidClick
{
    _blockBtnDidClick = blockBtnDidClick;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
