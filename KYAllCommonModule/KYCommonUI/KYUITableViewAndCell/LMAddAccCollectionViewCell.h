//
//  AddAccCollectionViewCell.h
//  kyExpress_Internal
//
//  Created by caichaosen on 2017/3/7.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "AddImageModel.h"

@class LMAddUploadImageModel;

@class LMAddAccCollectionViewCell;
@protocol LMAddAccCollectionViewCellDelegate <NSObject>

- (void)didClickDeleteButtonAddAccCollectionViewCell:(LMAddAccCollectionViewCell *)cell;
- (void)didClickReuploadButtonAddAccCollectionViewCell:(LMAddAccCollectionViewCell *)cell;

@end

@interface LMAddAccCollectionViewCell : UICollectionViewCell
//@property (weak, nonatomic) IBOutlet UIImageView *cellImageView;


@property (nonatomic, strong) LMAddUploadImageModel *model;
@property (weak, nonatomic) IBOutlet UIImageView *addImageView;

//@property(nonatomic, strong) UIImage *image;
//// 状态枚举
//@property(nonatomic, assign) AddImageModelType status;
// 当前菊花
@property(nonatomic, weak) UIActivityIndicatorView *indicator;
// 当前状态按钮
@property (weak, nonatomic) IBOutlet UIButton *statusButton;
// 当前图标
@property (weak, nonatomic) IBOutlet UIImageView *currentImageView;


@property(nonatomic, assign) BOOL isHiddentButton;
@property(nonatomic, weak) id<LMAddAccCollectionViewCellDelegate> delegate;
+ (UINib *)nib;
@end
