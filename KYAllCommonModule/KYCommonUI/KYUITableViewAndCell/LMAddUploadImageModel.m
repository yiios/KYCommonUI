//
//  LMAddUploadImageModel.m
//  kyExpress_Internal
//
//  Created by limo on 2017/3/11.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "LMAddUploadImageModel.h"
#import "Tools.h"
#import "NSString+Encryption.h"

@implementation LMAddUploadImageModel

- (instancetype)initWithThumbnailImage:(UIImage *)thumbnailImage PhotoImage:(UIImage *)photoImage{
    self = [super init];
    if (self) {
        self.base64Image = [Tools getCustomKBimageData:photoImage withOderKB:180];
        self.md5String = _base64Image.md5StrForLow;
        self.thumbnailImage = photoImage;
        self.photoImage = photoImage;
        self.imageStatusType = LMAddUploadImageTypeNone; // 无状态
    }
    return self;
}

- (void)setImageIndex:(NSInteger)imageIndex {
    _imageIndex = imageIndex;
}

@end
