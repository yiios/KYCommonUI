//
//  TableGroupViewController.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/8/30.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "TableGroupViewController.h"


@interface TableGroupViewController ()<UITableViewDelegate,UITableViewDataSource>



@end

static NSString * cellSystemID = @"cellSystemID";
static NSString * cellInputID = @"cellInputID";
static NSString * cellInputMoreID = @"cellInputMoreID";
static NSString * cellAddressID = @"cellAddressID";
static NSString * cellShowResultID = @"cellShowResultID";
static NSString * cellSelectTimeID = @"cellSelectTimeID";
static NSString * cellSelectID = @"cellSelectID";
static NSString * cellLineID = @"cellLineID";


@implementation TableGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = KWhite_Color;
    
    [self.view addSubview:self.tableView];
    
//    NSLog(@"======subviews :  \n %@",self.view.subviews);
}



# pragma mark  - UITableViewDelegate & UITableViewDataSource

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  self.titleArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *arr = self.titleArray[section];
    return arr.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellSystemID];
  
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}


# pragma  mark - get tableView
- (UITableView *)tableView{
    
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kSCREEN_WIDTH, kSCREEN_HEIGHT) style: UITableViewStyleGrouped ];
        _tableView.backgroundColor = KBack_Color;
        
        WS(weakSelf);
        
        [_tableView registerNib:[InPutTableViewCell nib] forCellReuseIdentifier:cellInputID];
        [_tableView registerNib:[ShowResultTableViewCell nib] forCellReuseIdentifier:cellShowResultID];
        
        [_tableView registerNib:[SelectTimeTableViewCell nib] forCellReuseIdentifier:cellSelectTimeID];
        [_tableView registerNib:[SelectTableViewCell nib] forCellReuseIdentifier:cellSelectID];
        [_tableView registerNib:[InputMoreTableCell nib] forCellReuseIdentifier:cellInputMoreID];
        [_tableView registerNib:[AddressTableViewCell nib] forCellReuseIdentifier:cellAddressID];
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:cellSystemID];
        
//        _tableView.tableFooterView = [_tableView tableViewToFootViewWithTitle:@"完成" buttonBackgroundColor: KMain_Color handler:^(id sender) {
//            
//            [weakSelf.view endEditing:YES];
//            
//            
//        }];
        
        _tableView.scrollEnabled = YES;
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    return _tableView;
}

-(BottomView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[BottomView alloc]init];

        NSLog(@"============ bottomView =========");
    }
    return _bottomView;
}

#pragma mark - 没有数据时显示没有数据的视图
-(NoDataView *)noDataView
{
    if (!_noDataView) {
        _noDataView  = [[NoDataView alloc] initWithFrame:CGRectMake((kSCREEN_WIDTH-300)/2, kSCREEN_HEIGHT/2 - 150-24, 300, 300)];
        _noDataView.hidden = YES;
        [self.view addSubview:_noDataView];
        
    }
    return _noDataView;
}

-(SheildInputView *)sheildInputView
{
    if (!_sheildInputView) {
        _sheildInputView = [[SheildInputView alloc]initSheild];
        _sheildInputView.frame = self.view.bounds;
        _sheildInputView.hidden = YES;
        [self.view addSubview:_sheildInputView];
    }
    return _sheildInputView;
}

-(SheildAlertView *)sheildAlertView
{
    if (!_sheildAlertView) {
        _sheildAlertView = [[SheildAlertView alloc]initSheild];
        _sheildAlertView.hidden = YES;
        _sheildAlertView.frame = self.view.bounds;
        [self.view addSubview:_sheildAlertView];
    }
    return _sheildAlertView;
}

-(SheildShowView *)sheildShowView
{
    
    if (!_sheildShowView) {
//        WS(weakSelf);
        _sheildShowView = [[SheildShowView alloc]initSheild];
        _sheildShowView.frame = self.view.bounds;
        _sheildShowView.hidden = YES;
        [self.view addSubview:_sheildShowView];
    }
    
    return _sheildShowView;
}

#pragma mark - GetData

- (NSMutableDictionary *)mutableDic{
    if (!_mutableDic) {
        _mutableDic = [NSMutableDictionary dictionary];
    }
    return _mutableDic;
}

-(NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (NSMutableArray *)titleArray{
    if (!_titleArray) {
        _titleArray = [NSMutableArray array];
    }
    return _titleArray;
}

-(NSMutableArray *)detailArray
{
    if (!_detailArray) {
        _detailArray = [NSMutableArray array];
    }
    return _detailArray;
}

- (NSArray *)placeHolderArray{
    if (!_placeHolderArray) {
        _placeHolderArray = [NSMutableArray array];
    }
    return _placeHolderArray;
}



@end
