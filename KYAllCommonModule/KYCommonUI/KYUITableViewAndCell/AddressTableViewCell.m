//
//  AddressTableViewCell.m
//  kyExpress
//
//  Created by 陈志刚 on 15/12/7.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import "AddressTableViewCell.h"
#import "UIImage+ColorImage.h"
#import "UIView+Common.h"
#import "Tools.h"

@interface AddressTableViewCell ()<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *xingImage;
@property (unsafe_unretained, nonatomic) IBOutlet NSLayoutConstraint *xingImageLeftLayout;
@end

@implementation AddressTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.titleLabel.textColor = kColorLabel;
    
    [self setXingImageHide:YES];
    
    self.addressTextView.delegate = self;
    [self.addressTextView cornerRadiusToNumber:5];
    self.addressTextView.layer.borderWidth = 0.5;
    self.addressTextView.layer.borderColor = K16BorderColor.CGColor;
    self.addressTextView.backgroundColor = KWhite_Color;
    self.addressTextView.contentInset = UIEdgeInsetsMake(3, 6, -3, -6);
    
    self.locBtn.backgroundColor = KMain_Color;
    self.locBtn.layer.cornerRadius = 5;
    self.locBtn.hidden = YES;
    [self.locBtn setTitle:@"当前位置" forState:UIControlStateNormal];
    
    if (![self.strHoldPlace isEqualToString:self.addressTextView.text]) {
        self.addressTextView.textColor =kColorTextBlack;
    }
    
    //    self.backgroundColor = KBack_Color;
    //    self.addressTextView.backgroundColor = KBack_Color;
    
    //默认编辑按钮是隐藏的（郑成杰添加）
    self.editorButton.hidden =YES;
    self.editorButton.backgroundColor = KMain_Color;
    self.editorButton.layer.masksToBounds =YES;
    self.editorButton.layer.cornerRadius = 2;
    
    [self.editorButton setBackgroundImage:[UIImage imageWithColor:KMain_Color] forState:UIControlStateSelected];
    
    self.countLabel.hidden =YES;
}


- (void)refreshUIWithTitle:(NSString *)title placeHolder:(NSString *)placeholder detail:(NSString *)detail  didEndEditingBlock:(BlockString)didEndEditingBlock{
    
    self.titleLabel.text = title;
    _strHoldPlace = placeholder;
    self.countLabel.text = [NSString stringWithFormat:@"0/%ld字",(long)self.maxNum];
    
    if ([Tools isBlankString:detail]) {
        if ([Tools isBlankString:placeholder]) {
            self.addressTextView.text = @"";
            self.addressTextView.textColor = kColorPlaceHolder;
        }else{
            self.addressTextView.text = placeholder;
            self.addressTextView.textColor = kColorPlaceHolder;
        }
        
    }else{
        self.addressTextView.text = detail;
        self.addressTextView.textColor =kColorTextBlack;
        
        NSInteger textCount =detail.length;
        self.countLabel.text = [NSString stringWithFormat:@"%ld/%ld字",(long)textCount,(long)self.maxNum];
        
    }
    
    _blockString = didEndEditingBlock;
    
}

// 设置星号是否隐藏
- (void)setXingImageHide:(BOOL)isHide {
    self.xingImage.hidden = isHide;
    
    if (isHide == YES) {
        self.xingImageLeftLayout.constant = 2;
    } else {
        self.xingImageLeftLayout.constant = 15;
    }
}

- (void)setHoldPlaceWith:(NSString *)str{
    _strHoldPlace = str;
    if (![str isEqualToString:@""]) {
        self.addressTextView.text = str;
        self.addressTextView.textColor =[UIColor grayColor];
    }
}


#pragma mark - UITextViewDelegate
- (void)textViewDidEndEditing:(UITextView *)textView{
    NSLog(@"%@",textView.text);
    
    if (_blockString) {
        _blockString([textView.text isEqualToString:_strHoldPlace]?@"":textView.text);
    }
    
    if ([textView.text isEqualToString:@""]) {
        textView.text = _strHoldPlace;
        self.addressTextView.textColor = kColorPlaceHolder;
    }
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:_strHoldPlace]) {
        if (![_strHoldPlace isEqualToString:@""]) {
            textView.text = @"";
            self.addressTextView.textColor =kColorTextBlack;
        }
    }
    
    if (_blockBeginEdit) {
        _blockBeginEdit();
    }
}

//限制输入字数
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if (self.maxNum > 0) {
        NSString *temp = [textView.text stringByAppendingString:text];
        
        NSLog(@"addressTextView : %@ , %@ ; replacementText : %@ , temp : %@",self.addressTextView.text,textView.text,text,temp);
        
        if (![temp isEqualToString:self.strHoldPlace]) {
            NSInteger textCount =temp.length;
            self.countLabel.text = [NSString stringWithFormat:@"%ld/%ld字",(long)textCount,(long)self.maxNum];
        }else{
            NSLog(@"填入的是placeholder");
        }
        
        if (temp.length > self.maxNum) {
            NSString *str = [temp substringToIndex:self.maxNum];
            textView.text = str;
            
            self.countLabel.text = [NSString stringWithFormat:@"%ld/%ld字",(long)self.maxNum,self.maxNum];
            return NO;
        }
        
    }
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView
{
    //    NSLog(@"textViewDidChange : %@",textView.text);
    
    if (![textView.text isEqualToString:self.strHoldPlace]) {
        NSInteger textCount =textView.text.length;
        self.countLabel.text = [NSString stringWithFormat:@"%ld/%ld字",(long)textCount,(long)self.maxNum];
    }else{
        self.countLabel.text = [NSString stringWithFormat:@"0/%ld字",(long)self.maxNum];
    }
    
}


+ (UINib *)nib{
    return [UINib nibWithNibName:@"AddressTableViewCell" bundle:nil];
}

- (void)setBlockString:(BlockString)blockString{
    _blockString = blockString;
}

- (void)setBlockBeginEdit:(BlockBeginEdit)blockBeginEdit{
    _blockBeginEdit = blockBeginEdit;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end

