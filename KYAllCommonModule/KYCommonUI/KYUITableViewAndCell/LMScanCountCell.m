//
//  LMScanCountCell.m
//  kyExpress_Internal
//
//  Created by limo on 2017/3/7.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "LMScanCountCell.h"

@implementation LMScanCountCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

+(UINib *)nib {
    return [UINib nibWithNibName:@"LMScanCountCell" bundle:nil];
}

-(void)setUIWithTitle:(NSString *)title PlaceOne:(NSString *)one PlaceTwo:(NSString *)two{
    self.title.text = title;
    self.CastHeavy.placeholder = one;
    self.PartyNum.placeholder = two;
}
- (IBAction)ClickScanButton:(id)sender {
    if (_clickScan) {
        _clickScan();
    }
}

-(void)setClickScan:(ClickScanButton)clickScan {
    _clickScan = clickScan;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
