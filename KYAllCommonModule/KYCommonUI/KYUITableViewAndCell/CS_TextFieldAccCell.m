//
//  CS_TextFieldAccCell.m
//  kyExpress_Internal
//
//  Created by caichaosen on 2017/4/5.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "CS_TextFieldAccCell.h"
#import "Tools.h"

@interface CS_TextFieldAccCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UITextField *contentTextField;

@property (weak, nonatomic) IBOutlet UIButton *accButton;

@end

@implementation CS_TextFieldAccCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)didClickAccButtonEvent:(UIButton *)sender {
}

#pragma mark - setter
-(void)setTitleString:(NSString *)titleString{
    _titleString = titleString;
    self.titleLabel.text = [Tools isBlankString:titleString] ? @"" : titleString ;
}
-(void)setContentString:(NSString *)contentString{
    _contentString = contentString;
    self.contentTextField.text = [Tools isBlankString:contentString] ? @"" : contentString;
}
-(void)setPlaceHoldString:(NSString *)placeHoldString{
    _placeHoldString = placeHoldString;
    self.contentTextField.placeholder = [Tools isBlankString:placeHoldString] ? @"" : placeHoldString;
}

@end
