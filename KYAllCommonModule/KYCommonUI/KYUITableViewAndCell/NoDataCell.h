//
//  NoDataCell.h
//  kyExpress_Internal
//
//  Created by wangkai on 2016/10/26.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoDataCell : UITableViewCell
+ (UINib *)nib;
    
@end
