//
//  NoDataCell.m
//  kyExpress_Internal
//
//  Created by wangkai on 2016/10/26.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "NoDataCell.h"

@implementation NoDataCell


+ (UINib *)nib{
    return [UINib nibWithNibName:@"NoDataCell" bundle:nil];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self setSeparatorInset:UIEdgeInsetsMake(0, kSCREEN_WIDTH, 0, 0)];  //上左下右
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
