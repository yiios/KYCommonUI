//
//  KYCustomTableView.h
//  kyExpress_Internal
//
//  Created by wangkai on 2016/10/21.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

//#import <BaiduMapAPI_Search/BMKSearchComponent.h>
@interface KYCustomTableView : UITableView

@property (nonatomic, strong) UIView *changeTitTextView;

@property (nonatomic, strong) UIScrollView *scrolView;

@end
