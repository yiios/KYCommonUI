//
//  AddAccPhotoCollectionViewCell.m
//  kyExpress_Internal
//
//  Created by caichaosen on 2017/3/11.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "LMPhotoIconCollectionViewCell.h"

@implementation LMPhotoIconCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
+ (UINib *)nib{
    return [UINib nibWithNibName:@"LMPhotoIconCollectionViewCell" bundle:nil];
}


@end
