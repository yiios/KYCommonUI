//
//  AddressTableViewCell.h
//  kyExpress
//
//  Created by 陈志刚 on 15/12/7.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BlockString)(NSString *string);
typedef void(^BlockBeginEdit)(void);

@interface AddressTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextView *addressTextView;
@property (weak, nonatomic) IBOutlet UIButton *locBtn;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UIButton *editorButton;

@property (unsafe_unretained, nonatomic) IBOutlet NSLayoutConstraint *addressTestViewBottomConstranit;

- (void)refreshUIWithTitle:(NSString *)title placeHolder:(NSString *)placeholder detail:(NSString *)detail  didEndEditingBlock:(BlockString)didEndEditingBlock;

@property (nonatomic, copy) BlockString blockString;
@property (nonatomic, copy) BlockBeginEdit blockBeginEdit;

//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titileLabelLength;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TestViewleftConstranit;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TilteleftConstranit;

// 设置星号是否隐藏
- (void)setXingImageHide:(BOOL)isHide;

// 输入框限制字数多少
@property (nonatomic, assign) NSInteger maxNum;

/*客户介绍专用*/
@property (strong , nonatomic) NSString *strHoldPlace;
- (void)setHoldPlaceWith:(NSString *)str;
/*************/

- (void)setBlockString:(BlockString)blockString;
- (void)setBlockBeginEdit:(BlockBeginEdit)blockBeginEdit;

+ (UINib *)nib;

@end
