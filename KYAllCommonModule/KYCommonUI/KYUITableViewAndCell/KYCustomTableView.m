//
//  KYCustomTableView.m
//  kyExpress_Internal
//
//  Created by wangkai on 2016/10/21.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "KYCustomTableView.h"

@implementation KYCustomTableView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
    UIView *hitView = [super hitTest:point withEvent:event];
    NSLog(@"[hitView class] %@",[hitView class]);
    CGRect rect = [self.changeTitTextView convertRect:self.changeTitTextView.frame toView:self];
    if (self.changeTitTextView && CGRectContainsPoint(rect, point)) {
        self.scrollEnabled = NO;
        self.scrolView.scrollEnabled = NO;
    }
    else
    {
        self.scrollEnabled = YES;
        self.scrolView.scrollEnabled = YES;
    }
    return hitView;
}

@end
