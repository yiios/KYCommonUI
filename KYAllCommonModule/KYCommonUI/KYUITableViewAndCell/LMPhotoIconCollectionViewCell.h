//
//  AddAccPhotoCollectionViewCell.h
//  kyExpress_Internal
//
//  Created by caichaosen on 2017/3/11.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LMPhotoIconCollectionViewCell : UICollectionViewCell

+ (UINib *)nib;

@end
