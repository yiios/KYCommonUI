//
//  AddAccCollectionViewCell.m
//  kyExpress_Internal
//
//  Created by caichaosen on 2017/3/7.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "LMAddAccCollectionViewCell.h"

#import "LMAddUploadImageModel.h"

@interface LMAddAccCollectionViewCell ()

// 删除按钮
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;


@end

@implementation LMAddAccCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    
}
+ (UINib *)nib{
    return [UINib nibWithNibName:@"LMAddAccCollectionViewCell" bundle:nil];
}
#pragma mark - 删除点击
- (IBAction)deleteImageViewEvent:(UIButton *)sender {
    if([self.delegate respondsToSelector:@selector(didClickDeleteButtonAddAccCollectionViewCell:)]){
        [_delegate didClickDeleteButtonAddAccCollectionViewCell:self];
    }
}
#pragma mark - 重发点击
- (IBAction)reUploadImageEvent:(UIButton *)sender {
    if([self.delegate respondsToSelector:@selector(didClickReuploadButtonAddAccCollectionViewCell:)]){
        [_delegate didClickReuploadButtonAddAccCollectionViewCell:self];
    }
}


#pragma mark - setter
- (void)setModel:(LMAddUploadImageModel *)model {
    _model = model;
    
    [self.addImageView setImage:model.thumbnailImage];
    
    
    
    switch (model.imageStatusType) {
        case LMAddUploadImageTypeNone:
            [self browseStatus];
            break;
        case LMAddUploadImageTypeUploading:
            [self addActivityView];
            break;
        case LMAddUploadImageTypeSuccess:
            [self success];
            break;
        case LMAddUploadImageTypeFailure:
            [self failed];
            break;
    }
}

- (void)setIsHiddentButton:(BOOL)isHiddentButton {
    _isHiddentButton = isHiddentButton;
    
    if (self.model.imageStatusType == LMAddUploadImageTypeSuccess) {
        self.deleteButton.hidden = YES;
    } else {
        self.deleteButton.hidden = !isHiddentButton;
    }
}

// 拍照按钮
- (void)setUpPhotoCell{
    self.statusButton.hidden = YES;
    [self.indicator stopAnimating];
    [self.indicator removeFromSuperview];
}

// 添加菊花
- (void)addActivityView{
    if (!self.indicator){
        self.statusButton.hidden = YES;
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        self.indicator = indicator;
        [indicator setFrame:CGRectMake(0, 0, 30, 30)];
        indicator.center = self.contentView.center;
        indicator.color = KMain_Color;
        [indicator startAnimating];
        [self.contentView addSubview:indicator];
        [indicator setHidesWhenStopped:YES];
    }
}
// 成功
- (void)success{
    [self.indicator stopAnimating];
    [self.indicator removeFromSuperview];
    self.statusButton.hidden = NO;
    [self.statusButton setEnabled:NO];  // 做按钮类型判断
    [self.statusButton setImage:[UIImage imageNamed:@"成功"] forState:UIControlStateNormal];
}
// 失败
- (void)failed{
    [self.indicator stopAnimating];
    [self.indicator removeFromSuperview];
    self.statusButton.hidden = NO;
    [self.statusButton setEnabled:YES];
    [self.statusButton setImage:[UIImage imageNamed:@"失败"] forState:UIControlStateNormal];
}

// 缩略图浏览状态
- (void)browseStatus{
    [self.indicator stopAnimating];
    [self.indicator removeFromSuperview];
    self.statusButton.hidden = YES;
}
@end
