//
//  LMAddUploadImageModel.h
//  kyExpress_Internal
//
//  Created by limo on 2017/3/11.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, LMAddUploadImageType) {
    /** 1为最初的状态 */
    LMAddUploadImageTypeNone      = 1,
    /** 2为上传图片中 */
    LMAddUploadImageTypeUploading = 2,
    /** 3为成功 */
    LMAddUploadImageTypeSuccess   = 3,
    /** 4为失败 */
    LMAddUploadImageTypeFailure   = 4,
};

@interface LMAddUploadImageModel : NSObject

/**
 图片 Base64 字符串
 */
@property (nonatomic, strong) NSString *base64Image;

/**
 图片 Base64 的 hash
 */
@property (nonatomic, strong) NSString *md5String;

/**
 图片在图片数组中的序号
 */
@property (nonatomic, assign, readonly) NSInteger imageIndex;

/**
 缩略图
 */
@property (nonatomic, strong) UIImage  *thumbnailImage;
/**
 图片
 */
@property (nonatomic, strong) UIImage  *photoImage;

/**
 图片当前的状态
 */
@property (nonatomic, assign) LMAddUploadImageType imageStatusType;

- (instancetype)initWithThumbnailImage:(UIImage *)thumbnailImage PhotoImage:(UIImage *)photoImage;

- (void)setImageIndex:(NSInteger)imageIndex;

@end
