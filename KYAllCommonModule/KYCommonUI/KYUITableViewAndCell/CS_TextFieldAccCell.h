//
//  CS_TextFieldAccCell.h
//  kyExpress_Internal
//
//  Created by caichaosen on 2017/4/5.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CS_TextFieldAccCell : UITableViewCell

@property(nonatomic, copy) NSString *titleString;
@property(nonatomic, copy) NSString *contentString;
@property(nonatomic, copy) NSString *placeHoldString;

@end
