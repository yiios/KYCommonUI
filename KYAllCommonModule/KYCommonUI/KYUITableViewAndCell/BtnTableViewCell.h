//
//  BtnTableViewCell.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/8/25.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BlockBtnDidClick)(void);

@interface BtnTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btn;

@property (nonatomic,copy) BlockBtnDidClick blockBtnDidClick;

- (void)setCellWithTitle:(NSString *)title blockBtnDidClick:(BlockBtnDidClick)blockBtnDidClick;
+ (UINib *)nib;

@end
