//
//  LMScanCountCell.h
//  kyExpress_Internal
//
//  Created by limo on 2017/3/7.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ClickScanButton)(void);

@interface LMScanCountCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *CastHeavy; //!<抛重
@property (weak, nonatomic) IBOutlet UITextField *PartyNum;  //!< 方数
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIButton *scanButton;

@property (nonatomic,copy) ClickScanButton clickScan;

-(void)setUIWithTitle:(NSString *)title PlaceOne:(NSString *)one PlaceTwo:(NSString *)two;

+(UINib *)nib;

@end
