//
//  LocationQueryCollectionViewCell.m
//  kyExpress
//
//  Created by 陈志刚 on 15/12/9.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import "LocationQueryCollectionViewCell.h"

@implementation LocationQueryCollectionViewCell

+ (UINib *)nib{
    return [UINib nibWithNibName:@"LocationQueryCollectionViewCell" bundle:nil];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.locationName.layer.borderWidth = 1;
    self.locationName.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
    
}

@end
