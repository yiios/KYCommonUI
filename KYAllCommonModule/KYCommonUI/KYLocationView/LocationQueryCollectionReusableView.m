//
//  LocationQueryCollectionReusableView.m
//  kyExpress
//
//  Created by 陈志刚 on 15/12/9.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import "LocationQueryCollectionReusableView.h"

@implementation LocationQueryCollectionReusableView

- (void)awakeFromNib {

}

+ (UINib *)nib{
    return [UINib nibWithNibName:@"LocationQueryCollectionReusableView" bundle:nil];
}

@end
