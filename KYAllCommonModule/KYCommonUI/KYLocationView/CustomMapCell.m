//
//  CustomMapCell.m
//  IYLM
//
//  Created by Jian-Ye on 12-11-8.
//  Copyright (c) 2012年 Jian-Ye. All rights reserved.
//

#import "CustomMapCell.h"

@implementation CustomMapCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    
    return self;
}



-(void)setTitle:(NSString *)title
{
    _title = title;
    self.titleLabel.text = _title;
//    self.titleLabel.backgroundColor = [UIColor redColor];
//    self.detailLabel.backgroundColor = [UIColor cyanColor];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    
}

-(void)setDetail:(NSString *)detail
{ 
    _detail = detail;
    self.detailLabel.text = _detail;
}


@end
