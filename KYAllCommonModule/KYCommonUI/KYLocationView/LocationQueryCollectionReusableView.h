//
//  LocationQueryCollectionReusableView.h
//  kyExpress
//
//  Created by 陈志刚 on 15/12/9.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationQueryCollectionReusableView : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

+ (UINib *)nib;

@end
