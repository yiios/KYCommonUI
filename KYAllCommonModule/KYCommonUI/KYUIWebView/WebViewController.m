//
//  WebViewController.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/7/29.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "WebViewController.h"
#import "RDVTabBarController.h"

@interface WebViewController ()


@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];    
    self.navigationButtonsHidden = YES;
    
        
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.rdv_tabBarController setTabBarHidden:YES animated:YES];

}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];


}



- (void)setNavigationBar
{
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc]initWithTitle:@"更多" style:UIBarButtonItemStylePlain target:self action:@selector(showList)];
   
    self.navigationItem.rightBarButtonItems = @[item1];
}

- (void)showList{

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
