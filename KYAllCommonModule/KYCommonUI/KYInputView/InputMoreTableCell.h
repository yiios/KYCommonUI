//
//  InputMoreTableCell.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/3/14.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InputMoreTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextView *inputField;
//@property (weak, nonatomic) IBOutlet UILabel *labelhidden;


+ (UINib *)nib;
@end
