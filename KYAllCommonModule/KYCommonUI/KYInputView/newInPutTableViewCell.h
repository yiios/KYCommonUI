//
//  newInPutTableViewCell.h
//  kyExpress_Internal
//
//  Created by ljz on 16/4/25.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BlockString)(NSString * string);

typedef void(^BlockChangeString)(NSString * string);

typedef void(^BlockBeginEdit)(void);



@interface newInPutTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *inPutTextField;


//@property (weak, nonatomic) IBOutlet UIImageView *imagePic;


@property (nonatomic, copy) BlockString blockString;
@property (nonatomic, copy) BlockChangeString BlockChangeString;
@property (nonatomic, copy) BlockBeginEdit blockbeginEdit;


+ (UINib *)nib;

- (void)refreshUIWithTitle:(NSString *)string place:(NSString *)placeString;

- (void)setBlockString:(BlockString)blockString;

-(void)setBlockChangeString:(BlockChangeString)BlockChangeString;


- (void)isLimited:(BOOL)flag;



@end
