//
//  newInPutTableViewCell.m
//  kyExpress_Internal
//
//  Created by ljz on 16/4/25.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "newInPutTableViewCell.h"


@interface newInPutTableViewCell ()<UITextFieldDelegate>{
    BOOL _isLimite;
}
@end

@implementation newInPutTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    self.titleLabel.font = KTextFont;
    self.titleLabel.textColor = kColorTextBlack;

    self.inPutTextField.delegate = self;
    self.inPutTextField.adjustsFontSizeToFitWidth = YES;
    
    self.backgroundColor = [UIColor colorWithHexString:@"0xf5f5f5"];

    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.inPutTextField.textColor = kColorTextBlack;
    
    
    [self.inPutTextField addTarget:self action:@selector(textChangeAction:)
                  forControlEvents:UIControlEventEditingChanged];
    
    self.inPutTextField.font = STextFont;
}


-(void)textChangeAction:(id )sender{
    
    UITextField *textField = (UITextField *)sender;
    
    if (_BlockChangeString) {
        _BlockChangeString(textField.text);
    }
}

+ (UINib *)nib{
    return [UINib nibWithNibName:@"newInPutTableViewCell" bundle:nil];
}

- (void)isLimited:(BOOL)flag{
    _isLimite = flag;
}

- (void)refreshUIWithTitle:(NSString *)string place:(NSString *)placeString{
    
    [self.titleLabel setText:string];
   // self.imagePic.image = [UIImage imageNamed:string];
    [self.inPutTextField setPlaceholder:placeString];
    
}


- (void)setBlockString:(BlockString)blockString{
    _blockString = blockString;
}

-(void)setBlockChangeString:(BlockChangeString)BlockChangeString{
    
    _BlockChangeString = BlockChangeString;
}

- (void)setBlockbeginEdit:(BlockBeginEdit)blockbeginEdit{
    _blockbeginEdit = blockbeginEdit;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (_blockbeginEdit) {
        _blockbeginEdit();
    }
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString*)string{
    
    //    NSLog(@"[UITextInputMode currentInputMode]primaryLanguage] = %@",[[UITextInputMode currentInputMode]primaryLanguage]);
    //
    //    NSLog(@"[UITextInputMode currentInputMode]primaryLanguage]222222 = %@",[[UIApplication sharedApplication]textInputMode].primaryLanguage);
    
    if ([[[UITextInputMode currentInputMode]primaryLanguage] isEqualToString:@"emoji"]) {
        return NO;
    }
    
    //    if (_BlockChangeString) {
    //        _BlockChangeString(textField.text);
    //    }
    //
    //    if ([Tools isMoreThan20Character:textField.text]) {
    //        NSLog(@"输入文字不能超过20个字符");
    //    }
    
    return YES;
    
    
    //    NSCharacterSet *cs;
    //    cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS]invertedSet];
    //    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs]componentsJoinedByString:@""];
    //    NSLog(@"匹配 cs  = %@ string = %@  filtered = %@",cs ,string,filtered);
    //    BOOL canChange = [string isEqualToString:filtered];
    //    return canChange;
    //
}






- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (_blockString) {
        _blockString(textField.text);
    }
    
    //    if (![Tools isMoreThan20Character:textField.text]) {
    //
    //        if (_blockString) {
    //            _blockString(textField.text);
    //        }
    //
    //    }else{
    //
    //
    //        [Tools myToast:@"输入文字不能超过20个字符"];
    //
    //        if (_blockString) {
    //            NSString *subStr = [textField.text substringToIndex:5];
    //            NSLog(@"substr---%@",subStr);
    //            _blockString(subStr);
    //        }
    //    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}


@end
