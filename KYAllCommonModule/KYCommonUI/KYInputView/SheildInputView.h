//
//  SheildInputView.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/7/19.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^CertainBlock)(NSString * string);
typedef void(^CanceBlock)(void);

@interface SheildInputView : UIView

@property (nonatomic, copy) CanceBlock canceBlock;
@property (nonatomic, copy) CertainBlock certainBlock;

@property (weak, nonatomic) IBOutlet UIView *tanView;
@property (weak, nonatomic) IBOutlet UITextField *inputField;//


- (instancetype)initSheild;
- (void)setSheildWithTitle:(NSString *)title placeholder:(NSString *)placeholder CertainBlock:(CertainBlock)CertainBlock CanceBlock:(CanceBlock)CanceBlock;

- (void)refrshUIWithTitle:(NSString *)title placeholder:(NSString *)placeholder CertainBlock:(CertainBlock)CertainBlock  CanceBlock:(CanceBlock)CanceBlock;

- (instancetype)initWithTitle:(NSString *)title placeholder:(NSString *)placeholder CertainBlock:(CertainBlock)CertainBlock  CanceBlock:(CanceBlock)CanceBlock;

@end
