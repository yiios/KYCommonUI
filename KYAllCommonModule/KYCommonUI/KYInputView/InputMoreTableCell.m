//
//  InputMoreTableCell.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/3/14.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "InputMoreTableCell.h"


@interface InputMoreTableCell()<UITextViewDelegate>

@end

@implementation InputMoreTableCell

- (void)awakeFromNib {
 
    [super awakeFromNib];
    self.titleLabel.font = KTextFont;
    self.inputField.font = STextFont;
    self.titleLabel.textColor = kColorTextBlack;
    self.inputField.textColor = kColorTextBlack;
    
    _inputField.layer.borderColor =  [UIColor groupTableViewBackgroundColor].CGColor;
    _inputField.layer.borderWidth = 1;
    _inputField.layer.cornerRadius = 3;
    _inputField.layer.masksToBounds = YES;
}



+ (UINib *)nib{
    return [UINib nibWithNibName:@"InputMoreTableCell" bundle:nil];
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView{
//    if (textView.text.length == 0)
//        _labelhidden.hidden = NO;
//  else
//      _labelhidden.hidden =YES;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
