//
//  newInPutTableViewCell.h
//  kyExpress_Internal
//
//  Created by ljz on 16/4/25.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BlockString)(NSString * string);
typedef void(^BlockChangeString)(NSString * string);
typedef void(^BlockBeginEdit)(void);
typedef void(^BlockArrowClick)(void);
typedef void(^BlockDeleteClick)(void);


@interface accountInPutTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *deleteImage;
@property (weak, nonatomic) IBOutlet UIImageView *Arrows;
@property (weak, nonatomic) IBOutlet UITextField *inPutTextField;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;
@property (weak, nonatomic) IBOutlet UIView *containView;
//
//@property (weak, nonatomic) IBOutlet UIButton *regest;

@property (nonatomic, copy) BlockString blockString;
@property (nonatomic, copy) BlockChangeString BlockChangeString;
@property (nonatomic, copy) BlockBeginEdit blockbeginEdit;
@property (nonatomic, copy) BlockArrowClick BlockArrowClick;
@property (nonatomic, copy) BlockDeleteClick BlockDeleteClick;




+ (UINib *)nib;

- (void)refreshUIWithTitle:(NSString *)string place:(NSString *)placeString;

- (void)setBlockString:(BlockString)blockString;

-(void)setBlockChangeString:(BlockChangeString)BlockChangeString;


- (void)isLimited:(BOOL)flag;



@end
