//
//  InPutTableViewCell.h
//  SpanLogistics
//
//  Created by 陈志刚 on 15/12/3.
//  Copyright © 2015年 Kylin. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BlockString)(NSString * string);
typedef void(^BlockChangeString)(NSString * string);
typedef void(^BlockBeginEdit)(void);
typedef void(^BlockInputCellBeginEdit)(NSString * string);
typedef void(^BlockDidBeginEdit)(NSString *);
typedef void(^BlockShouldBeginEdit)(NSString * string);
typedef void(^BlockImageBut)(void);
typedef void(^BlockEntrustBookEdit)(void);
typedef void(^BlockAreaLabelClick)(void); //右边的label被点击


@interface InPutTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *inPutTextField;
@property (weak, nonatomic) IBOutlet UILabel *areaLabel; //后面的label 默认隐藏 考勤打卡 附近点部

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelTextFieldMargin;  // 用来表示label和textfield之间的距离
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *areaLabelWidth;

@property (nonatomic, copy) BlockAreaLabelClick blockAreaLabelClick;
@property (nonatomic, copy) BlockString blockString;
@property (nonatomic, copy) BlockChangeString BlockChangeString;
@property (nonatomic, copy) BlockBeginEdit blockbeginEdit;
@property (nonatomic, copy) BlockShouldBeginEdit blockShouldBeginEdit;
@property (nonatomic, copy) BlockDidBeginEdit blockDidBeginEdit;
@property (nonatomic, copy) BlockImageBut imageBlock;
@property (nonatomic, copy) BlockInputCellBeginEdit blockInputCellBeginEdit;
@property (nonatomic, copy) BlockEntrustBookEdit blockEntrustBookEdit;

@property (nonatomic, strong) UIButton *weiTuoButton;   //下单模块的委托书按钮
@property (nonatomic, strong) UIButton *imageLableBut;
@property (nonatomic, strong) UILabel *rmbLabel;//元字默认隐藏
@property (nonatomic, strong) UIView *segmentationView;  //cell分割线

@property (nonatomic, assign) BOOL isLimitLength;  // 是否限制输入长度
@property (nonatomic, assign) BOOL isOrderWeigth;  // 用来表示发货信息输入重量的判断
@property (nonatomic, assign) BOOL isOrderPoll;   // 用来判断是否是发货信息总票数输入判断
@property (nonatomic, assign) BOOL isSKU;      //SKU模块使用
@property (nonatomic, assign) BOOL isTextFiledValueCanChange;


- (void)refreshUIWithTitle:(NSString *)string place:(NSString *)placeString;
- (void)refreshUIWithTitle:(NSString *)title placeHolder:(NSString *)placeholder detail:(NSString *)detail  didEndEditingBlock:(BlockString)didEndEditingBlock;

- (void)isLimited:(BOOL)flag;
- (void)toSetUnitWithName:(NSString *)unitName;
- (void)setXingImageHide:(BOOL)isHide; // 是否需要隐藏*号 默认隐藏

+ (UINib *)nib;
- (void)setBlockString:(BlockString)blockString;
- (void)setBlockbeginEdit:(BlockBeginEdit)blockbeginEdit;
- (void)setBlockChangeString:(BlockChangeString)BlockChangeString;
- (void)setImageBlock:(BlockImageBut)imageBlock;
- (void)setBlockShouldBeginEdit:(BlockShouldBeginEdit)blockShouldBeginEdit;
- (void)setBlockInputCellBeginEdit:(BlockInputCellBeginEdit)blockInputCellBeginEdit;

@end
