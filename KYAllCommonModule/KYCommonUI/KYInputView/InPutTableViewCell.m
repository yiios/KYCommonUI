//
//  InPutTableViewCell.m
//  SpanLogistics
//
//  Created by 陈志刚 on 15/12/3.
//  Copyright © 2015年 Kylin. All rights reserved.
//

#import "InPutTableViewCell.h"

//#define NUMBERS @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

@interface InPutTableViewCell ()<UITextFieldDelegate>{
    BOOL _isLimite;
}
@property (weak, nonatomic) IBOutlet UIImageView *xingImage;
@property (unsafe_unretained, nonatomic) IBOutlet NSLayoutConstraint *xingImageLeftLayout;
@end

@implementation InPutTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;

    [self setXingImageHide:YES];

    self.titleLabel.font = KTextFont;
    self.titleLabel.textColor = kColorTextBlack;

    self.isTextFiledValueCanChange=NO;
    self.inPutTextField.delegate = self;
    self.inPutTextField.adjustsFontSizeToFitWidth = YES;
    [self.inPutTextField addTarget:self action:@selector(textChangeAction:)
                  forControlEvents:UIControlEventEditingChanged];
    self.inPutTextField.font = KTextFont;
    self.inPutTextField.textColor = kColorTextBlack;

    
    self.imageLableBut = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.imageLableBut addTarget:self action:@selector(imageLableButAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.imageLableBut];
    
    self.rmbLabel = [[UILabel alloc]init];
    self.rmbLabel.frame = CGRectMake(KSCREEN_W-54, 0, 44, 44);
    [self.contentView addSubview:self.rmbLabel];
    self.rmbLabel.text = @"元";
    self.rmbLabel.hidden = YES;
    
    self.areaLabel.hidden = YES;
    self.areaLabel.font = STextFont;
    self.areaLabel.backgroundColor = KMain_Color;
    self.areaLabel.textColor = [UIColor whiteColor];
    self.areaLabel.textAlignment = NSTextAlignmentCenter;
    self.areaLabelWidth.constant = 0;
    self.areaLabel.userInteractionEnabled = YES;
    [self.areaLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(areaLabelClick)]];
    
    //我要下单增加委托书按钮
    self.weiTuoButton = [[UIButton alloc]init];
    self.weiTuoButton.backgroundColor = KMain_Color;
    self.weiTuoButton.layer.masksToBounds =YES;
    self.weiTuoButton.layer.cornerRadius = 2;
    self.weiTuoButton.frame = CGRectMake(KSCREEN_W-60, 10, 50, 25);
    [self.weiTuoButton setTitle:@"委托书" forState:UIControlStateNormal];
    [self.weiTuoButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.weiTuoButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.weiTuoButton addTarget:self action:@selector(entrustBookClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.weiTuoButton];
    self.weiTuoButton.hidden =YES;

    self.segmentationView = [[UIView alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(self.frame)-1, KSCREEN_W-30, 1)];
    self.segmentationView.backgroundColor = K16GrayColor;
    [self addSubview:self.segmentationView];
    self.segmentationView.hidden = YES;
}


// 设置星号是否隐藏
- (void)setXingImageHide:(BOOL)isHide {
    self.xingImage.hidden = isHide;
    
    if (isHide == YES) {
        self.xingImageLeftLayout.constant = 2;
    } else {
        self.xingImageLeftLayout.constant = 15;
    }
}


- (void)refreshUIWithTitle:(NSString *)title
               placeHolder:(NSString *)placeholder
                    detail:(NSString *)detail
        didEndEditingBlock:(BlockString)didEndEditingBlock{
    
    [self.titleLabel setText:title];
    [self.inPutTextField setPlaceholder:placeholder];
    [self.inPutTextField setValue:kColorPlaceHolder forKeyPath:@"_placeholderLabel.textColor"];
    
    self.inPutTextField.text = detail;
    
    _blockString = didEndEditingBlock;
    
}


- (void)refreshUIWithTitle:(NSString *)string place:(NSString *)placeString {
    
    [self.titleLabel setText:string];
    [self.inPutTextField setPlaceholder:placeString];
    
}

- (void)imageLableButAction {
    if (_imageBlock) {
        _imageBlock();
    }
}


#pragma mark  textChange
-(void)textChangeAction:(id )sender {
    
    UITextField *textField = (UITextField *)sender;
    
    NSString *str = [textField textInRange:textField.markedTextRange];
    
//    NSLog(@"textChangeAction str = %@",str);
    if (![str isEqualToString:@""]) {
        return;
    }
    
    if (_BlockChangeString) {
        _BlockChangeString(textField.text);
    }
}


- (void)isLimited:(BOOL)flag{
    _isLimite = flag;
}


#pragma mark ShouldBeginEditing
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldBeginEditing ---------- ");
    
    if (_blockShouldBeginEdit) {
        _blockShouldBeginEdit(textField.text);
    }
    return YES;
}


#pragma mark DidBeginEditing
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    NSLog(@"textFieldDidBeginEditing----------");
    
    if (_blockShouldBeginEdit) {
        _blockShouldBeginEdit(textField.text);
    }
    
    if (_blockbeginEdit) {
        _blockbeginEdit();
        return;
    }
    
    if (_blockInputCellBeginEdit) {
        _blockInputCellBeginEdit(textField.text);
    }
    
    if (_blockDidBeginEdit) {
        _blockDidBeginEdit(textField.text);
    }
    
}


#pragma mark shouldChangeCharactersInRange
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString*)string{
    
    
    if ([[[UITextInputMode currentInputMode]primaryLanguage] isEqualToString:@"emoji"]) {
        return NO;
    }
    
    // 出车刷卡路程限制
    if (self.isLimitLength == YES) {
        
        if (textField.text.length < 8) {
            return YES;
        }
        if ([string isEqualToString:@""]) {
            return YES;
        }
        return NO;
    }
    
    // 下单里重量，件数，做限制
    if (self.isOrderWeigth == YES) {
        NSString *str = textField.text;
        // 粘贴的时候，不能一下粘贴6位以上的字符
        if (string.length > 6) {
            return NO;
        }
        // 如果文本框的文字是0 ，再输入0 就不可以输
        if ([str isEqualToString:@"0"] && [string isEqualToString:@"0"]) {
            return NO;
        }
        else if([str isEqualToString:@"0"])  // 如果文本框文字是0 ，输入文字不是0，就把输入文字赋值给文本框
        {
            textField.text = string;
            return NO;
        }
        
        if (textField.text.length < 6) {
            return YES;
        }
        if ([string isEqualToString:@""]) {
            return YES;
        }
        return NO;
    }
    if (self.isOrderPoll == YES) {
        NSString *str = textField.text;
        // 粘贴的时候，不能一下粘贴6位以上的字符
        if (string.length > 3) {
            return NO;
        }
        // 如果文本框的文字是0 ，再输入0 就不可以输
        if ([str isEqualToString:@"0"] && [string isEqualToString:@"0"]) {
            return NO;
        }
        else if([str isEqualToString:@"0"])  // 如果文本框文字是0 ，输入文字不是0，就把输入文字赋值给文本框
        {
            textField.text = string;
            return NO;
        }
        
        if (textField.text.length < 3) {
            return YES;
        }
        if ([string isEqualToString:@""]) {
            return YES;
        }
        return NO;
    }
    
    if (self.isTextFiledValueCanChange) {
        if ((textField.text.length+string.length)>15) {
            return NO;
        }else{
            return YES;
        }
    }
    
    if(_isSKU){
        //限制小数点两位
        NSMutableString * futureString = [NSMutableString stringWithString:textField.text];
        [futureString  insertString:string atIndex:range.location];
        NSInteger flag=0;
        const NSInteger limited = 2;//小数点后需要限制的个数
        for (NSInteger i = futureString.length-1; i>=0; i--) {
            
            if ([futureString characterAtIndex:i] == '.') {
                if (flag > limited) {
                    return NO;
                }
                break;
            }
            flag++;
        }
    }

    return YES;

}



#pragma mark DidEndEditing
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (_blockString) {
        _blockString(textField.text);
    }
    
}


#pragma mark 单位标示
- (void)toSetUnitWithName:(NSString *)unitName{
    [self.imageLableBut setTitle:unitName forState:UIControlStateNormal];
    [self.imageLableBut setTitleColor:kColorTextBlack forState:UIControlStateNormal];
    self.imageLableBut.frame = CGRectMake(CGRectGetWidth(self.contentView.frame) - 38, 9, 27, 27);
    self.imageLableBut.userInteractionEnabled = NO;
   
}

+ (UINib *)nib{
    return [UINib nibWithNibName:@"InPutTableViewCell" bundle:nil];
}

-(void)areaLabelClick{
    
    if (_blockAreaLabelClick) {
        _blockAreaLabelClick();
    }
}

//委托书按钮点击
-(void)entrustBookClick:(UIButton *)sender{
    if (_blockEntrustBookEdit) {
        _blockEntrustBookEdit();
    }
}

-(void)setBlockbeginEdit:(BlockBeginEdit)blockbeginEdit {
    _blockbeginEdit = blockbeginEdit;
}

-(void)setBlockDidBeginEdit:(BlockDidBeginEdit)blockDidBeginEdit {
    _blockDidBeginEdit = blockDidBeginEdit;
}

-(void)setBlockShouldBeginEdit:(BlockShouldBeginEdit)blockShouldBeginEdit {
    _blockShouldBeginEdit = blockShouldBeginEdit;
}

- (void)setBlockString:(BlockString)blockString {
    _blockString = blockString;
}

-(void)setBlockChangeString:(BlockChangeString)BlockChangeString{
    _BlockChangeString = BlockChangeString;
}
- (void)setImageBlock:(BlockImageBut)imageBlock{
    _imageBlock = imageBlock;
}

-(void)setBlockInputCellBeginEdit:(BlockInputCellBeginEdit)blockInputCellBeginEdit {
    _blockInputCellBeginEdit = blockInputCellBeginEdit;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}

@end
