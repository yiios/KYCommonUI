//
//  SheildInputView.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/7/19.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "SheildInputView.h"

@interface SheildInputView()


@property (weak, nonatomic) IBOutlet UILabel *titleLabel;//标题-温馨提示


//@property (weak, nonatomic) IBOutlet UIView *tanView;

@property (weak, nonatomic) IBOutlet UIButton *cancerBtn;
@property (weak, nonatomic) IBOutlet UIButton *certainBtn;

@end

@implementation SheildInputView



//取消
- (IBAction)canceButton:(id)sender {
    
    if (_canceBlock) {
        _canceBlock();
    }
}

//确定
- (IBAction)certainButton:(id)sender {
    
    [self endEditing:YES];
    
    if (_certainBtn) {
        
        NSLog(@"已输入：%@",self.inputField.text);
        _certainBlock(self.inputField.text);
        
    }
}

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    return self;
}

-(instancetype)init
{
    self = [super init];
    
    return self;
}



- (instancetype)initSheild{
    self = [[NSBundle mainBundle] loadNibNamed:@"SheildInputView" owner:nil options:nil][0];
    return self;
}

- (void)setSheildWithTitle:(NSString *)title placeholder:(NSString *)placeholder CertainBlock:(CertainBlock)CertainBlock CanceBlock:(CanceBlock)CanceBlock
{
    self.titleLabel.text = title;
    self.inputField.placeholder = placeholder;
    self.inputField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    _canceBlock = CanceBlock;
    _certainBlock = CertainBlock;
}


- (instancetype)initWithTitle:(NSString *)title placeholder:(NSString *)placeholder CertainBlock:(CertainBlock)CertainBlock
         CanceBlock:(CanceBlock)CanceBlock
{
//    self = [super init];
    
     self = [[NSBundle mainBundle] loadNibNamed:@"SheildInputView" owner:nil options:nil][0];
    
    self.titleLabel.text = title;
    self.inputField.placeholder = placeholder;
    self.inputField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.inputField.backgroundColor=K16TextBackGroundColor;

    _canceBlock = CanceBlock;
    _certainBlock = CertainBlock;
    
    return self;
}


-(void)awakeFromNib
{
    [super awakeFromNib];
     self.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.5];
     [self.cancerBtn setTitleColor:KMain_Color forState:UIControlStateNormal];
    [self.certainBtn setTitleColor:KMain_Color forState:UIControlStateNormal];
}




- (void)refrshUIWithTitle:(NSString *)title placeholder:(NSString *)placeholder CertainBlock:(CertainBlock)CertainBlock
         CanceBlock:(CanceBlock)CanceBlock
{
    self.titleLabel.text = title;
    self.inputField.placeholder = placeholder;
    _canceBlock = CanceBlock;
    _certainBlock = CertainBlock;
}

- (void)CanceBlock:(CanceBlock)CanceBlock{
    _canceBlock = CanceBlock;
}

- (void)CertainBlock:(CertainBlock)CertainBlock{
    _certainBlock = CertainBlock;
}

- (void)drawRect:(CGRect)rect {
    
    [super drawRect:rect];
    
    // Drawing code
    self.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.5];
    self.tanView.backgroundColor = KWhite_Color;
    self.tanView.layer.cornerRadius = 5;
    
    self.tanView.layer.masksToBounds = YES;
    self.tanView.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.tanView.layer.borderWidth = 1.0;
    [self.cancerBtn setTitle:@"取消" forState:UIControlStateNormal];
    [self.certainBtn setTitle:@"确定" forState:UIControlStateNormal];
    
}


@end
