//
//  Ky_GradeLabel.m
//  我的评分
//
//  Created by alangavin on 16/9/1.
//  Copyright © 2016年 alangavin. All rights reserved.
//

#import "Ky_GradeLabel.h"
#define kScreenW [UIScreen mainScreen].bounds.size.width
#define fontSize 15.f

@implementation Ky_GradeLabel

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
      

        self.textAlignment =NSTextAlignmentCenter;
        self.font = [UIFont systemFontOfSize:fontSize];
        self.numberOfLines = 0;

    }
    return self;
}
//一行多字
-(CGRect)ky_LabelFrameSizeofFiteWid:(CGRect)frame{
    
    CGSize size =[self boundingRectWithSize:CGSizeMake(frame.size.width, frame.size.height)];
    CGRect rect=CGRectMake(frame.origin.x, frame.origin.y, size.width, 18);
    return rect;
}
-(CGRect)ky_LabelFrameSizeofFite:(CGRect)frame{
    
    CGSize size =[self boundingRectWithSize:CGSizeMake(frame.size.width, frame.size.height)];
    CGRect rect;
    if (size.height>K16CellRowHeight*0.7) {
    rect=CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, size.height);
    }else{
    rect=CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 18);
    }
    
    
    
    return rect;
}
- (CGSize)boundingRectWithSize:(CGSize)size
{
    NSDictionary *attribute = @{NSFontAttributeName: self.font};
    
    CGSize retSize = [self.text boundingRectWithSize:size
                                             options:\
                      NSStringDrawingTruncatesLastVisibleLine |
                      NSStringDrawingUsesLineFragmentOrigin |
                      NSStringDrawingUsesFontLeading
                                          attributes:attribute
                                             context:nil].size;
    
    return retSize;
}
-(void)setPlaceholder:(NSString *)placeholder{
    _placeholder=placeholder;
    self.text=placeholder;
    self.textColor=KTextGray;
}

//初始化方法一
-(id) initWithFrame:(CGRect)frame andInsets:(UIEdgeInsets)insets {
    self = [super initWithFrame:frame];
    if(self){
        self.insets = insets;
    }
    return self;
}
//初始化方法二
-(id) initWithInsets:(UIEdgeInsets)insets {
    self = [super init];
    if(self){
        self.insets = insets;
    }
    return self;
}
//重载drawTextInRect方法
-(void) drawTextInRect:(CGRect)rect {
    return [super drawTextInRect:UIEdgeInsetsInsetRect(rect, self.insets)];
}

@end
