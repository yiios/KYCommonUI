//
//  Ky_GradeLabel.h
//  我的评分
//
//  Created by alangavin on 16/9/1.
//  Copyright © 2016年 alangavin. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "Ky_GradeModel.h"
@interface Ky_GradeLabel : UILabel

@property (nonatomic, assign) int index;//第几个label
@property (nonatomic, strong) NSString *placeholder;//默认字体
//@property (nonatomic, assign) int tag;
/**
 *  存放模型
 */
//@property (nonatomic, strong) Ky_GradeModel *gradeModel;
/**
 *  自适应高度
 *
 *  @param size 最大size限制,0,无限制
 */
- (CGSize)boundingRectWithSize:(CGSize)size;
-(CGRect)ky_LabelFrameSizeofFite:(CGRect)frame;//自适应,Y设置为0;
-(CGRect)ky_LabelFrameSizeofFiteWid:(CGRect)frame;
//用于设置Label的内边距
@property(nonatomic) UIEdgeInsets insets;
//初始化方法一
-(id) initWithFrame:(CGRect)frame andInsets: (UIEdgeInsets) insets;
//初始化方法二
-(id) initWithInsets: (UIEdgeInsets) insets;

@end
