//
//  CountLabel.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/11/10.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "CountLabel.h"

@implementation CountLabel


-(instancetype)initWithFrame:(CGRect)frame
{
    
    self = [super initWithFrame:frame];
    
    //    [self setUpViews];
    
    return self;
}


- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    self.backgroundColor = [UIColor clearColor];
    self.textColor = [UIColor redColor];
    self.textAlignment = NSTextAlignmentRight;
    self.font = [UIFont systemFontOfSize:13];
    
    
}


@end
