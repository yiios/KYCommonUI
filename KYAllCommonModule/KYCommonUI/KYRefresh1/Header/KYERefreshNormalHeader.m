//
//  KYERefreshNormalHeader.m
//  kyExpress_Internal
//
//  Created by caichaosen on 2017/5/18.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "KYERefreshNormalHeader.h"
#import "NetStatusView.h"
#import "UIScrollView+KYRefresh.h"
#import "MJRefreshComponent.h"
#import "Tools.h"

#import "UIScrollView+KYERefresh.h"

@interface KYERefreshNormalHeader ()
// 文字
@property (weak, nonatomic) UILabel *label;
// 上下箭头
@property (weak, nonatomic) UIImageView *arrowView;
// 小人跑
@property (weak, nonatomic) UIImageView *refreshingView;
// 状态图标
@property (weak, nonatomic) UIImageView *finalView;
// 记录上一个状态
@property (nonatomic, assign) KYERefreshState headOldState;
@end

@implementation KYERefreshNormalHeader {
   __weak UITableView *_tableView;
}
#pragma mark 在这里做一些初始化配置（比如添加子控件）
- (void)prepare
{
    [super prepare];
    
    // 添加label
    UILabel *label = [[UILabel alloc] init];
    label.textColor = [UIColor lightGrayColor];
    label.font = [UIFont systemFontOfSize:14];
    label.textAlignment = NSTextAlignmentCenter;
    [self addSubview:label];
    self.label = label;
    
    // 箭头
    UIImageView *arrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pull_down"]];
    arrowView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:arrowView];
    self.arrowView = arrowView;
    
    // 设置正在刷新状态的动画图片
    NSMutableArray *refreshingImages = [NSMutableArray array];
    for (NSUInteger i = 1; i<=8; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"dropdown_anim__000%zd", i]];
        [refreshingImages addObject:image];
    }
    UIImageView *refreshingImage = [[UIImageView alloc] init];
    refreshingImage.animationImages = refreshingImages;
    refreshingImage.animationDuration = 0.75;
    refreshingImage.hidden = YES;
    self.refreshingView = refreshingImage;
    [self addSubview:refreshingImage];
    
    
    // 最终状态显示图片
    UIImageView *finalView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"done"]];
    finalView.hidden = YES;
    [self addSubview:finalView];
    self.finalView = finalView;
}

#pragma mark 在这里设置子控件的位置和尺寸
- (void)placeSubviews {
    [super placeSubviews];
    
    self.label.frame = self.bounds;
    
    self.arrowView.center = CGPointMake(self.mj_w * 0.5 - 75, self.mj_h * 0.5);
    
    self.refreshingView.bounds = CGRectMake(0, 0, 50, 50);
    self.refreshingView.center = CGPointMake(self.mj_w * 0.5 - 75, self.mj_h * 0.5);
    
    self.finalView.center = CGPointMake(self.mj_w * 0.5 - 45, self.mj_h * 0.5);
}
#pragma mark - beginRefresh
- (void)beginRefreshingWithDataCount:(NSUInteger)dataCount {
    if (dataCount == 0) {
        [self showHudOnTableView];
        [self executeRefreshingCallback];
    } else {
        [super beginRefreshing];
    }
}
- (void)beginRefreshing {
    [self getTableView];
    // 设置KYERefreshStateRefreshing状态
    if ((self.scrollView.getTotalDataCount == 0) &&
        self.state == KYERefreshStateIdle) {
        [self showHudOnTableView];
        [self executeRefreshingCallback];
    } else {
        [super beginRefreshing];
    }
}
- (void)beginBlocking {
    [self getTableView];
    [self showHudOnTableView];
    //    [Tools showMBProgressHUDAddView:_tableView.superview];
    [self executeRefreshingCallback];
}
// 删除无数据水印再跑小人
- (void)showHudOnTableView {
    
    [_tableView kye_removeNodataView];
    [Tools showMBProgressHUDAddView:_tableView.superview];
}
#pragma mark - 单纯跑小人
- (void)runExpressMan {
    if (!_tableView)    [self getTableView];
    [self showHudOnTableView];
    //    [Tools showMBProgressHUDAddView:_tableView.superview];
}
#pragma mark 监听控件的刷新状态
- (void)setState:(KYERefreshState)state {
    
    KYERefreshCheckState;
    
    // 记录oldState
    self.headOldState = oldState;
    
    switch (state) {
        case KYERefreshStateIdle: {
            [UIView animateWithDuration:MJRefreshFastAnimationDuration animations:^{
                self.arrowView.transform = CGAffineTransformIdentity;
            }];
            self.refreshingView.hidden = NO;
            self.arrowView.hidden = YES;
            if (oldState == KYERefreshStateNoMoreData ||
                oldState == KYERefreshStateNoNetwork) {
                // 延迟隐藏
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    self.finalView.hidden = YES;
                });
            } else {
                self.finalView.hidden = NO;
            }
            //  延迟小人的消失
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.refreshingView stopAnimating];
            });
        }
            break;
            
        case KYERefreshStateDraging: {
            self.label.text = @"下拉刷新";
            [UIView animateWithDuration:MJRefreshFastAnimationDuration animations:^{
                self.arrowView.transform = CGAffineTransformIdentity;
            }];
            self.refreshingView.hidden = YES;
            self.arrowView.hidden = NO;
            self.finalView.hidden = YES;
            [self.refreshingView stopAnimating];
            self.label.hidden = NO;
        }
            break;
            
        case KYERefreshStatePulling: {
            self.label.text = @"释放立即刷新";
            [UIView animateWithDuration:MJRefreshFastAnimationDuration animations:^{
                self.arrowView.transform = CGAffineTransformMakeRotation(0.0001-M_PI);
            }];
            self.label.hidden = NO;
            self.refreshingView.hidden = YES;
            self.arrowView.hidden = NO;
            self.finalView.hidden = YES;
            [self.refreshingView stopAnimating];
        }
            break;
        case KYERefreshStateRefreshing:
            self.label.text = @"正在刷新...";
            
            self.label.hidden = NO;
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = NO;
            self.finalView.hidden = YES;
            [self.refreshingView startAnimating];
            break;
            
        case KYERefreshStateSuccess:
            
            self.label.text = @"刷新成功";
            
            self.label.hidden = NO;
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = YES;
            self.finalView.hidden = NO;
            self.finalView.image = [UIImage imageNamed:@"done"];
            [self.refreshingView stopAnimating];
            break;
            
        case KYERefreshStateFail:
            self.label.text = @"刷新失败";
            
            self.label.hidden = NO;
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = YES;
            self.finalView.hidden = NO;
            self.finalView.image = [UIImage imageNamed:@"failure"];
            [self.refreshingView stopAnimating];
            break;
        case KYERefreshStateNoNetwork:
            self.label.text = @"正在刷新...";
            
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = NO;
            self.finalView.hidden = YES;
            self.finalView.image = [UIImage imageNamed:@"failure"];
            [self.refreshingView startAnimating];
            
            self.label.hidden = NO;
            break;
        case KYERefreshStateBeyondNetwork:   // 超时
            self.label.text = @"刷新失败";
            
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = YES;
            self.finalView.hidden = NO;
            self.finalView.image = [UIImage imageNamed:@"failure"];
            [self.refreshingView stopAnimating];
            
            self.label.hidden = NO;
            break;
            
        case KYERefreshStateNoResponse:
            //            self.label.text = kNetError;
            self.label.text = @"刷新失败";
            
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = YES;
            self.finalView.hidden = NO;
            self.finalView.image = [UIImage imageNamed:@"failure"];
            [self.refreshingView stopAnimating];
            
            self.label.hidden = NO;
            break;
        case KYERefreshStateNoMoreData:
            /** LQ要无数据情况下全部显示刷新成功 */
            self.finalView.hidden = NO;
            self.label.text = @"刷新成功";
            self.finalView.image = [UIImage imageNamed:@"done"];
//            if (self.scrollView.getTotalDataCount == 0) {
//                self.label.text = @"刷新成功";
//                self.finalView.image = [UIImage imageNamed:@"done"];
//            } else {
//                self.label.text = @"刷新成功";
//                self.finalView.image = [UIImage imageNamed:@"failure"];
//                self.finalView.hidden = YES;
//            }
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = YES;
            [self.refreshingView stopAnimating];
            self.label.hidden = NO;
            break;
        default:
            break;
    }
}
- (void)endRefreshingFailureWithError:(NSError *)error {
    // 判断是没有网络还是链接超时，还是服务器出错
    /*
     -1009 没有网络   -1001 网络超时 -1004 无法连接服务器
     -1011 坏的服务器响应
     */
    if (error.code == -1009) {
        [self endRefreshingWithState:KYERefreshStateNoNetwork];
    } else if (error.code == -1001 ||
               error.code == -1004) {
        [self endRefreshingWithState:KYERefreshStateBeyondNetwork];
    } else {
        [self endRefreshingWithState:KYERefreshStateNoResponse];
    }
}
#pragma mark - 重写方法
- (void)endRefreshingWithState:(KYERefreshState)state {
    
    if (!_tableView) [self getTableView];
    [Tools hideMBProgressHUDView:_tableView.superview];
    
    
    if (self.state == state)    return;
    
    self.state = state;
    
    /*
     这里只控制tableHeaderView
     */
    switch (state) {
        case KYERefreshStateSuccess:        // 成功
            [self clearTableHeadView];
            break;
        case KYERefreshStateFail:           // 失败
            [self clearTableHeadView];
            break;
        case KYERefreshStateNoMoreData:     // 无更多数据
            [self clearTableHeadView];
            break;
        case KYERefreshStateNoNetwork:      // 无网络
            // 是否设置表头
            [self isSetTableHeadView];
            break;
        case KYERefreshStateNoResponse:     // 无响应
            // 是否设置表头
            [self isSetTableHeadView];
            break;
        case KYERefreshStateBeyondNetwork:  // 网络超时
            // 是否设置表头
            [self isSetTableHeadView];
            break;
        case KYERefreshStateStop:           // 停留
            
            break;
        default:
            break;
    }
    // 恢复成普通状态
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.35 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self endRefreshing];
    });
}

-(void)endRefreshing {
    // 隐藏小人跑
    if (!_tableView) [self getTableView];
    [Tools hideMBProgressHUDView:_tableView.superview];
    // 恢复普通状态
    [super endRefreshing];
}
#pragma mark - 获取tableView
- (void)getTableView {
    //获取表视图
    if ([self.scrollView isKindOfClass:[UITableView class]]) {
        _tableView = (UITableView *)self.scrollView;
    }
}

- (void)isSetTableHeadView {
    if ([_tableView getTotalDataCount] != 0) {
        [self setTableHeadView];
    } else {
        [self clearTableHeadView];
    }
}
#pragma mark - 设置表头
- (void)setTableHeadView {
    NetStatusView *view = [[NetStatusView alloc] initWithFrame:CGRectMake(0, -1, 330, 40)];
    [view setNetErrorStyle];
    
    _tableView.tableHeaderView = view;
    
    //    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    //
    //    });
}
#pragma mark - 清除表头
- (void)clearTableHeadView {
    _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KSCREEN_W, CGFLOAT_MIN)];
}
//#pragma mark - 禁用和开启滚动
//- (void)EnabledScrollWithState:(KYERefreshState)state {
//    self.scrollView.scrollEnabled = YES;
//    dispatch_async(dispatch_get_main_queue(), ^{
//        if (self.scrollView.getTotalDataCount == 0) {
//            self.scrollView.scrollEnabled = NO;
//        }
//    });
//}
//#pragma mark - 是否设置无数据水印
//- (void)header_setNoDataView {
//
//    //设置无数据视图
//    if (self.state != KYRefreshStateNoNetwork  &&
//        self.state != KYRefreshStateNoResponse &&
//        self.state != KYRefreshStateBeyondNetwork) {
//        // 清除表头
//        [self clearTableHeadView];
//
//        if ([_tableView getTotalDataCount] == 0) {
//            [_tableView setNoData]; // 无数据视图
//        } else {
//            [_tableView hideStatusView];
//        }
//    }
//}

@end
