//
//  KYERefreshHeader.h
//  kyExpress_Internal
//
//  Created by caichaosen on 2017/5/18.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "KYRefreshComponent.h"
typedef  void(^KYEHeaderDidFinish)(void);

@interface KYERefreshHeader : KYRefreshComponent

/** 创建header */
+ (instancetype)headerWithRefreshingBlock:(MJRefreshComponentRefreshingBlock)refreshingBlock;
/** 创建header */
+ (instancetype)headerWithRefreshingTarget:(id)target refreshingAction:(SEL)action;

/** 这个key用来存储上一次下拉刷新成功的时间 */
@property (copy, nonatomic) NSString *lastUpdatedTimeKey;
/** 上一次下拉刷新成功的时间 */
@property (strong, nonatomic, readonly) NSDate *lastUpdatedTime;
/** 忽略多少scrollView的contentInset的top */
@property (assign, nonatomic) CGFloat ignoredScrollViewContentInsetTop;
/** 头部动画完成回调 */
@property (nonatomic, copy) KYEHeaderDidFinish headerDidFinishBlock;

/** 结束刷新,刷入对应状态显示对应提示 */
- (void)endRefreshingWithState:(KYERefreshState)state;
- (void)endRefreshingFailureWithError:(NSError *)error;
/** 开始跑小人，不调用任何方法 */
- (void)runExpressMan;
@end
