//
//  KYERefreshHeader.m
//  kyExpress_Internal
//
//  Created by caichaosen on 2017/5/18.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "KYERefreshHeader.h"
#import "MJRefreshComponent.h"

@interface KYERefreshHeader ()
@property (assign, nonatomic) CGFloat insetTDelta;
@end

@implementation KYERefreshHeader

#pragma mark - 构造方法
+ (instancetype)headerWithRefreshingBlock:(MJRefreshComponentRefreshingBlock)refreshingBlock
{
    KYERefreshHeader *cmp = [[self alloc] init];
    cmp.refreshingBlock = refreshingBlock;
    cmp.backgroundColor = [UIColor clearColor];
    return cmp;
}
+ (instancetype)headerWithRefreshingTarget:(id)target refreshingAction:(SEL)action
{
    KYERefreshHeader *cmp = [[self alloc] init];
    [cmp setRefreshingTarget:target refreshingAction:action];
    cmp.backgroundColor = [UIColor clearColor];
    return cmp;
}

#pragma mark - 覆盖父类的方法
- (void)prepare
{
    [super prepare];
    
    // 设置key
    self.lastUpdatedTimeKey = MJRefreshHeaderLastUpdatedTimeKey;
    
    // 设置高度
    self.mj_h = MJRefreshHeaderHeight;
}

- (void)placeSubviews
{
    [super placeSubviews];
    
    // 设置y值(当自己的高度发生改变了，肯定要重新调整Y值，所以放到placeSubviews方法中设置y值)
    self.mj_y = - self.mj_h - self.ignoredScrollViewContentInsetTop;
}

- (void)scrollViewContentOffsetDidChange:(NSDictionary *)change
{
    [super scrollViewContentOffsetDidChange:change];
    
    
    // 在刷新的refreshing状态
    if (self.state == KYERefreshStateRefreshing) {
        if (self.window == nil) return;
        //FIXME: 正在刷新状态的停留解决
        /*
         如果下拉了，insetT = - self.scrollView.mj_offsetY,即scrollView的contentOffset.y的正值
         如果没有下拉，insetT等于原始的顶部距离（即0）
         */
        CGFloat insetT = - self.scrollView.mj_offsetY > _scrollViewOriginalInset.top ? - self.scrollView.mj_offsetY : _scrollViewOriginalInset.top;
        /*
         是否下拉到完全展示headerView(可以刷新)
         如果可以刷新，insetT= 自身高度+原始顶部间距
         */
        insetT = insetT > self.mj_h + _scrollViewOriginalInset.top ? self.mj_h + _scrollViewOriginalInset.top : insetT;
        self.scrollView.mj_insetT = insetT;
        /*
         该值为负，计算结果作为合拢的距离
         */
        self.insetTDelta = _scrollViewOriginalInset.top - insetT;
        return;
    }
    
    // 跳转到下一个控制器时，contentInset可能会变
    _scrollViewOriginalInset = self.scrollView.contentInset;
    
    // 当前的contentOffset
    CGFloat offsetY = self.scrollView.mj_offsetY;
    // 头部控件刚好出现的offsetY
    CGFloat happenOffsetY = - self.scrollViewOriginalInset.top;
    
    // 如果是向上滚动到看不见头部控件，直接返回
    // >= -> >
    if (offsetY > happenOffsetY) return;
    
    // 普通 和 即将刷新 的临界点
    CGFloat normal2pullingOffsetY = happenOffsetY - self.mj_h;
    CGFloat pullingPercent = (happenOffsetY - offsetY) / self.mj_h;
    
    if (self.scrollView.isDragging) { // 如果正在拖拽
        self.pullingPercent = pullingPercent;
        if (self.state == KYERefreshStateIdle) {
            self.state = KYERefreshStateDraging;
        } else if (self.state == KYERefreshStateDraging && offsetY < normal2pullingOffsetY) {
            // 转为即将刷新状态
            self.state = KYERefreshStatePulling;
        } else if (self.state == KYERefreshStatePulling && offsetY >= normal2pullingOffsetY) {
            // 转为普通状态
            self.state = KYERefreshStateIdle;
        }
    } else if (self.state == KYERefreshStatePulling) {// 即将刷新 && 手松开
        // 开始刷新，不调用beginRefreshing，状态就不会切换到MJRefreshStateRefreshing
        [self beginRefreshing];
    } else if (pullingPercent < 1) {
        self.pullingPercent = pullingPercent;
    }
}
/*
 在滚动的方法做了insetTDelta的合拢距离的计算，
 条件是state == MJRefreshStateRefreshing
 */
/*
 在两个地方做了停留，一个是setState,一个是滚动的方法，
 条件都是state == MJRefreshStateRefreshing
 */
- (void)setState:(KYERefreshState)state
{
    KYERefreshCheckState
    /*
     1.上拉刷洗成功的时候，header也走了合拢的方法，insetTop算不对了
     */
    // 根据状态做事情
    if (state == KYERefreshStateIdle) {
        // 由这些状态 切回 普通状态才可以合拢
        if (oldState != KYERefreshStateRefreshing       &&
            oldState != KYERefreshStateSuccess          &&
            oldState != KYERefreshStateFail             &&
            oldState != KYERefreshStateNoMoreData       &&
            oldState != KYERefreshStateNoNetwork        &&
            oldState != KYERefreshStateBeyondNetwork    &&
            oldState != KYERefreshStateNoResponse) return;
        
        // 保存刷新时间
//        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:self.lastUpdatedTimeKey];
//        [[NSUserDefaults standardUserDefaults] synchronize];
        
        // 恢复inset和offset
        [UIView animateWithDuration:MJRefreshSlowAnimationDuration animations:^{
            // insetTDelta记录了要合拢的距离，恢复合拢
            //            self.scrollView.mj_insetT += self.insetTDelta;
            self.scrollView.mj_insetT = 0;
            // 自动调整透明度
            if (self.isAutomaticallyChangeAlpha) self.alpha = 0.0;
        } completion:^(BOOL finished) {
            self.pullingPercent = 0.0;
            // 利用完self.insetTDelta之后，置为0，反正每一次下拉刷新都会计算一次
            self.insetTDelta += -self.insetTDelta;
            /** 动画完成回调出去 */
            if (self.headerDidFinishBlock) {
                _headerDidFinishBlock();
            }
        }];
    } else if (state == KYERefreshStateRefreshing) {
        
        [UIView animateWithDuration:MJRefreshFastAnimationDuration delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
            // 增加滚动区域
            CGFloat top = self.scrollViewOriginalInset.top + self.mj_h;
            self.scrollView.mj_insetT = top;
            
            // 设置滚动位置，下拉offsetY为负
            self.scrollView.mj_offsetY = - top;
        } completion:^(BOOL finished) {
            // 不要做刷新的拦截
            [self executeRefreshingCallback];
        }];
        
        //        [UIView animateWithDuration:MJRefreshFastAnimationDuration animations:^{
        //            // 增加滚动区域
        //            CGFloat top = self.scrollViewOriginalInset.top + self.mj_h;
        //            self.scrollView.mj_insetT = top;
        //
        //            // 设置滚动位置，下拉offsetY为负
        //            self.scrollView.mj_offsetY = - top;
        //
        //            NSLog(@"headerheader%@",@(self.scrollView.userInteractionEnabled));
        //        } completion:^(BOOL finished) {
        //            // 不要做刷新的拦截
        //            [self executeRefreshingCallback];
        //        }];
    }
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
}

#pragma mark - 公共方法
- (void)endRefreshing
{
    // 恢复状态为MJRefreshStateIdle
    if ([self.scrollView isKindOfClass:[UICollectionView class]]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [super endRefreshing];
        });
    } else {
        [super endRefreshing];
    }
}

- (NSDate *)lastUpdatedTime
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:self.lastUpdatedTimeKey];
}
#pragma mark - 子类重写方法
- (void)endRefreshingWithState:(KYERefreshState)state{}
- (void)endRefreshingFailureWithError:(NSError *)error{}

@end
