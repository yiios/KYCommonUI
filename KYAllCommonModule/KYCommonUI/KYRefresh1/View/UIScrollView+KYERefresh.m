//
//  UIScrollView+KYERefresh.m
//  kyExpress_Internal
//
//  Created by caichaosen on 2017/5/18.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "UIScrollView+KYERefresh.h"
#import "UIScrollView+KYRefresh.h"
#import "KYERefreshHeader.h"
#import "UITableView+NetDataStatus.h"
#import "KYRefreshHeader.h"
#import "KYRefreshFooter.h"
#import "UIView+NetDataStatus.h"

static char const * const KYERefreshHeaderKey = "KYERefreshHeaderKey";
static char const * const KYERefreshFooterKey = "KYERefreshFooterKey";

@implementation UIScrollView (KYERefresh)
-(KYERefreshHeader *)kye_header
{
    return objc_getAssociatedObject(self, &KYERefreshHeaderKey);
}

-(KYERefreshFooter *)kye_footer
{
    return objc_getAssociatedObject(self, &KYERefreshFooterKey);
}

-(void)setKye_header:(KYERefreshHeader *)kye_header
{
    if (kye_header != self.kye_header) {
        // 删除旧的，添加新的
        [self.kye_header removeFromSuperview];
        [self insertSubview:kye_header atIndex:0];
        
        // 存储新的
        [self willChangeValueForKey:@"kye_header"]; // KVO
        objc_setAssociatedObject(self, &KYERefreshHeaderKey,
                                 kye_header, OBJC_ASSOCIATION_ASSIGN);
        [self didChangeValueForKey:@"kye_header"]; // KVO
    }
}


-(void)setKye_footer:(KYERefreshFooter *)kye_footer
{
    if (kye_footer != self.kye_footer) {
        // 删除旧的，添加新的
        [self.kye_footer removeFromSuperview];
        [self insertSubview:kye_footer atIndex:0];
        
        // 存储新的
        [self willChangeValueForKey:@"kye_footer"]; // KVO
        objc_setAssociatedObject(self, &KYERefreshFooterKey,
                                 kye_footer, OBJC_ASSOCIATION_ASSIGN);
        [self didChangeValueForKey:@"kye_footer"]; // KVO
    }
}
#pragma mark - other
- (NSInteger)getTotalDataCount
{
    NSInteger totalCount = 0;
    if ([self isKindOfClass:[UITableView class]]) {
        UITableView *tableView = (UITableView *)self;
        
        for (NSInteger section = 0; section<tableView.numberOfSections; section++) {
            totalCount += [tableView numberOfRowsInSection:section];
        }
    } else if ([self isKindOfClass:[UICollectionView class]]) {
        UICollectionView *collectionView = (UICollectionView *)self;
        
        for (NSInteger section = 0; section<collectionView.numberOfSections; section++) {
            totalCount += [collectionView numberOfItemsInSection:section];
        }
    }
    return totalCount;
}
#pragma mark - 整合
- (void)finishAllRefreshingWithState:(KYERefreshState)state DataCount:(NSUInteger)dataCount {
    [self.kye_header endRefreshingWithState:state];
    [self.kye_footer endRefreshingWithState:state];
    [self endRefreshingWithState:state DataCount:dataCount];
}
- (void)finishAllRefreshingFailureWithError:(NSError *)error DataCount:(NSUInteger)dataCount {
    [self.kye_header endRefreshingFailureWithError:error];
    [self.kye_footer endRefreshingFailureWithError:error];
    [self endRefreshingFailureWithError:error DataCount:dataCount];
}
#pragma mark - 在tableView上添加视图的方法
/**
 处理成功、失败、无更多数据三种状态
 
 @param state 传KYERefreshStateSuccess
 KYERefreshStateFail
 KYERefreshStateNoMoreData
 */
- (void)endRefreshingWithState:(KYERefreshState)state DataCount:(NSUInteger)dataCount {
    
    switch (state) {
        case KYERefreshStateSuccess:
            [self p_refreshSuccessWithState:state DataCount:dataCount];
            break;
        case KYERefreshStateFail:
            [self p_refreshFailureWithState:state DataCount:dataCount];
            break;
        case KYERefreshStateNoMoreData:
            [self p_refreshNoMoreDataWithState:state DataCount:dataCount];
            break;
        default:
            break;
    }
}

/**
 处理无网、超时，无响应三种状态
 
 @param error error
 */
- (void)endRefreshingFailureWithError:(NSError *)error DataCount:(NSUInteger)dataCount {
    
    // 判断是没有网络、链接超时，还是服务器出错
    NSInteger errorType = error.code;
    switch (errorType) {
        case -1009:     // 没有网络
            [self p_refreshNoNetworkWithState:KYERefreshStateNoNetwork DataCount:dataCount];
            break;
        case -1001:     // 网络超时
        case -1004:     // 无法连接服务器
            [self p_refreshOverTimeWithState:KYERefreshStateBeyondNetwork DataCount:dataCount];
            break;
        default:        // 坏的服务器响应
            [self p_refreshNoResponseWithState:KYERefreshStateNoResponse DataCount:dataCount];
            break;
    }
}
/**
 tableView设置无数据视图,跟KYERefresh一起使用就不用考虑移除,调用时tableView的frame需被正确地设置
 */
- (void)kye_setNodataView {
    [self showNodataViewWithFrame:self.frame];
}
/**
 tableView移除无数据视图,跟KYERefresh一起使用就不用考虑移除
 */
- (void)kye_removeNodataView {
    [self hideStatusView];
}
#pragma mark - private
// 成功
- (void)p_refreshSuccessWithState:(KYERefreshState)state DataCount:(NSUInteger)dataCount {
    
    // 成功直接移除reloadView
    [self hideStatusView];
    self.scrollEnabled  = YES;
}
// 失败
- (void)p_refreshFailureWithState:(KYERefreshState)state DataCount:(NSUInteger)dataCount {
    
    // 判断是否要添加无数据视图
    [self isSetNodataViewWitdDataCount:dataCount];
    // 保险判断
    if (dataCount > 0) {
        self.scrollEnabled  = YES;
    }
}
// 无更多数据
- (void)p_refreshNoMoreDataWithState:(KYERefreshState)state DataCount:(NSUInteger)dataCount {
    
    // 判断是否要添加无数据视图
    [self isSetNodataViewWitdDataCount:dataCount];
    // 保险判断
    if (dataCount > 0) {
        self.scrollEnabled  = YES;
    }
}
// 无网
- (void)p_refreshNoNetworkWithState:(KYERefreshState)state DataCount:(NSUInteger)dataCount {
    // 添加视图
    [self isSetNetErrorView];
    // 保险判断
    if (dataCount > 0) {
        self.scrollEnabled  = YES;
    }
}
// 超时
- (void)p_refreshOverTimeWithState:(KYERefreshState)state DataCount:(NSUInteger)dataCount {
    // 添加视图
    [self isSetNetErrorView];
    // 保险判断
    if (dataCount > 0) {
        self.scrollEnabled  = YES;
    }
}
// 无响应
- (void)p_refreshNoResponseWithState:(KYERefreshState)state DataCount:(NSUInteger)dataCount {
    // 添加视图
    [self isShowNoResponseView];
    // 保险判断
    if (dataCount > 0) {
        self.scrollEnabled  = YES;
    }
}
#pragma mark - 是否设置无网 / 超时视图
- (void)isSetNetErrorView {
    if ([self getTotalDataCount] == 0) {
        [self showNetErrorViewWithFrame:self.frame]; // 无法连接服务器
        self.scrollEnabled = NO;
    } else {
        self.scrollEnabled = YES;
        [self hideStatusView];
    }
}
#pragma mark - 是否设置无响应视图
- (void)isShowNoResponseView {
    if ([self getTotalDataCount] == 0) {
        [self showNoResponseViewWithFrame:self.frame]; // 服务器无法响应
        self.scrollEnabled = NO;
    } else {
        self.scrollEnabled = YES;
        [self hideStatusView];
    }
}
#pragma mark - 是否设置无数据视图
- (void)isSetNodataViewWitdDataCount:(NSUInteger)dataCount {
    
    if ([self getTotalDataCount] == 0 || dataCount == 0) {
        // 老覃要先小人消失，再出现无数据视图
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self showNodataViewWithFrame:self.frame];// 无数据视图
        });
        self.scrollEnabled = NO;
    } else {
        [self hideStatusView];
        self.scrollEnabled = YES;
    }
}
//#pragma mark - 清除表头
//- (void)removeTableHeadView {
//    
//    if ([self isKindOfClass:[UITableView class]]) {
//        UITableView *_tableView = (UITableView *)self;
//        _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KSCREEN_W, CGFLOAT_MIN)];
//    }
//}
//#pragma mark - 是否可以滚动
//- (void)isEnableScrollingWithState:(KYERefreshState)state {
//    self.scrollEnabled = YES;
//    dispatch_async(dispatch_get_main_queue(), ^{
//        if (state == KYERefreshStateNoNetwork   ||
//            state == KYERefreshStateNoResponse  ||
//            state == KYERefreshStateBeyondNetwork) {
//            self.scrollEnabled = !([self getTotalDataCount] == 0);
//            [self clearTableHeadView];
//        }
//    });
//}
@end
