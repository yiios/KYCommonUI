//
//  UIScrollView+KYERefresh.h
//  kyExpress_Internal
//
//  Created by caichaosen on 2017/5/18.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KYERefreshNormalHeader.h"
#import "KYERefreshNormalFooter.h"


@interface UIScrollView (KYERefresh)
/** 下拉刷新控件 */
@property (strong, nonatomic) KYERefreshHeader *kye_header;
/** 上拉拉刷新控件 */
@property (strong, nonatomic) KYERefreshFooter *kye_footer;

#pragma mark - other
- (NSInteger)getTotalDataCount;

@property (copy, nonatomic) void (^mj_reloadDataBlock)(NSInteger totalDataCount);

#pragma mark - 整合结束刷新的三个方法
/**
 整合结束刷新的三个方法
 @param state 状态
 @param dataCount 数据源的数量，需确保正确
 */
- (void)finishAllRefreshingWithState:(KYERefreshState)state DataCount:(NSUInteger)dataCount;
/**
 整合结束刷新的三个方法
 @param error error
 @param dataCount 数据源的数量，需确保正确
 */
- (void)finishAllRefreshingFailureWithError:(NSError *)error DataCount:(NSUInteger)dataCount;
#pragma mark - 在tableView上添加视图的方法
/**
 处理成功、失败、无更多数据三种状态

 @param state 传KYERefreshStateSuccess
                KYERefreshStateFail
                KYERefreshStateNoMoreData
 @param dataCount 数据源的数量，需确保正确
 */
- (void)endRefreshingWithState:(KYERefreshState)state DataCount:(NSUInteger)dataCount;

/**
 处理无网、超时，无响应三种状态

 @param error error
 @param dataCount 数据源的数量，需确保正确
 */
- (void)endRefreshingFailureWithError:(NSError *)error DataCount:(NSUInteger)dataCount;

/**
 tableView设置无数据视图,跟KYERefresh一起使用就不用考虑移除,调用时tableView的frame需被正确地设置
 */
- (void)kye_setNodataView;

/**
 tableView移除无数据视图,跟KYERefresh一起使用就不用考虑移除
 */
- (void)kye_removeNodataView;

@end
