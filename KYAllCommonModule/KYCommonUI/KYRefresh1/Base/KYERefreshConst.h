//
//  KYERefreshConst.h
//  kyExpress_Internal
//
//  Created by caichaosen on 2017/5/26.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


// 状态检查
#define KYERefreshCheckState \
KYERefreshState oldState = self.state; \
if (state == oldState) return; \
[super setState:state];


#define logSize(_tableView) NSLog(@"contentInset:%@--contentSize:%@--frame:%@",NSStringFromUIEdgeInsets(_tableView.contentInset),NSStringFromCGSize(_tableView.contentSize),NSStringFromCGRect(_tableView.frame));

#define logState(type,state) switch (state) { \
case KYERefreshStateIdle:                \
NSLog(@"%@普通闲置状态",type);               \
break;                              \
case KYERefreshStateDraging:     \
NSLog(@"%@下拉状态",type);         \
break;                  \
case KYERefreshStatePulling:         \
NSLog(@"%@普通松开就可以进行刷新的状态",type);       \
break;                          \
case KYERefreshStateRefreshing:      \
NSLog(@"%@正在刷新中的状态",type);         \
break;                  \
case KYERefreshStateWillRefresh:     \
NSLog(@"%@即将刷新的状态",type);      \
break;              \
case KYERefreshStateNoMoreData:          \
NSLog(@"%@所有数据加载完毕，没有更多的数据了",type);        \
break;              \
case KYERefreshStateSuccess:     \
NSLog(@"%@请求成功的状态",type);         \
break;              \
case KYERefreshStateFail:        \
NSLog(@"%@请求失败的状态",type);             \
break;              \
case KYERefreshStateNoNetwork:       \
NSLog(@"%@无网络的状态",type);              \
break;                  \
case KYERefreshStateNoResponse:      \
NSLog(@"%@无响应状态",type);                \
break;              \
case KYERefreshStateBeyondNetwork:      \
NSLog(@"%@网络超时状态",type);                \
break;              \
case KYERefreshStateStop:      \
NSLog(@"%@停留状态",type);                \
break;              \
}
