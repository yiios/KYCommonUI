//
//  KYERefreshFooter.h
//  kyExpress_Internal
//
//  Created by caichaosen on 2017/5/19.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "KYRefreshComponent.h"

@interface KYERefreshFooter : KYRefreshComponent
/** 创建footer */
+ (instancetype)footerWithRefreshingBlock:(MJRefreshComponentRefreshingBlock)refreshingBlock;
/** 创建footer */
+ (instancetype)footerWithRefreshingTarget:(id)target refreshingAction:(SEL)action;

/** 提示没有更多的数据 */
- (void)endRefreshingWithNoMoreData;

/*刷新出错*/
- (void)endRefreshingWithError;

- (void)noticeNoMoreData MJRefreshDeprecated("使用endRefreshingWithNoMoreData");

/** 重置没有更多的数据（消除没有更多数据的状态） */
- (void)resetNoMoreData;

/** 忽略多少scrollView的contentInset的bottom */
@property (assign, nonatomic) CGFloat ignoredScrollViewContentInsetBottom;

/** 自动根据有无数据来显示和隐藏（有数据就显示，没有数据隐藏。默认是NO） */
@property (assign, nonatomic, getter=isAutomaticallyHidden) BOOL automaticallyHidden;

#pragma mark - 自定义方法
/** 结束刷新,刷入对应状态显示对应提示 */
- (void)endRefreshingWithState:(KYERefreshState)state;
- (void)endRefreshingFailureWithError:(NSError *)error;
#pragma mark - 自定义属性
/** 是否可以做上拉刷新的开关（YES就禁止上拉刷新，NO就可以上拉，默认是NO） */
@property (assign, nonatomic, getter=isProhibitFooter) BOOL prohibitFooter;
/** 是否可以自动做上拉刷新的开关（YES就禁止自动上拉刷新，NO就可以上拉，默认是NO） */
@property (assign, nonatomic, getter=isCloseAotuFooter) BOOL closeAotuFooter;
@end
