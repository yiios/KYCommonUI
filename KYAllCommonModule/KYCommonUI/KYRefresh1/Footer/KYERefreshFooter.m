//
//  KYERefreshFooter.m
//  kyExpress_Internal
//
//  Created by caichaosen on 2017/5/19.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "KYERefreshFooter.h"

@implementation KYERefreshFooter

#pragma mark - 构造方法
+ (instancetype)footerWithRefreshingBlock:(MJRefreshComponentRefreshingBlock)refreshingBlock
{
    KYERefreshFooter *cmp = [[self alloc] init];
    cmp.refreshingBlock = refreshingBlock;
    cmp.backgroundColor = [UIColor clearColor];
    return cmp;
}
+ (instancetype)footerWithRefreshingTarget:(id)target refreshingAction:(SEL)action
{
    KYERefreshFooter *cmp = [[self alloc] init];
    [cmp setRefreshingTarget:target refreshingAction:action];
    cmp.backgroundColor = [UIColor clearColor];
    return cmp;
}

#pragma mark - 重写父类的方法
- (void)prepare
{
    [super prepare];
    
    // 设置自己的高度
    self.mj_h = MJRefreshFooterHeight;
    
    // 默认不会自动隐藏
    self.automaticallyHidden = NO;
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    [super willMoveToSuperview:newSuperview];
    
    if (newSuperview) {
        // 监听scrollView数据的变化
        if ([self.scrollView isKindOfClass:[UITableView class]] || [self.scrollView isKindOfClass:[UICollectionView class]]) {
            [self.scrollView setMj_reloadDataBlock:^(NSInteger totalDataCount) {
                if (self.isAutomaticallyHidden) {
                    self.hidden = (totalDataCount == 0);
                }
            }];
        }
    }
}

#pragma mark - 自定义方法
/** 结束刷新,刷入对应状态显示对应提示 */
- (void)endRefreshingWithState:(KYERefreshState)state{}
- (void)endRefreshingFailureWithError:(NSError *)error{}

#pragma mark - 公共方法
-(void)beginRefreshing {
    [super beginRefreshing];
}
- (void)endRefreshingWithNoMoreData
{
    self.state = KYERefreshStateNoMoreData;
}

- (void)endRefreshingWithError
{
    self.state = KYERefreshStateFail;
}

-(void)setState:(KYERefreshState)state
{
    [super setState:state];
    
}

- (void)noticeNoMoreData
{
    [self endRefreshingWithNoMoreData];
}

- (void)resetNoMoreData
{
    self.state = KYERefreshStateIdle;
}


@end
