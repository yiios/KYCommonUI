//
//  KYEBaseRefreshFooter.m
//  kyExpress_Internal
//
//  Created by caichaosen on 2017/5/19.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "KYEBaseRefreshFooter.h"
#import "Tools.h"


@interface KYEBaseRefreshFooter ()
// 记录刷新前的tableView的items
@property (assign, nonatomic) NSInteger lastRefreshCount;
// 计算要合拢的距离
@property (assign, nonatomic) CGFloat lastBottomDelta;
@end

@implementation KYEBaseRefreshFooter

#pragma mark - 初始化
- (void)willMoveToSuperview:(UIView *)newSuperview
{
    [super willMoveToSuperview:newSuperview];
    
    [self scrollViewContentSizeDidChange:nil];
}

#pragma mark - 实现父类的方法
- (void)scrollViewContentOffsetDidChange:(NSDictionary *)change
{
    [super scrollViewContentOffsetDidChange:change];
    
    // 如果正在刷新，直接返回
    if (self.state == KYERefreshStateRefreshing) return;
    
    _scrollViewOriginalInset = self.scrollView.contentInset;
    
    // 如果内容不足一屏，直接返回
    if ([self heightForContentBreakView] < 0)    return;
    // 如果禁止上拉刷新，直接返回
    if (self.isProhibitFooter)                   return;
    
    /******************* 自动执行回调******************/
    if (!self.closeAotuFooter) {
        [self autoExecBlockWithChange:change];
    }
    /******************* 自动执行回调******************/
    
    
    // 当前的contentOffset
    CGFloat currentOffsetY = self.scrollView.mj_offsetY;
    // 尾部控件刚好出现的offsetY
    CGFloat happenOffsetY = [self happenOffsetY];
    // 如果是向下滚动到看不见尾部控件，直接返回
    if (currentOffsetY <= happenOffsetY) return;
    
    CGFloat pullingPercent = (currentOffsetY - happenOffsetY) / self.mj_h;
    
    // 如果已全部加载，仅设置pullingPercent，然后返回
    //    if (self.state == KYERefreshStateNoMoreData) {
    //        self.pullingPercent = pullingPercent;
    //        return;
    //    }
    // 普通 和 即将刷新 的临界点
    CGFloat normal2pullingOffsetY = happenOffsetY + self.mj_h;
    if (self.scrollView.isDragging) {
        self.pullingPercent = pullingPercent;
        if (self.state == KYERefreshStateIdle) {
            self.state = KYERefreshStateDraging;
        }
        
        if (self.state == KYERefreshStateDraging && currentOffsetY > normal2pullingOffsetY) {
            // 转为即将刷新状态
            self.state = KYERefreshStatePulling;
        } else if (self.state == KYERefreshStatePulling && currentOffsetY <= normal2pullingOffsetY) {
            // 转为普通状态
            self.state = KYERefreshStateIdle;
        }
    } else if (self.state == KYERefreshStatePulling) {// 即将刷新 && 手松开
        // 开始刷新
        [self beginRefreshing];
    } else if (self.state == KYERefreshStateNoMoreData &&
               currentOffsetY > normal2pullingOffsetY) {
        self.state = KYERefreshStateStop;
    } else if (pullingPercent < 1) {
        self.pullingPercent = pullingPercent;
    }
}

- (void)scrollViewContentSizeDidChange:(NSDictionary *)change
{
    [super scrollViewContentSizeDidChange:change];
    
    // 内容的高度
    CGFloat contentHeight = self.scrollView.mj_contentH + self.ignoredScrollViewContentInsetBottom;
    // 表格的高度
    CGFloat scrollHeight = self.scrollView.mj_h - self.scrollViewOriginalInset.top - self.scrollViewOriginalInset.bottom + self.ignoredScrollViewContentInsetBottom;
    // 设置位置和尺寸
    self.mj_y = MAX(contentHeight, scrollHeight);
}

- (void)setState:(KYERefreshState)state
{
    KYERefreshCheckState
    /******************* 停留 **********************/
    // 无更多数据状态->无更多数据状态下的停留和合拢
    if (self.state == KYERefreshStateStop &&
        oldState == KYERefreshStateNoMoreData) {
        [self stopWithState:KYERefreshStateStop];
        return;
    }
    /******************* 停留 **********************/
    
    // 根据状态来设置属性
    if (state == KYERefreshStateNoMoreData ||
        state == KYERefreshStateIdle ) {
        // 刷新完毕
        if (KYERefreshStateRefreshing == oldState ||
            KYERefreshStateSuccess    == oldState ||
            KYERefreshStateFail       == oldState ||
            KYERefreshStateNoNetwork  == oldState ||
            KYERefreshStateNoResponse == oldState ) {
            //MJRefreshSlowAnimationDuration
            CGFloat closureTime = 0.5;
            if (state == KYERefreshStateNoMoreData) {
                // 为了由刷新状态->无更多数据状态的暂缓回收
                closureTime = 1.0;
            }
            //合拢
            [UIView animateWithDuration:closureTime animations:^{
                
                //                self.scrollView.mj_insetB -= self.lastBottomDelta;
                self.scrollView.mj_insetB = 0;
                // 自动调整透明度
                if (self.isAutomaticallyChangeAlpha) self.alpha = 0.0;
            } completion:^(BOOL finished) {
                // 用完lastBottomDelta，置为0，会重新计算
                self.lastBottomDelta -= self.lastBottomDelta;
                self.pullingPercent = 0.0;
            }];
        }
        
        CGFloat deltaH = [self heightForContentBreakView];
        // 刚刷新完毕
        if (KYERefreshStateRefreshing == oldState && deltaH > 0 && self.scrollView.mj_totalDataCount != self.lastRefreshCount) {
            self.scrollView.mj_offsetY = self.scrollView.mj_offsetY;
        }
    } else if (state == KYERefreshStateRefreshing) {
        // 记录刷新前的数量
        self.lastRefreshCount = self.scrollView.mj_totalDataCount;
        
        [UIView animateWithDuration:MJRefreshFastAnimationDuration animations:^{
            CGFloat bottom = self.mj_h + self.scrollViewOriginalInset.bottom;
            CGFloat deltaH = [self heightForContentBreakView];
            if (deltaH < 0) { // 如果内容高度小于view的高度
                bottom -= deltaH;
            }
            self.lastBottomDelta = bottom - self.scrollView.mj_insetB;
            self.scrollView.mj_insetB = bottom;
            self.scrollView.mj_offsetY = [self happenOffsetY] + self.mj_h;
        } completion:^(BOOL finished) {
            // 可以在这里做无网络的判断，不走下面的回调，同时恢复状态
            if ([Tools isNoNetwork]) {
                [self endRefreshingWithState:KYERefreshStateNoNetwork];
            } else {
                [self executeRefreshingCallback];
            }
        }];
    }
}

#pragma mark - 公共方法
- (void)endRefreshing
{
    if ([self.scrollView isKindOfClass:[UICollectionView class]]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [super endRefreshing];
        });
    } else {
        [super endRefreshing];
    }
}

- (void)noticeNoMoreData
{
    if ([self.scrollView isKindOfClass:[UICollectionView class]]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [super noticeNoMoreData];
        });
    } else {
        [super noticeNoMoreData];
    }
}

#pragma mark - 私有方法
#pragma mark 获得scrollView的内容 超出 view 的高度
- (CGFloat)heightForContentBreakView
{
    CGFloat h = self.scrollView.frame.size.height - self.scrollViewOriginalInset.bottom - self.scrollViewOriginalInset.top;
    return self.scrollView.contentSize.height - h;
}

#pragma mark 刚好看到上拉刷新控件时的contentOffset.y
- (CGFloat)happenOffsetY
{
    CGFloat deltaH = [self heightForContentBreakView];
    if (deltaH > 0) {
        return deltaH - self.scrollViewOriginalInset.top;
    } else {
        return - self.scrollViewOriginalInset.top;
    }
}
#pragma mark - 自动执行回调
- (void)autoExecBlockWithChange:(NSDictionary *)change {
    // NSConcreteValue对象（NSValue）
    id new = change[@"new"];
    CGPoint newPoint = [new CGPointValue];
    CGFloat height = self.scrollView.frame.size.height;
    CGFloat contentOffsety = newPoint.y;
    CGFloat distance = self.scrollView.contentSize.height - height;
    CGFloat offDistance = distance- contentOffsety;
    
    if (offDistance <= -10) {   // 刚好到底部再偏移一点，降低敏感度
        // 拦截上拉刷新，如果内容不足一屏，就不能上拉刷新
        if ([self heightForContentBreakView] < 0)   return;
        // 监控触摸事件，手指触摸的时候，就不自动触发刷新
        if (self.scrollView.isTracking == YES)      return;
        // 一下几种情况不继续做自动刷新网络请求，页面会很卡顿
        if (self.state == KYERefreshStateNoMoreData ||
            self.state == KYERefreshStateStop       ||
            self.state == KYERefreshStateNoResponse ||
            self.state == KYERefreshStateNoNetwork  ||
            self.state == KYERefreshStateFail       ||
            self.state == KYERefreshStateSuccess)    return;
        if ([Tools isNoNetwork])                     return;
        // 为了有小人跑的动画，做刷新
        [self beginRefreshing];
    }
}
#pragma mark - 单纯做停留，不做网络请求
- (void)stopWithState:(KYERefreshState)state {
    
    [UIView animateWithDuration:MJRefreshFastAnimationDuration animations:^{
        CGFloat bottom = self.mj_h + self.scrollViewOriginalInset.bottom;
        CGFloat deltaH = [self heightForContentBreakView];
        if (deltaH < 0) { // 如果内容高度小于view的高度
            bottom -= deltaH;
        }
        self.lastBottomDelta = bottom - self.scrollView.mj_insetB;
        self.scrollView.mj_insetB = bottom;
        self.scrollView.mj_offsetY = [self happenOffsetY] + self.mj_h;
    } completion:^(BOOL finished) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.65 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            // 合拢
            [self closureWithState:state];
        });
    }];
}
#pragma mark - 合拢
- (void)closureWithState:(KYERefreshState)state {
    
    //合拢
    [UIView animateWithDuration:MJRefreshSlowAnimationDuration animations:^{
        
        //        self.scrollView.mj_insetB -= self.lastBottomDelta;
        self.scrollView.mj_insetB = 0;
        // 自动调整透明度
        if (self.isAutomaticallyChangeAlpha) self.alpha = 0.0;
        
    } completion:^(BOOL finished) {
        // 用完lastBottomDelta，置为0，刷新的时候会重新计算
        self.lastBottomDelta -= self.lastBottomDelta;
        self.pullingPercent = 0.0;
        // stop后恢复无更多数据，因为oldState是stop，所以不会重复合拢
        if (state == KYERefreshStateStop) {
            self.state = KYERefreshStateNoMoreData;
        }
    }];
}
@end
