//
//  KYERefreshNormalFooter.m
//  kyExpress_Internal
//
//  Created by caichaosen on 2017/5/19.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "KYERefreshNormalFooter.h"
#import "NetStatusView.h"
#import "UIScrollView+KYRefresh.h"

@interface KYERefreshNormalFooter ()
// 文字
@property (weak, nonatomic) UILabel *label;
// 箭头
@property (weak, nonatomic) UIImageView *arrowView;
// 小人跑
@property (weak, nonatomic) UIImageView *refreshingView;
// 状态图标
@property (weak, nonatomic) UIImageView *finalView;


@end

@implementation KYERefreshNormalFooter {
    __weak UITableView *_tableView;
}

- (void)beginRefreshing {
    [self getTableView];
    [super beginRefreshing];
}

#pragma mark 在这里做一些初始化配置（比如添加子控件）
- (void)prepare {
    [super prepare];
    
    // 添加label
    UILabel *label = [[UILabel alloc] init];
    label.textColor = [UIColor lightGrayColor];
    label.font = [UIFont systemFontOfSize:14];
    label.textAlignment = NSTextAlignmentCenter;
    [self addSubview:label];
    self.label = label;
    
    // 箭头
    UIImageView *arrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pull_up"]];
    arrowView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:arrowView];
    self.arrowView = arrowView;
    
    // 设置正在刷新状态的动画图片
    NSMutableArray *refreshingImages = [NSMutableArray array];
    for (NSUInteger i = 1; i<=8; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"dropdown_anim__000%zd", i]];
        [refreshingImages addObject:image];
    }
    UIImageView *refreshingImage = [[UIImageView alloc] init];
    [self addSubview:refreshingImage];
    refreshingImage.animationImages = refreshingImages;
    refreshingImage.animationDuration = 0.75;
    refreshingImage.hidden = YES;
    self.refreshingView = refreshingImage;
    
    // 最终状态显示图片
    UIImageView *finalView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"done"]];
    finalView.hidden = YES;
    [self addSubview:finalView];
    self.finalView = finalView;
    
    
    self.refreshingView.hidden = YES;
    self.finalView.hidden = YES;
    self.arrowView.hidden = YES;
}

#pragma mark 在这里设置子控件的位置和尺寸
- (void)placeSubviews
{
    [super placeSubviews];
    
    self.label.frame = self.bounds;
    
    self.arrowView.center = CGPointMake(self.mj_w * 0.5 - 75, self.mj_h * 0.5);
    
    self.refreshingView.bounds = CGRectMake(0, 0, 35, 35);
    self.refreshingView.center = CGPointMake(self.mj_w * 0.5 - 75, self.mj_h * 0.5);
    
    self.finalView.center = CGPointMake(self.mj_w * 0.5 - 60, self.mj_h * 0.5);
}
-(void)setState:(KYERefreshState)state {
    
    KYERefreshCheckState;
    
    switch (state) {
        case KYERefreshStateIdle:
            // getTotalDataCount为0，基本就不能上拉刷新了
            if (oldState == KYERefreshStateNoNetwork   ||
                oldState == KYERefreshStateNoResponse) {
                self.label.text = kNetError;
            } else {
                self.label.text = @"查看更多"; // 与安卓统一
            }
            self.arrowView.image = [UIImage imageNamed:@"pull_up"];
            [self.refreshingView stopAnimating];
            self.refreshingView.hidden = YES;
            self.finalView.hidden = YES;
            self.arrowView.hidden = YES;
            self.label.hidden  = NO;
            break;
            
        case KYERefreshStateDraging:
            self.label.text = @"查看更多";
            self.arrowView.image = [UIImage imageNamed:@"pull_up"];
            self.refreshingView.hidden = YES;
            self.arrowView.hidden = YES;
            self.finalView.hidden = YES;
            [self.refreshingView stopAnimating];
            
            self.label.hidden = NO;
            
            break;
            
        case KYERefreshStatePulling:
            self.label.text = @"查看更多";
            self.arrowView.image = [UIImage imageNamed:@"pull_down"];
            
            self.label.hidden = NO;
            self.refreshingView.hidden = YES;
            self.arrowView.hidden = YES;
            self.finalView.hidden = YES;
            [self.refreshingView stopAnimating];
            break;
        case KYERefreshStateRefreshing:

            self.label.text = @"加载中，请稍候...";
            self.label.hidden = NO;
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = NO;
            self.finalView.hidden = YES;
            [self.refreshingView startAnimating];
            break;
            
        case KYERefreshStateNoMoreData:
        case KYERefreshStateStop:
            if (oldState == KYERefreshStateIdle ||
                self.isProhibitFooter           ||
                [self heightForContentBreakView] < 0) {
                self.label.text = @"";
            } else {
                self.label.text = @"没有更多数据";
            }
            
            self.label.hidden = NO;
            self.arrowView.image = [UIImage imageNamed:@"pull_up"];
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = YES;
            self.finalView.hidden = YES;
            self.finalView.image = [UIImage imageNamed:@"failure"];
            [self.refreshingView stopAnimating];
            break;
            
        case KYERefreshStateSuccess:
            self.label.text = @"刷新成功";
            
            self.label.hidden = NO;
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = YES;
            self.finalView.hidden = NO;
            self.finalView.image = [UIImage imageNamed:@"done"];
            [self.refreshingView stopAnimating];
            break;
        case KYERefreshStateFail:
            self.label.text = @"刷新失败";
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = YES;
            self.finalView.hidden = YES;
            self.finalView.image = [UIImage imageNamed:@"failure"];
            [self.refreshingView stopAnimating];
            break;
            
        case KYERefreshStateNoNetwork:
            self.label.text = kNetError;
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = YES;
            self.finalView.hidden = YES;
            self.finalView.image = [UIImage imageNamed:@"failure"];
            [self.refreshingView stopAnimating];
            break;
            
        case KYERefreshStateNoResponse:
            self.label.text = kNetError;
            self.arrowView.hidden = YES;
            self.refreshingView.hidden = YES;
            self.finalView.hidden = YES;
            self.finalView.image = [UIImage imageNamed:@"failure"];
            [self.refreshingView stopAnimating];
            break;
        default:
            break;
    }
}
- (void)endRefreshingFailureWithError:(NSError *)error {
    // 判断是没有网络还是链接超时，还是服务器出错
    // - 1009 没有网络   -1001 网络超时
    // footer不做超时和无网的区分，header是因为超时要提示失败，无网显示正在刷新
    if (error.code == -1009 || error.code == -1001 ) {
        [self endRefreshingWithState:KYERefreshStateNoNetwork];
    } else {
        [self endRefreshingWithState:KYERefreshStateNoResponse];
    }
}
/** 结束刷新,刷入对应状态显示对应提示 */
- (void)endRefreshingWithState:(KYERefreshState)state {
    
    if (!_tableView) [self getTableView];
    
    self.state = state;
    /*
     做自己的操作
     */
    switch (state) {
        case KYERefreshStateSuccess:        // 成功
            [self clearTableHeadView];
            break;
        case KYERefreshStateFail:           // 失败
            [self clearTableHeadView];
            break;
        case KYERefreshStateNoMoreData:     // 无更多数据
            [self clearTableHeadView];
            break;
        case KYERefreshStateNoNetwork:      // 无网络
            [self isSetTableHeadView];
            break;
        case KYERefreshStateNoResponse:     // 无响应
            [self isSetTableHeadView];
            break;
        case KYERefreshStateBeyondNetwork:  // 网络超时
            [self isSetTableHeadView];
            break;
        default:
            break;
    }
    
    // 恢复成普通状态
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.35 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        // 无更多数据就不恢复成普通状态了
        if (state != KYERefreshStateNoMoreData) {
            [self endRefreshing];
        }
        [self IsClearLabelText];
    });
}
-(void)endRefreshing {
    // 恢复成普通状态
    [super endRefreshing];
    
}
#pragma mark - 获取tableView
- (void)getTableView {
    //获取表视图
    if ([self.scrollView isKindOfClass:[UITableView class]]) {
        _tableView = (UITableView *)self.scrollView;
    }
}
#pragma mark - 清除表头
- (void)clearTableHeadView {
    _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KSCREEN_W, CGFLOAT_MIN)];
}
#pragma mark - 设置表头
- (void)isSetTableHeadView {
    if ([_tableView getTotalDataCount] != 0) {
        [self setTableHeadView];
    } else {
        [self clearTableHeadView];
    }
}
#pragma mark - 是否清除labelText
- (void)IsClearLabelText {
    // 内容不足一屏幕 or 禁止上拉刷新，上拉无文字显示
    if ([self heightForContentBreakView] < 0 ||
        self.isProhibitFooter){
        self.label.text = @"";
    }
}
- (void)setTableHeadView {
    NetStatusView *view = [[NetStatusView alloc] initWithFrame:CGRectMake(0, -1, 330, 40)];
    [view setNetErrorStyle];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        _tableView.tableHeaderView = view;
    });
}

#pragma mark 获得scrollView的内容 超出 view 的高度
- (CGFloat)heightForContentBreakView
{
    CGFloat h = self.scrollView.frame.size.height - self.scrollViewOriginalInset.bottom - self.scrollViewOriginalInset.top;
    return self.scrollView.contentSize.height - h;
}
#pragma mark 监听scrollView的contentOffset改变
- (void)scrollViewContentOffsetDidChange:(NSDictionary *)change {
    [super scrollViewContentOffsetDidChange:change];
}

#pragma mark 监听scrollView的contentSize改变
- (void)scrollViewContentSizeDidChange:(NSDictionary *)change {
    [super scrollViewContentSizeDidChange:change];
}

#pragma mark 监听scrollView的拖拽状态改变
- (void)scrollViewPanStateDidChange:(NSDictionary *)change {
    [super scrollViewPanStateDidChange:change];
}
//#pragma mark - 禁用和开启滚动
//- (void)EnabledScrollWithState:(KYERefreshState)state {
//    self.scrollView.scrollEnabled = YES;
//    dispatch_async(dispatch_get_main_queue(), ^{
//        if (self.scrollView.getTotalDataCount == 0) {
//            self.scrollView.scrollEnabled = NO;
//        }
//    });
//}
//#pragma mark - 是否设置无数据水印
//- (void)header_setNoDataView {
//    
//    //设置无数据视图
//    if (self.state != KYRefreshStateNoNetwork  &&
//        self.state != KYRefreshStateNoResponse &&
//        self.state != KYRefreshStateBeyondNetwork) {
//        // 清除表头
//        [self clearTableHeadView];
//        
//        if ([_tableView getTotalDataCount] == 0) {
//            [_tableView setNoData]; // 无数据视图
//        } else {
//            [_tableView hideStatusView];
//        }
//    }
//}
@end
