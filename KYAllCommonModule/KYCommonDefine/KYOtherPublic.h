//
//  KYOtherPublic.h
//  testPods
//
//  Created by xiegang on 2017/5/18.
//  Copyright © 2017年 xiegang. All rights reserved.
//

/**
 *  此部分宏定义均为项目非UI设置相关的一些功能宏定义，除非是经常使用的宏定义否则请勿随意添加！
 *  标注不清晰的烦请添加人详细标注一下
 */

#ifndef KYOtherPublic_h
#define KYOtherPublic_h

#pragma mark  *** DEBUG环境的log替换 ***

#ifdef DEBUG

#define NSLog(FORMAT, ...) do {                                                      \
fprintf(stderr,"<%s : %d> %s\n",[[[NSString stringWithUTF8String:__FILE__] lastPathComponent] UTF8String],                                                  \
__LINE__, __func__);                                                                 \
NSLog(@" ");\
fprintf(stderr,"%s\n",[[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);   \
fprintf(stderr, "-------\n"); \
} while (0)

#else
#define NSLog(...)

#endif


#ifdef __OBJC__

/** 宏定义涉及的引用 (依赖于KYIntegratedToolsModule私有组件)*/
#import "StringTools.h"
#import "MyUUIDManager.h"
#import "UserInfoManage.h"


#pragma mark *** 项目常用URL切换 ***
/** 正式（新） */
#define H5_BASE_URL          @"https://nbweb.ky-express.com/"
/** 测试（新） */
//#define H5_BASE_URL        @"http://10.86.86.56/"
/** 正式（旧） */
//#define H5_BASE_URL        @"http://res.ky-express.com/"
/** 正式环境获取热门搜索地址 */
#define KY_STUDY_BASE_URL    @"https://nbapi.ky-express.com/json/studykw.html"
/** 测试环境获取热门搜索地址 */
//#define KY_STUDY_BASE_URL  @"http://10.86.86.56:8080/studykw.html"


#pragma mark *** 百度地图账号信息 ***
//#define BaiduKey @"rWlVLQNGVitkVjOzRyxnMH59RoNX9E4k"
#define BaiduKey @"m1cCABMwONIq6EUjNgG3OvSRTHdVro9E"
//boundID： com.InternalkyExpresstest.www 百度账号：18948177303 ky4008admin
//#define BaiduKey  @"LpUGZ6jPpGnLlZoOuR5gikUHM7nKosXs" //测试
//#define BaiduKey @"HnFOH8qufSj6CtElFGBinim6cT01X63l"


#pragma mark *** 简单的字符串标志替换定义 ***
/** 文本定义(未做国际化) */
#define kServiceId          @"126196"
#define KPhoneNumer         @"4008-098-098"
#define kNetError           @"无法连接服务器，请检查您的网络设置"
#define kDefaultBlankStr    @"暂无数据"
#define kDefaultBlankStr2   @" "
#define kUnderContructStr   @"模块在建设中..."
#define kNoDataStr          @"暂无相关数据"
#define kNoPermission       @"抱歉,您没有访问权限"
#define kNoModulePermission @"您没有权限使用此模块"
/** 请添加注释说明）*/
#define Ky_Idetifider        @"Ky_Idetifider"
#define kLaunchDict          @"kLaunchDict"
#define kLaunchData          @"kLaunchData"
#define LOGIN_STATE          @"login_state"
#define RESULTCODE           @"returnCode"
#define RESULTTYPE           @"returnType"
#define JD_RESULTMESSAGE     @"returnMessage"
#define KY_ERROR_MSG         @"errorMsg"
#define RESULT               @"result"
#define SUCCESS              @"success"
#define JD_return            @"errMsg"
#define KY_RetStatus         @"retStatus"
#define KIsCheck             @"isCheck"
#define KSettingMessage      @"settingMessage"
#define KSettingAvoidDisturb @"SettingAvoidDisturb"
/** 用来保存市场约车的需不需要显示小红点的路径 */
#define marketAboutCarPath   @"shichangyueche"
/** 市场约车监听的通知 */
#define marketAboutCarNotice @"shichangyuechetongzhi"
/** 按钮ID */
#define moduleAccess         @"1"
#define moduleSelect         @"2"
#define moduleRevise         @"3"
#define moduleDelete         @"4"
#define moduleSave           @"5"
/** 摇红包相关 */
#define KThreeMinute         1800
#define KFiveMinute          300
#define K600MM               600
#define K60MM                180
#define KLocalNotificationKey_one  @"localNotificationKey_one"
#define KLocalNotificationKey_two  @"localNotificationKey_two"
/** 保存登录时间 */
#define KLoginTime             @"loginTime"
/** 保存收件查询时间 */
#define KCheckReceiveOrderTime @"checkReceiveOrderTime"
/** 保存寄件查询时间 */
#define KCheckSendOrderTime    @"checkSendOrderTime"
/** 开始时间 */
#define KStartTime             @"startTime"
/** 结束时间 */
#define KEndTime               @"endTime"
/** 登录成功 */
#define KYLoginSuccessNotification @"KYLoginSuccessNotification"


#pragma mark *** 便捷方法与对象获取替换定义 ***
/** keywindow与AppDelegate单利对象获取 */
#define KyeAppDelegate   ((AppDelegate *)[UIApplication sharedApplication].delegate)
/** 设备的唯一编号 不是UUID */
#define MyUUID      [MyUUIDManager getUUID]
#define kKeyWindow  ([UIApplication sharedApplication].keyWindow)
#define kAppDel     ((AppDelegate *)[UIApplication sharedApplication].delegate)
#define kAppDelegate  ((AppDelegate *)[UIApplication sharedApplication].delegate)
/** 网络请求返回状态获取 */
#define kRetStatus  [StringTools getStringWithID:responseObject[@"retStatus"]]
#define kErrMsg     [StringTools getStringWithID:responseObject[@"errMsg"]]
#define kErrCode    [StringTools getStringWithID:responseObject[@"errCode"]]
#define kRetMsg     [StringTools getStringWithID:responseObject[@"result"][@"retMsg"]]
#define kRetStatusIsNotOne (![kRetStatus isEqualToString:@"1"])
#define kErrCodeIsLogOut ([kErrCode isEqualToString:@"1111"])  //登录超时

/** 沙盒路径获取 */
#define kSandboxPathStr [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject]
/** 模块标新的路径 1=模块访问 2＝查询 3=修改 4=删除 5=保存／提交 */
#define ModuleBouncyPath [NSString stringWithFormat:@"ModuleBouncyPath%@",kGetUserName ? kGetUserName : @""]
/** 登录用户基本信息获取 各项信息详情咨询李军樟 */
#define kUUIDStr  ([[UserInfoManage sharedInstance] getUUID] ? [[UserInfoManage sharedInstance] getUUID] : @"")
#define kUserName ([[UserInfoManage sharedInstance] getUserName] ? [[UserInfoManage sharedInstance] getUserName] : @"")
#define kUserDepartment ([[UserInfoManage sharedInstance] getDepartment] ? [[UserInfoManage sharedInstance] getDepartment] : @"") //部门
#define kUserID   ([[UserInfoManage sharedInstance] getUserId] ? [[UserInfoManage sharedInstance] getUserId] : @"")
#define kUserNo   ([[UserInfoManage sharedInstance] getPeopleNo] ? [[UserInfoManage sharedInstance] getPeopleNo] : @"")
#define kUserTelePhone ([[UserInfoManage sharedInstance] getTelePhone] ? [[UserInfoManage sharedInstance] getTelePhone] : @"")
#define kGetUserName    ([[UserInfoManage sharedInstance] getUserName] ? [[UserInfoManage sharedInstance] getUserName] : @"")
#define kGetUserRank  ([[UserInfoManage sharedInstance] getUserRank] ? [[UserInfoManage sharedInstance] getUserRank] : @"")
#define kIsShowName ([[UserInfoManage sharedInstance] IsShowName] ? [[UserInfoManage sharedInstance] IsShowName] : @"")
#define kGetDataSync ([[UserInfoManage sharedInstance] getDataSync] ? [[UserInfoManage sharedInstance] getDataSync] : @"")
#define kGetIsOther ([[UserInfoManage sharedInstance] getIsOther]? [[UserInfoManage sharedInstance] getIsOther] : @"")
#define kGetJobtitle ([[UserInfoManage sharedInstance] getJobtitle]? [[UserInfoManage sharedInstance] getJobtitle] : @"") //职务
#define kGetUserIsAgree ([[UserInfoManage sharedInstance] getUserIsAgree]? [[UserInfoManage sharedInstance] getUserIsAgree] : @"") ////是否同意协议）
#define kIfNeedChecked ([[UserInfoManage sharedInstance] getIfNeedChecked] ? [[UserInfoManage sharedInstance] getIfNeedChecked] : @"0")
/** 判断字符串A是否包含字符串B */
#define  Ky_StringAHasStringB(A,B)   [A rangeOfString:B].location !=NSNotFound? YES : NO;
/** block 中使用Self */
#define WS(weakSelf)        __weak __typeof(&*self)weakSelf = self
/** block 中调用cell */
#define WC(weakCell,cell)   __weak __typeof(&*cell)weakCell = cell

#pragma mark *** 用户注册登录 ***
/** 设置已经登录 */
#define kSetLogin     [[LMUserDefaults standardUserDefaults] setBool:YES forKey:@"isLogin"]
/** 设置退出登录 */
#define kSetLogout    [[LMUserDefaults standardUserDefaults] setBool:NO forKey:@"isLogin"]
/** 判断是否登录 */
#define kIsLogin      [[LMUserDefaults standardUserDefaults] boolForKey:@"isLogin"]
/** 是否第一次启动程序 */
#define kIsFristLaunching  [[LMUserDefaults standardUserDefaults] boolForKey:@"isFristLaunching"]
/** 设置已启动 */
#define KSetFristLaunching  [[LMUserDefaults standardUserDefaults] setBool:YES forKey:@"isFristLaunching"]
/** 默认图片 */
#define kDefaultImage [UIImage imageNamed:@"placeholder"]
/** 获取图片 */
#define IMAGE(name) [UIImage imageNamed:name]




#pragma mark  *** 系统设备相关信息信息 ***
/** iOS7 以上版本 */
#define kIOS7_OR_LATER ([[[UIDevice currentDevice] systemVersion] compare:@"7.0"] != NSOrderedAscending)
//iOS8 以上版本
#define kIOS8_OR_LATER ([[[UIDevice currentDevice] systemVersion] compare:@"8.0"] != NSOrderedAscending)
//iOS8.4.1 版本
#define kIOS8_4_1      [[[UIDevice currentDevice] systemVersion] isEqualToString:@"8.4.1"]
//iOS8 以下版本
#define kIOS8_Before ([[[UIDevice currentDevice] systemVersion] compare:@"8.0"] != NSOrderedDescending)
//当前系统版本
#define kIOS_VERSION   ([[[UIDevice currentDevice] systemVersion] floatValue])
//iPhone机型判断
#define IS_IPHONE_X     (CGSizeEqualToSize(CGSizeMake(375, 812), [[UIScreen mainScreen] bounds].size))
#define IS_IPHONE_6Plus ([[UIScreen mainScreen ] bounds].size.height == 736)
#define IS_IPHONE_6     ([[UIScreen mainScreen ] bounds].size.height == 667)
#define IS_IPHONE_5     ([[UIScreen mainScreen ] bounds].size.height == 568)
#define IS_IPHONE_4     ([[UIScreen mainScreen ] bounds].size.height == 480)

#pragma mark *** ios系统中各种设置项的url短链链接 ***
/** 设置主界面 */
#define KSettingMenu          @"prefs:root=General"
/** 设置中关于页面 */
#define KSettingAbout         @"prefs:root=General&path=About"
/** 辅助功能 */
#define KSettingAccessibility @"prefs:root=General&path=ACCESSIBILITY"
/** 蓝牙 */
#define KSettingBluetooth     @"prefs:root=Bluetooth"
/** 日期和时间 */
#define KSettingDateTime      @"prefs:root=General&path=DATE_AND_TIME"
/** 语言与地区 */
#define KSettingInternational @"prefs:root=General&path=INTERNATIONAL"
/** 通知 */
#define KSettingNotification  @"prefs:root=NOTIFICATIONS_ID"
/** WiFi */
#define KSettingWiFi          @"prefs:root=WIFI"
/** 定位服务 */
#define KSettingLocationServices  @"prefs:root=LOCATION_SERVICES"
/** 自动锁定 */
#define KSettingAUTOLOCK      @"prefs:root=General&path=AUTOLOCK"
/** FaceTime */
#define KSettingFaceTime      @"prefs:root=FACETIME"
/** 通用 */
#define KSettingGeneral       @"prefs:root=General"
/** 键盘 */
#define KSettingKeyboard      @"prefs:root=General&path=Keyboard"
/** 音乐 */
#define KSettingMusic         @"prefs:root=MUSIC"
/** 备忘录 */
#define KSettingNotes         @"prefs:root=NOTES"
/** 照片与相机 */
#define KSettingPhotos        @"prefs:root=Photos"
/** 设备管理 */
#define KSettingProfile       @"prefs:root=General&path=ManagedConfigurationList"
/** 还原 */
#define KSettingReset         @"prefs:root=General&path=Reset"
/** 声音 */
#define KSettingSounds        @"prefs:root=Sounds"
/** 系统升级 */
#define KSettingSoftwareUpdate  @"prefs:root=General&path=SOFTWARE_UPDATE_LINK"
/** iTunes Store & App Store */
#define KSettingStore         @"prefs:root=STORE"
/** 墙纸 */
#define KSettingWallpaper     @"prefs:root=Wallpaper"
/** 隐私 */
#define KSettingPrivacy       @"prefs:root=Privacy"
/** 隐私-通讯录 */
#define KSettingPrivacyContacts @"prefs:root=Privacy&path=CONTACTS"
/** 隐私-相机 */
#define KSettingPrivacyCamera @"prefs:root=Privacy&path=CAMERA"
/** 隐私-麦克风 */
#define KSettingPrivacyMICROPHONE @"prefs:root=Privacy&path=MICROPHONE"


#pragma mark *** UUD ***
#define  KEY_USERNAME_PASSWORD @"com.company.app.usernamepassword"
#define  KEY_USERNAME          @"com.company.app.username"
#define  KEY_PASSWORD          @"com.company.app.password"


#endif

#endif /* KYOtherPublic_h */
