//
//  KYUIPublic.h
//  testPods
//
//  Created by xiegang on 2017/5/18.
//  Copyright © 2017年 xiegang. All rights reserved.
//


/** 
 *  此部分宏定义均为UI设置相关宏定义，除非是经常使用的宏定义否则请勿随意添加！
 *  标注不清晰的请添加人标注一下
 */


#ifndef KYUIPublic_h
#define KYUIPublic_h

#ifdef __OBJC__

/** 宏定义涉及的引用 (依赖于KYClassExtensions私有组件) */
#import "UIColor+Common.h"
#import "UIColor+Extensior.h"
#import "KIInstructTool.h"

#pragma mark  ***屏幕比例 UI控件宽高比例 相关***
/** 屏幕机型比例系数*/
#define kScale (KSCREEN_W / 375)
#define kScale5S (KSCREEN_W / 320)
#define KSpaceWidth 15.0*kScale

/** 屏幕宽高 */
#define KSCREEN_W      ([UIScreen mainScreen].bounds.size.width)
#define KSCREEN_H      ([UIScreen mainScreen].bounds.size.height)
#define kSCREEN_WIDTH  ([UIScreen mainScreen].bounds.size.width)
#define kSCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)

/** 屏幕元素 */
#define kStatusbarHeight ([[UIApplication sharedApplication] statusBarFrame].size.height)
#define kStatusBarAddNavigationBarHeight (kStatusbarHeight+44.f)
#define kSafeAreaInsetsTop ((CGSizeEqualToSize(CGSizeMake(375, 812), [[UIScreen mainScreen] bounds].size))?24.f:0.f)
#define kSafeAreaInsetsBottom ((CGSizeEqualToSize(CGSizeMake(375, 812), [[UIScreen mainScreen] bounds].size))?34.f:0.f)


/** 42高度cell 或者 36高度cell */
#define K16CellRowHeight (KSCREEN_W>350?42:36)
#define K16LabelRowHeight 23

#define kTabbar_Height  49
#define kNav_Height     64
#define AddressTableViewCell_Height 120
#define colMargin 10
#define kBottomH 60
#define kMargin 15
#define kRelateRowH 35

#define HOME_COLLECTION_CELL_HEIGHT 265

//#pragma mark  *** 字体大小比例相关 ***(无关)
/** 15字，14号字 */
#define kFifteenSize [UIFont systemFontOfSize:15.0]
#define kFourteenSize [UIFont systemFontOfSize:14.0]
/** 屏幕大于350 16号字体 否则 14号 */
#define Ky_FontSize5S14Or16 KSCREEN_W>350?[UIFont systemFontOfSize:16.0f]:[UIFont systemFontOfSize:14.0f]
#define Ky_FontSize5S12Or14 KSCREEN_W>350?[UIFont systemFontOfSize:14.0f]:[UIFont systemFontOfSize:12.0f]
/** 字体 */
#define KTextFont [UIFont systemFontOfSize:15.0f]
#define kFont10 [UIFont systemFontOfSize:10.0f]
#define kFont11 [UIFont systemFontOfSize:11.0f]
#define kFont12 [UIFont systemFontOfSize:12.0f]
#define kFont14 [UIFont systemFontOfSize:14.0f]
#define STextFont [UIFont systemFontOfSize:15.0f]
#define kFont16 [UIFont systemFontOfSize:16.0f]
#define kFont18 [UIFont systemFontOfSize:18.0f]
#define kFont24 [UIFont systemFontOfSize:24.0f]

#pragma mark  *** 色值相关 ***
#define K16TextPlaceholderColor ([UIColor colorWithRed:187/255.0 green:187/255.0 blue:194/255.0 alpha:1.0])
#define K16WhiteColor [UIColor whiteColor]//白色
#define K16ThemeColor [UIColor Ky_colorWithHex:0xC879F6]//主题灰
#define K16PurpleColor [UIColor Ky_colorWithHex:0x7C0FBB]//主题紫cccccc
#define K16GrayColor [UIColor Ky_colorWithHex:0xEEEEEE]//808080背景灰
#define K16GrayTextColor [UIColor Ky_colorWithHex:0x999999]//字体灰
#define K16OrangeColor [UIColor Ky_colorWithHex:0xFF5500]//橙色
#define K16LightPurpleColor [UIColor Ky_colorWithHex:0x696BFA]//淡紫色APP下载
#define K16BorderColor [UIColor Ky_colorWithHex:0xCCCCCC]//边框颜色
#define K16TextBackGroundColor [UIColor Ky_colorWithHex:0xEDEDED]//字背景颜色
#define kColorRGB(r,g,b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]
#define COLORRGB(r, g, b) ([UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0])
#define COLOR_RGB16(rgbValue) ([UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0])
#define kPurpleColor COLORRGB(113, 37, 180)
#define kLineColor COLOR_RGB16(0xdddddd)
//RGB颜色值
#define RGB(r,g,b)    [UIColor colorWithRed:(double)r/255.0f green:(double)g/255.0f blue:(double)b/255.0f alpha:1]
#define RGBA(r,g,b,a) [UIColor colorWithRed:(double)r/255.0f green:(double)g/255.0f blue:(double)b/255.0f alpha:(double)a]
//十六进制颜色
#define HEXCOLOR(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
// 浅蓝色
#define KBlueColor    [UIColor colorWithRed:0.2902 green:0.5882 blue:0.8471 alpha:1.0]
// 浅绿色
#define KGreeColor    [UIColor colorWithRed:0.3098 green:0.7294 blue:0.3569 alpha:1.0]
#define Kmain_fense   [UIColor colorWithRed:1.0 green:0.0036 blue:0.3987 alpha:1.0]
// 主色调
#define KMain_Color   [UIColor colorWithHexString:@"0x7c0fbb"]
#define KBack_Color   [UIColor groupTableViewBackgroundColor]
#define KBack_Color2     RGBA(249,249,251,1)

#define KWhite_Color  [UIColor whiteColor]
#define KTextGray     RGB(192,192,192)

#define KMain_Color_light  RGB(147, 58, 202)

#define KColorPhone   [UIColor colorWithHexString:@"006600"]
#define KBlackGray   [UIColor colorWithHexString:@"999999"]

#define KColor666666   [UIColor colorWithHexString:@"666666"]
#define KColorRed   [UIColor redColor]

#define KLightGrayColor   [UIColor colorWithHexString:@"cdcdcd"]
#define KMainBackColor [UIColor colorWithHexString:@"0xf5f5f5"]

#define KColorCellSelection   [UIColor colorWithHexString:@"f5f5f5"]  //tableViewCell点击效果颜色
#define kColorPlaceHolder RGB(196, 195, 199)   //PlaceHolder颜色
#define kColorTextBlack [UIColor colorWithHexString:@"333333"]  //普通黑色文本
#define kColorTextGray  [UIColor grayColor] //普通灰色文本
#define KColorTextContent [UIColor colorWithHexString:@"0x666666"]
#define KColorPhone [UIColor colorWithHexString:@"006600"]  //电话号码文本颜色
#define kColorBorderGray [UIColor lightGrayColor]
#define kColorLightPurple RGB(186, 119, 227)
#define kColorBorderTri   RGB(207, 207, 207)
#define kColorVoice       RGB(215, 155, 252)
#define kColorLabel       RGB(73, 73, 73);    //普通label颜色
#define kColorDarPurple  RGB(105, 26, 158)  //深紫色
#define kPhoneBackColor  [UIColor colorWithHexString:@"49ab49"]  //电话号码拨打按钮背景绿色
#define k16PhoneNumberColor kColorRGB(14, 105, 0)//字背景颜色

#endif

#endif /* KYUIPublic_h */
