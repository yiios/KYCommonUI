//
//  KYAllDefine.h
//  testPods
//
//  Created by xiegang on 2017/5/18.
//  Copyright © 2017年 xiegang. All rights reserved.
//

#define MAS_SHORTHAND

#ifndef KYAllDefine_h
#define KYAllDefine_h

#ifdef __OBJC__

#import "KYUIPublic.h"
#import "KYOtherPublic.h"

#endif

#endif /* KYAllDefine_pch */
